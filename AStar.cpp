#include "AStar.h"

std::vector<std::shared_ptr<Tile>> AStarPathfinding::FindPath(std::map<std::shared_ptr<Tile>, std::map<std::shared_ptr<Tile>,int>>& edges, std::shared_ptr<Tile> originNode, std::shared_ptr<Tile> destinationNode){
    HeapQueue<std::shared_ptr<Tile>> frontier;
    frontier.Enqueue(originNode,0);

    std::map<std::shared_ptr<Tile>, std::shared_ptr<Tile>> cameFrom;
    cameFrom[originNode] = nullptr;
    std::map<std::shared_ptr<Tile>, int> costSoFar;
    costSoFar[originNode] = 0;

    while(frontier.size()!=0){
        std::shared_ptr<Tile> current(frontier.Dequeue());
        if(current == destinationNode) break;

        std::vector<std::shared_ptr<Tile>> neigbours = GetNeighbours(current, edges);
        for(std::shared_ptr<Tile> neigbour : neigbours){
            int newCost = costSoFar[current] + edges[current][neigbour];
            if(costSoFar.find(neigbour)==costSoFar.end() || newCost < costSoFar[neigbour]){
                costSoFar[neigbour] = newCost;
                cameFrom[neigbour] = current;
                int priority = newCost + Heuristic(neigbour, destinationNode);
                frontier.Enqueue(neigbour, priority);
            }
        }
    }

    std::vector<std::shared_ptr<Tile>> path;
    if(originNode == destinationNode)
        return path;
    if(cameFrom.find(destinationNode)==cameFrom.end())
        return path;

    path.push_back(destinationNode);
    std::shared_ptr<Tile> temp(destinationNode);
    while(cameFrom[temp]!=originNode){
        std::shared_ptr<Tile> currentPathElement(cameFrom[temp]);
        path.push_back(currentPathElement);
        temp = currentPathElement;
    }

    return path;
}

std::map<std::shared_ptr<Tile>, std::map<std::shared_ptr<Tile>, int>> AStarPathfinding::GetGraphEdges(const std::map<glm::vec3, std::shared_ptr<Tile>, util::vec3Comparator>& tiles){
    std::map<std::shared_ptr<Tile>, std::map<std::shared_ptr<Tile>, int>> ret;
    for(const auto& p : tiles){
        std::shared_ptr<Tile> tile(p.second);
        if(tile->trespassable){
            for(std::shared_ptr<Tile> neighbour : tile->GetNeighbours(tiles))
                if(neighbour->trespassable)
                    ret[tile][neighbour] = neighbour->movementCost;
        }
    }
    return ret;
}

std::vector<std::shared_ptr<Tile>> AStarPathfinding::GetNeighbours(std::shared_ptr<Tile> node, const std::map<std::shared_ptr<Tile>,std::map<std::shared_ptr<Tile>,int>>& edges){
    std::vector<std::shared_ptr<Tile>> ret;
    std::map<std::shared_ptr<Tile>,int> e;
    try{
        /// gets the container of neighbours mapped to the node.
        e = edges.at(node);
    }
    catch(...){
        /// if the node passed is not in the container of edges, return an empty vector.
        return ret;
    }

    /// only the pointer is needed, not the associated integer.
    std::transform(std::begin(e), std::end(e), std::back_inserter(ret), [](const auto& p){return p.first;});
    return ret;
}

int AStarPathfinding::Heuristic(std::shared_ptr<Tile> a, std::shared_ptr<Tile> b){
    return a->GetDistance(*b) * (a->movementCost+b->movementCost) * 0.5f;
}
