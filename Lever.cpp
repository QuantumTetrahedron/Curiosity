#include "Lever.h"
#include "LevelManager.h"
#include <regex>

Lever::Lever(const Lever& other){
    *this = other;
}

Lever::Lever(glm::vec3 powerDest, glm::vec3 position, glm::vec3 rotation, glm::vec3 scale, std::string modelName, std::string baseModel)
    : GameObject(position, rotation, scale, modelName, true, true, false), PowerSource(powerDest){
    target.rotation = transform.rotation - glm::vec3(30,0,0);
    transform.rotation = transform.rotation - glm::vec3(30,0,0);
    baseModelName = baseModel;
    active = false;
}

Lever::Lever(std::string str){
    std::regex pattern{R"((\w{1})\s+(\S+)\s+(\S+)\s+(\S+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+))"};
    std::smatch matches;
    if(std::regex_match(str, matches, pattern)){
        std::string name = matches[3];
        std::string type = matches[2];
        baseModelName = matches[4];

        int x = atoi(((std::string)matches[5]).c_str());
        int y = atoi(((std::string)matches[6]).c_str());
        int z = atoi(((std::string)matches[7]).c_str());
        int r = atoi(((std::string)matches[8]).c_str());
        int dx = atoi(((std::string)matches[9]).c_str());
        int dy = atoi(((std::string)matches[10]).c_str());
        int dz = atoi(((std::string)matches[11]).c_str());

        *this = Lever(glm::vec3(2*dx,dy,2*dz), glm::vec3(2*x, y, 2*z), glm::vec3(0,90*r,0), glm::vec3(1.0f), name, baseModelName);
    }
}

std::unique_ptr<GameObject> Lever::clone() const{
    return std::make_unique<Lever>(*this);
}

void Lever::Draw(const Shader& shader) {
    GameObject::Draw(shader);
    glm::vec3 oldRot = transform.rotation;
    transform.rotation.x = 0;
    Model& model = ResourceManager::GetModel(baseModelName);

    glm::mat4 m = GetModelMatrix();
    shader.Use();
    shader.SetMatrix4("model", m);
    model.Draw(shader);
    transform.rotation = oldRot;
}

void Lever::Activate(){
    if(!active){
        GivePower();
        target.AddRotation(glm::vec3(60,0,0));
        active = true;
    }
    else{
        CutPower();
        target.AddRotation(glm::vec3(-60,0,0));
        active = false;
    }
}

void Lever::Attract(float fromRot){
    float relRot = transform.rotation.y;
    if(active)
    {
        relRot += 180;
        if(relRot >= 360) relRot -= 360;
    }

    if(fromRot == 0 && relRot == 270)
        Activate();
    if(fromRot == 90 && relRot == 180)
        Activate();
    if(fromRot == 180 && relRot == 90)
        Activate();
    if(fromRot == 270 && relRot == 0)
        Activate();
}

void Lever::Repulse(float fromRot){
    float relRot = transform.rotation.y;
    if(active)
    {
        relRot += 180;
        if(relRot >= 360) relRot -= 360;
    }

    if(fromRot == 0 && relRot == 90)
        Activate();
    if(fromRot == 90 && relRot == 0)
        Activate();
    if(fromRot == 180 && relRot == 270)
        Activate();
    if(fromRot == 270 && relRot == 180)
        Activate();
}
