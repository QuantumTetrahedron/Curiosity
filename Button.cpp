#include "Button.h"

Button::Button() : renderer{nullptr}, textRenderer{nullptr} {}

Button::Button(std::shared_ptr<SpriteRenderer> imageRenderer, std::shared_ptr<TextRenderer> fontRenderer, std::string text,
               glm::vec2 pos, glm::vec2 size, Texture2D image, glm::vec3 color)
               : renderer{imageRenderer}, textRenderer{fontRenderer}, label{text}, imagePosition{pos}, imageSize{size}, sprite{image}, color{color}, active{true} {
                    float textWidth = textRenderer->GetStringWidth(label);
                    textPosition.x = imagePosition.x + (imageSize.x - textWidth)/2;
                    textPosition.y = imagePosition.y + (imageSize.y - textRenderer->GetStringHeight(label))/2;
               }

bool Button::IsClicked(const Input &mouse) const {
    return (IsHovered(mouse) && mouse.mouseButtons[0]/*mouse.left == ButtonState::PRESSED*/);
}

bool Button::IsHovered(const Input &mouse) const {
    if (active && mouse.mouseX > imagePosition.x && mouse.mouseX < imagePosition.x + imageSize.x &&
        mouse.mouseY > imagePosition.y && mouse.mouseY < imagePosition.y + imageSize.y)
            return true;
    return false;
}

void Button::Update(const Input& mouse) {
    if (IsHovered(mouse)) {
        color = glm::vec3(0.8f, 0.8f, 1.0f);
    }
    else {
        color = glm::vec3(1.0f);
    }
}

void Button::Draw() const {
    renderer->DrawSprite(sprite, imagePosition, imageSize, 0.0f, color);
    textRenderer->Render(label, textPosition.x, textPosition.y);
}
