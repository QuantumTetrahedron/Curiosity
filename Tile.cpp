#include "Tile.h"
#include "ResourceManager.h"
#include "LevelManager.h"
#include "Utility.h"

const std::vector<glm::vec3> Tile::directions{glm::vec3(2.0f, 0.0f,0.0f), glm::vec3(2.0f, 1.0f,0.0f), glm::vec3(2.0f,-1.0f,0.0f),
                                                   glm::vec3(-2.0f, 0.0f,0.0f), glm::vec3(-2.0f, 1.0f,0.0f), glm::vec3(-2.0f,-1.0f,0.0f),
                                                   glm::vec3(0.0f, 0.0f,2.0f), glm::vec3(0.0f, 1.0f,2.0f), glm::vec3(0.0f,-1.0f,2.0f),
                                                   glm::vec3(0.0f, 0.0f,-2.0f), glm::vec3(0.0f, 1.0f,-2.0f), glm::vec3(0.0f,-1.0f,-2.0f) };



Tile::Tile(glm::vec3 position, glm::vec3 rotation, glm::vec3 scale, std::string modelName, int moveCost, glm::vec3 TilePosOffset, bool isFloor)
 : GameObject(position, rotation, scale, modelName, true, false, true, true), highlighter(position+TilePosOffset+glm::vec3(0.0f,0.25f,0.0f), rotation, scale*glm::vec3(0.9f,0.2f,0.9f), "tile", false, false), movementCost(moveCost){
    TilePositionOffset = TilePosOffset;
    collider = std::experimental::make_optional(BoxCollider(position, ResourceManager::GetModel(modelName).height, isFloor));
    hovered = false;
    isPath=false;
    selected=false;
    occupied=false;
    trespassable=true;
    hasPlayer = false;
}

std::unique_ptr<GameObject> Tile::CopyAt(glm::vec3 pos, glm::vec3 rot, bool safeCopy){
    std::unique_ptr<Tile> obj(dynamic_cast<Tile*>(GameObject::CopyAt(pos, rot).release()));
    obj->highlighter.transform.position = pos + glm::vec3(0.0f,0.25f,0.0f) + TilePositionOffset;
    obj->Unhover();
    return obj;
}

void Tile::Update(double dt){
    if(transform.position != target.position){
        auto objOnTop = LevelManager::GetObject(transform.position + TilePositionOffset);
        if(objOnTop){
            objOnTop->target.position = target.position + TilePositionOffset;
        }
    }

    GameObject::Update(dt);
    highlighter.transform.position = transform.position + TilePositionOffset + glm::vec3(0.0f,0.25f,0.0f);
}

void Tile::Draw(const Shader& shader){
    GameObject::Draw(shader);
    if(Highlighted())
        highlighter.Draw(shader);
}

void Tile::UpdateHighlighter(){
    /*if(hovered){
        highlighter.tint=glm::vec3(1.0f,0.0f,0.0f);
        if(selected)
            highlighter.tint+=glm::vec3(0.0f,0.0f,1.0f);
    }
    else if(isPath)
        highlighter.tint=glm::vec3(1.0f,1.0f,0.0f);
    else if(selected)
        highlighter.tint=glm::vec3(0.0f,0.0f,1.0f);*/
}

bool Tile::Highlighted() const{
    return hovered || isPath || selected;
}

void Tile::Hover(){
    if(!trespassable) return;
    hovered = true;
    UpdateHighlighter();
}
void Tile::Unhover(){
    hovered = false;
    UpdateHighlighter();
}
void Tile::SetAsPath(){
    isPath = true;
    UpdateHighlighter();
}
void Tile::ClearPath(){
    isPath = false;
    UpdateHighlighter();
}

void Tile::Select(){
    selected = true;
    UpdateHighlighter();
}

void Tile::Deselect(){
    selected = false;
    UpdateHighlighter();
}

int Tile::GetDistance(const Tile& other){
    return glm::abs(GetPos().x - other.GetPos().x)+glm::abs(GetPos().y-other.GetPos().y)+glm::abs(GetPos().z-other.GetPos().z);
}

std::vector<std::shared_ptr<Tile>> Tile::GetNeighbours(const std::map<glm::vec3, std::shared_ptr<Tile>, util::vec3Comparator>& tiles) const{
    std::vector<std::shared_ptr<Tile>> ret;
    for(const glm::vec3& direction : directions){
        try{
            ret.push_back(tiles.at(GetPos()+direction));
        }
        catch(...){}
    }
    return ret;
}

void Tile::Fall(){
    GameObject::Fall();
    highlighter.transform.position.y -= 2.0f;
}

void Tile::Enter(){
    hasPlayer = true;
    try{
        LevelManager::Events.at(transform.position).Trigger();
    }catch(...){}
}

void Tile::Leave(){
    hasPlayer = false;
}
