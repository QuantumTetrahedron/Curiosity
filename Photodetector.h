#ifndef PHOTODETECTOR_H
#define PHOTODETECTOR_H
#include "GameObject.h"
#include "Powered.h"
#include "PowerSource.h"

class Photodetector : public GameObject, public LaserPowered, public PowerSource{
public:
    Photodetector(glm::vec3 powerDest, glm::vec3 position, glm::vec3 rotation, glm::vec3 scale, std::string modelName);

    Photodetector(std::string str);

    std::unique_ptr<GameObject> clone() const override;
    std::unique_ptr<GameObject> CopyAt(glm::vec3 pos, glm::vec3 rot = glm::vec3(0.0), bool safeCopy = false) override;

    void TurnOn(Direction dir) override;
    void TurnOff(Direction dir) override;
};

#endif // PHOTODETECTOR_H
