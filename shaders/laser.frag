#version 330 core

layout (location = 0) out vec4 fragColor;
layout (location = 1) out vec4 bloomColor;

void main(){
	vec3 finalColor = vec3(10.0,0.0,0.0);
	fragColor = vec4(10.0, 0.9, 0.9, 1.0);
    float brightness = dot(finalColor, vec3(0.2126, 0.7152, 0.0722));
    if (brightness > 1.0)
        bloomColor = vec4(finalColor, 1.0);
    else
        bloomColor = vec4(0.0, 0.0, 0.0, 1.0);
}
