#version 330 core

struct DirLight{
	vec3 direction;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

struct SpotLight{
	vec3 position;
	vec3 direction;
	float cutOff;
	float outerCutOff;

	float constant;
	float linear;
	float quadratic;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

struct RayLight{
	vec3 posA;
	vec3 posB;

	float constant;
	float linear;
	float quadratic;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

#define NR_RAY_LIGHTS 8

in vec3 FragPos;
in vec3 Normal;
in vec2 TexCoords;
in vec4 FragPosLightSpace;

layout (location = 0) out vec4 fragColor;
layout (location = 1) out vec4 bloomColor;

//uniform vec3 colorTint;

uniform vec3 viewPos;
uniform DirLight dirLight;
uniform SpotLight spotLight;
uniform RayLight rayLights[NR_RAY_LIGHTS];
uniform int rayLightsToRender;

uniform sampler2D shadowMap;
uniform sampler2D texture_albedo1;
uniform sampler2D texture_metallic1;

float ShadowCalculation(vec4 fragPosLightSpace, SpotLight light);
vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir);
vec3 CalcSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir);
vec3 CalcRayLight(RayLight light, vec3 normal, vec3 fragPos, vec3 viewDir);

void main()
{
	vec3 norm = normalize(Normal);
	vec3 viewDir = normalize(viewPos - FragPos);

	vec3 result = CalcDirLight(dirLight, norm, viewDir);
	
	for(int i=0;i<rayLightsToRender;++i)
		result += CalcRayLight(rayLights[i], norm, FragPos, viewDir);

	result += CalcSpotLight(spotLight, norm, FragPos, viewDir);

    fragColor = vec4(result, 1.0);
    float brightness = dot(result, vec3(0.2126, 0.7152, 0.0722));
    if (brightness > 1.0)
        bloomColor = vec4(result, 1.0);
    else
        bloomColor = vec4(0.0, 0.0, 0.0, 1.0);
}

vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir)
{
	vec3 lightDir = normalize(-light.direction);

	float diff = max(dot(normal, lightDir), 0.0);

	//vec3 reflectDir = reflect(-lightDir, normal);
	//float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);
	vec3 halfwayDir = normalize(lightDir + viewDir);
	float spec = pow(max(dot(normal, halfwayDir), 0.0), 64.0);

	vec3 ambient = light.ambient * vec3(texture(texture_albedo1, TexCoords));
	vec3 diffuse = light.diffuse * diff * vec3(texture(texture_albedo1, TexCoords));// * colorTint;
	vec3 specular = light.specular * spec * vec3(texture(texture_metallic1, TexCoords));
	return ambient+diffuse+specular;
}

vec3 CalcSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
	vec3 lightDir = normalize(light.position - fragPos);

	float diff = max(dot(normal,lightDir),0.0);

	vec3 reflectDir = reflect(-lightDir, normal);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);

	float distance = length(light.position - fragPos);
	float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * distance * distance);

	float theta = dot(lightDir, normalize(-light.direction));
	float epsilon = light.cutOff - light.outerCutOff;
	float intensity = clamp((theta - light.outerCutOff) / epsilon, 0.0, 1.0);

	vec3 ambient = light.ambient * vec3(texture(texture_albedo1, TexCoords));
	vec3 diffuse = light.diffuse * diff * vec3(texture(texture_albedo1, TexCoords));
	vec3 specular = light.specular * spec * vec3(texture(texture_metallic1, TexCoords));
	ambient *= attenuation * intensity;
	diffuse *= attenuation * intensity;
	specular *= attenuation * intensity;
	float shadow = ShadowCalculation(FragPosLightSpace, light);
	return ambient+(1.0-shadow)*(diffuse+specular);
}

vec3 CalcRayLight(RayLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
	vec3 lightPosition;

	float a = length(light.posB-fragPos);
	float b = length(light.posA-fragPos);
	float c = length(light.posA-light.posB);

	float cosAlpha = (b*b+c*c-a*a)/(2*b*c);
	float cosBeta = (a*a+c*c-b*b)/(2*a*c);

	if(cosAlpha <= 0.0)
	{
		lightPosition = light.posA;
	}
	else
	{
		if(cosBeta <= 0.0)
		{
			lightPosition = light.posB;
		}
		else
		{
			lightPosition = light.posA*(1-(cosAlpha*b)/c) + light.posB*(cosAlpha*b)/c;
		}
	}

	vec3 lightDir = normalize(lightPosition - fragPos);
	float diff = max(dot(normal, lightDir), 0.0);

	vec3 reflectDir = reflect(-lightDir, normal);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);

	float distance = length(lightPosition - fragPos);
	float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * distance * distance);

	//vec3 ambient = light.ambient * vec3(texture(texture_albedo1, TexCoords));
	vec3 diffuse = light.diffuse * diff * vec3(texture(texture_albedo1, TexCoords));
	vec3 specular = light.specular * spec * vec3(texture(texture_metallic1, TexCoords));
	//ambient *= attenuation;
	diffuse *= attenuation;
	specular *= attenuation;
	
	return (diffuse+specular);
}

float ShadowCalculation(vec4 fragPosLightSpace, SpotLight light){
    // perform perspective divide
    vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
    // transform to [0,1] range
    projCoords = projCoords * 0.5 + 0.5;
    // get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
    float closestDepth = texture(shadowMap, projCoords.xy).r;
    // get depth of current fragment from light's perspective
    float currentDepth = projCoords.z;
    // check whether current frag pos is in shadow
    float bias = 0.001;
	float shadow = 0.0;
    vec2 texelSize = 1.0 / textureSize(shadowMap, 0);
    for(int x = -1; x <= 1; ++x)
    {
        for(int y = -1; y <= 1; ++y)
        {
            float pcfDepth = texture(shadowMap, projCoords.xy + vec2(x, y) * texelSize).r;
            shadow += currentDepth - bias > pcfDepth  ? 1.0 : 0.0;
        }
    }
    shadow /= 9.0;

	if(projCoords.z > 1.0)
        shadow = 0.0;

    return shadow;
}
