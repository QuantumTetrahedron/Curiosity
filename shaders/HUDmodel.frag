#version 330 core
out vec4 color;

uniform vec3 colorTint;

void main()
{
	color = vec4(colorTint, 1.0);
}