#version 330 core
in vec2 TexCoords;
out vec4 color;

uniform sampler2D fontImage;
uniform vec3 fontColor;

void main() {
	vec4 sampled = vec4(1.0, 1.0, 1.0, texture(fontImage, TexCoords).r);
	color = vec4(fontColor, 1.0) * sampled;
}