#version 330 core

struct DirLight{
	vec3 direction;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

struct SpotLight{
	vec3 position;
	vec3 direction;
	float cutOff;
	float outerCutOff;

	float constant;
	float linear;
	float quadratic;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

struct RayLight{
	vec3 posA;
	vec3 posB;

	float constant;
	float linear;
	float quadratic;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

#define NR_RAY_LIGHTS 8

in vec3 FragPos;
in vec3 Normal;
in vec2 TexCoords;
in vec4 FragPosLightSpace;

layout (location = 0) out vec4 fragColor;
layout (location = 1) out vec4 bloomColor;

uniform vec3 viewPos;
uniform DirLight dirLight;
uniform SpotLight spotLight;
uniform RayLight rayLights[NR_RAY_LIGHTS];
uniform int rayLightsToRender;

uniform bool pbrTest;
uniform bool laserTest;
//uniform vec3 colorTint;

uniform sampler2D shadowMap;
uniform sampler2D texture_albedo1;
uniform sampler2D texture_metallic1;
uniform sampler2D texture_roughness1;
uniform sampler2D texture_normal1;
uniform sampler2D texture_ao1;

float DistributionGGX(vec3 N, vec3 H, float roughness);
float GeometrySchlickGGX(float NdotV, float roughness);
float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness);
vec3 fresnelSchlick(float cosTheta, vec3 F0);

float ShadowCalculation(vec4 fragPosLightSpace, SpotLight light);
vec3 CalcDirLight(DirLight light, vec3 N, vec3 V, vec3 F0);
vec3 CalcSpotLight(SpotLight light, vec3 N, vec3 fragPos, vec3 V, vec3 F0);
vec3 CalcRayLight(RayLight light, vec3 N, vec3 fragPos, vec3 V, vec3 F0);

const float PI = 3.14159265359;
vec3 albedo;
float metallic;
float roughness;
float ao;

void main()
{
	albedo = texture(texture_albedo1, TexCoords).rgb;
	metallic = texture(texture_metallic1, TexCoords).r;
	roughness = texture(texture_roughness1, TexCoords).r;
	ao = texture(texture_ao1, TexCoords).r;

	vec3 N = normalize(Normal);
	vec3 V = normalize(viewPos - FragPos);

	vec3 F0 = vec3(0.04);
	F0 = mix(F0, albedo, metallic);

	vec3 result = CalcDirLight(dirLight, N, V, F0);

	for(int i=0;i<rayLightsToRender;++i)
		result += CalcRayLight(rayLights[i], N, FragPos, V, F0);

	result += CalcSpotLight(spotLight, N, FragPos, V, F0);

	fragColor = vec4(result, 1.0);
    float brightness = dot(result, vec3(0.2126, 0.7152, 0.0722));
    if (brightness > 1.0)
        bloomColor = vec4(result, 1.0);
    else
        bloomColor = vec4(0.0, 0.0, 0.0, 1.0);
}

float DistributionGGX(vec3 N, vec3 H, float roughness){
	float a = roughness * roughness;
	float a2 = a * a;
	float NdotH = max(dot(N, H), 0.0);
	float NdotH2 = NdotH * NdotH;

	float nom = a2;// * a2;
	if(pbrTest) nom = nom * nom;
	float denom = (NdotH2 * (a2 - 1.0) + 1.0);
	if(pbrTest)
		denom = denom * denom;
	else
		denom = PI * denom * denom;
	return nom / max(denom, 0.001);
}

float GeometrySchlickGGX(float NdotV, float roughness)
{
	float r = (roughness + 1.0);
	float k = (r*r) / 8.0;

	float nom = NdotV;
	float denom = NdotV * (1.0 - k) + k;

	return nom / denom;
}

float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
	float NdotV = max(dot(N, V), 0.0);
	float NdotL = max(dot(N, L), 0.0);
	float ggx2 = GeometrySchlickGGX(NdotV, roughness);
	float ggx1 = GeometrySchlickGGX(NdotL, roughness);

	return ggx1 * ggx2;
}

vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
	return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

vec3 CalcDirLight(DirLight light, vec3 N, vec3 V, vec3 F0)
{
	vec3 L = normalize(-light.direction);
	vec3 H = normalize(V+L);

	vec3 radiance = light.diffuse;

	float NDF = DistributionGGX(N, H, roughness);
	float G = GeometrySmith(N, V, L, roughness);
	vec3 F = fresnelSchlick(max(dot(H, V), 0.0), F0);

	vec3 kS = F;
	vec3 kD = vec3(1.0) - kS;
	kD *= 1.0 - metallic;

	vec3 nominator = NDF * G * F;
	float denominator = 4.0 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0);
	vec3 specular = nominator / max(denominator, 0.001);

	float NdotL = max(dot(N, L), 0.0);
	//return (kD * albedo/PI + specular) * radiance * NdotL;
	return (kD * albedo + specular) * radiance * NdotL;
}

vec3 CalcSpotLight(SpotLight light, vec3 N, vec3 fragPos, vec3 V, vec3 F0)
{
	vec3 L = normalize(light.position - fragPos);
	vec3 H = normalize(V+L);
	float distance = length(light.position - fragPos);
	float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * distance * distance);

	float theta = dot(L, normalize(-light.direction));
	float epsilon = light.cutOff - light.outerCutOff;
	float intensity = clamp((theta - light.outerCutOff) / epsilon, 0.0, 1.0);

	vec3 radiance = light.diffuse * attenuation * intensity;

	float NDF = DistributionGGX(N, H, roughness);
	float G = GeometrySmith(N, V, L, roughness);
	vec3 F = fresnelSchlick(max(dot(H, V), 0.0), F0);

	vec3 kS = F;
	vec3 kD = vec3(1.0) - kS;
	kD *= 1.0 - metallic;

	vec3 nominator = NDF * G * F;
	float denominator = 4.0 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0);
	vec3 specular = nominator / max(denominator, 0.001);

	float NdotL = max(dot(N, L), 0.0);
	float shadow = ShadowCalculation(FragPosLightSpace, light);
	//return (1.0 - shadow) * (kD * albedo / PI + specular) * radiance * NdotL;// * colorTint;
	return (1.0 - shadow) * (kD * albedo + specular) * radiance * NdotL;
}

vec3 CalcRayLight(RayLight light, vec3 N, vec3 fragPos, vec3 V, vec3 F0)
{
	vec3 lightPosition;
	if(!laserTest){ /// old
		float a = length(light.posB-fragPos);
		float b = length(light.posA-fragPos);
		float c = length(light.posA-light.posB);

		float cosAlpha = (b*b+c*c-a*a)/(2*b*c);
		float cosBeta = (a*a+c*c-b*b)/(2*a*c);

		if(cosAlpha <= 0.0)
		{
			lightPosition = light.posA;
		}
		else
		{
			if(cosBeta <= 0.0)
			{
				lightPosition = light.posB;
			}
			else
			{
				lightPosition = light.posA*(1-(cosAlpha*b)/c) + light.posB*(cosAlpha*b)/c;
			}
		}
	}
	else{ // new still wrong
		vec3 p1 = light.posA;
		vec3 d1 = normalize(light.posB-light.posA);
		vec3 p2 = fragPos;
		vec3 d2 = normalize(reflect(-V,N));
		
		float t_n = (p1.x-p2.x)*d2.z - (p1.z-p2.z)*d2.x;
		float t_d = d1.z*d2.x-d1.x*d2.z;

		float t1 = t_n/t_d;

		//float t2 = ((p2.x-p1.x)*d1.z - (p2.z-p1.z)*d1.x) / (d2.z*d1.x-d2.x*d1.z);
		//if(t2 < 0){
		//	t1 = length(light.posB-light.posA) - t1;
		//}

		if(t1 > length(light.posB-light.posA)){
			lightPosition = light.posB;	
		}
		else if(t1 < 0){
			lightPosition = light.posA;
		}
		else{
			lightPosition = p1+t1*d1;
		}
	}

	vec3 L = normalize(lightPosition - fragPos);
	//vec3 L = normalize(L2);
	vec3 H = normalize(V+L);
	float distance = length(lightPosition - fragPos);
	float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * distance * distance);
	vec3 radiance = light.diffuse * attenuation;

	float NDF = DistributionGGX(N, H, roughness);
	float G = GeometrySmith(N, V, L, roughness);
	vec3 F = fresnelSchlick(max(dot(H, V), 0.0), F0);

	vec3 kS = F;
	vec3 kD = vec3(1.0) - kS;
	kD *= 1.0 - metallic;

	vec3 nominator = NDF * G * F;
	float denominator = 4.0 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0);
	vec3 specular = nominator / max(denominator, 0.001);

	float NdotL = max(dot(N, L), 0.0);
	//return (kD * albedo / PI + specular) * radiance * NdotL;
	//return (kD * albedo + specular) * radiance * NdotL;
	return specular * radiance * NdotL;
}

float ShadowCalculation(vec4 fragPosLightSpace, SpotLight light)
{
	vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
	projCoords = projCoords * 0.5 + 0.5;
	float closestDepth = texture(shadowMap, projCoords.xy).r;
	float currentDepth = projCoords.z;

	float bias = 0.001;
	float shadow = 0.0;
	vec2 texelSize = 1.0 / textureSize(shadowMap, 0);
	for(int x = -1; x <= 1; ++x)
	{
		for(int y = -1; y <= 1; ++y)
		{
			float pcfDepth = texture(shadowMap, projCoords.xy + vec2(x, y) * texelSize).r;
			shadow += currentDepth - bias > pcfDepth ? 1.0 : 0.0;
		}
	}
	shadow /= 9.0;

	if(projCoords.z > 1.0)
		shadow = 0.0;

	return shadow;
}
