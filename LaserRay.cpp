#include "LaserRay.h"
#include "LevelManager.h"
#include "Powered.h"
#include "Mirror.h"

#define MAX_LASER_LENGTH 50

LaserRay::LaserRay(glm::vec3 position, Direction direction){
    modelName = "ray";
    dir = direction;
    length = 1;
    origin = position;
    oldOrigin = origin;
    dest = glm::vec3(-1);
    markedToDestroy = false;
}

glm::mat4 LaserRay::GetModelMatrix() const{
    glm::mat4 m;
    m = glm::translate(m, origin+glm::vec3(0.0f,2.0f,0.0f));
    float rot = (float)dir;
    if(rot>0.0f)
        m = glm::rotate(m, glm::radians(rot), glm::vec3(0.0f,1.0f,0.0f));
    m = glm::scale(m, glm::vec3(length,1.0f,1.0f));
    return m;
}

void LaserRay::Draw(const Shader& shader) {
    Model& model = ResourceManager::GetModel(modelName);
    glm::mat4 m = GetModelMatrix();
    shader.Use();
    shader.SetMatrix4("model", m);
    model.Draw(shader);
}

glm::vec3 LaserRay::Front() const{
    glm::vec3 ret;
    if(dir == Direction::EAST)
        ret = glm::vec3(1.0f,0.0f,0.0f);
    else if(dir == Direction::NORTH)
        ret = glm::vec3(0.0f,0.0f,-1.0f);
    else if(dir == Direction::WEST)
        ret = glm::vec3(-1.0f,0.0f,0.0f);
    else if(dir == Direction::SOUTH)
        ret = glm::vec3(0.0f,0.0f,1.0f);
    return ret;
}

void LaserRay::Update(){
    ClearTiles();
    glm::vec3 dp = Front()*2.0f;
    glm::vec3 pos = origin;
    float l = 1;
    for(pos += dp; l <= MAX_LASER_LENGTH;pos+=dp){
        if(LevelManager::hasBlockingObject(pos) || l==MAX_LASER_LENGTH)
        {
            oldOrigin = origin;
            length = l;

            if(pos.x != dest.x || pos.y != dest.y || pos.z != dest.z)
            {
                ClearTarget();
                std::shared_ptr<GameObject> obj = LevelManager::GetObject(pos);
                std::shared_ptr<LaserPowered> p = std::dynamic_pointer_cast<LaserPowered>(obj);
                if(p)
                    p->OnLaserEnter(GetDirection());
                dest = pos;
            }
            break;
        }
        else{
            //if(LevelManager::PlayerAt(pos))
            //    LevelManager::state = LevelState::dying;
            glm::vec3 pp = *LevelManager::playerPos;
            pp.y -= 1.5;
            if(pp == pos)
                LevelManager::state = LevelState::dying;

            ++l;
            try{
                if(!markedToDestroy)
                LevelManager::GetTiles()->at(pos)->trespassable = false;
            }catch(...){}
        }
    }
}

void LaserRay::ClearTarget(){
    std::shared_ptr<GameObject> obj(LevelManager::GetObject(dest));
    std::shared_ptr<LaserPowered> old = nullptr;
    if(obj)
        old = std::dynamic_pointer_cast<LaserPowered>(obj);
    if(old)
        old->OnLaserLeave(GetDirection());
}

void LaserRay::ClearTiles(){
    if(length<=1.0f)return;
    glm::vec3 dp = Front()*2.0f;
    glm::vec3 pos = oldOrigin;
    for(pos += dp; pos != dest; pos += dp){
        try{
        if(!LevelManager::hasBlockingObject(pos))
            LevelManager::GetTiles()->at(pos)->trespassable = true;
        }catch(...){}
    }
}

void LaserRay::UpdateShader(Shader& shader, int i){
    shader.Use();
    std::string n = std::to_string(i);
    shader.SetVector3f(("rayLights["+n+"].posA").c_str(), origin.x, origin.y+2.0f, origin.z);
    shader.SetVector3f(("rayLights["+n+"].posB").c_str(), dest.x, dest.y+2.0f, dest.z);
    shader.SetVector3f(("rayLights["+n+"].ambient").c_str(), 0.0f, 0.0f, 0.0f);
    shader.SetVector3f(("rayLights["+n+"].diffuse").c_str(), 3.0f, 0.2f, 0.2f);
    shader.SetVector3f(("rayLights["+n+"].specular").c_str(), 1.0f, 0.2f, 0.2f);
    shader.SetFloat(("rayLights["+n+"].constant").c_str(), 1.0f);
    shader.SetFloat(("rayLights["+n+"].linear").c_str(), 0.5);
    shader.SetFloat(("rayLights["+n+"].quadratic").c_str(), 0.2);
}

Direction LaserRay::GetDirection() const{
    Direction dir;
    glm::vec3 dp = Front();
    if(dp.x > 0.1)
        dir = Direction::WEST;
    else if(dp.x < -0.1)
        dir = Direction::EAST;
    else if(dp.z > 0.1)
        dir = Direction::NORTH;
    else
        dir = Direction::SOUTH;
    return dir;
}
