#include "HUDelement.h"

HUDelement::HUDelement(std::string texture, float width, float height, int x, int y, int z, RelativePos anchor, RelativePos pivot, glm::vec3 tint)
    : texture(texture), xPos(x), yPos(y), zIndex(z), width(width), height(height), anchor(anchor), pivot(pivot), colorTint(tint){}

glm::vec2 HUDelement::CalculateScreenPos(float WinWidth, float WinHeight) const {
    glm::vec2 retPos = glm::vec2(xPos, -yPos);
    if(pivot == RelativePos::TOP || pivot == RelativePos::CENTER || pivot == RelativePos::BOTTOM)
        retPos.x -= width/2;
    else if(pivot == RelativePos::RIGHT || pivot == RelativePos::TOP_RIGHT || pivot == RelativePos::BOTTOM_RIGHT)
        retPos.x -= width;

    if(pivot == RelativePos::LEFT || pivot == RelativePos::CENTER || pivot == RelativePos::RIGHT)
        retPos.y -= height/2;
    else if(pivot == RelativePos::BOTTOM || pivot == RelativePos::BOTTOM_LEFT || pivot == RelativePos::BOTTOM_RIGHT)
        retPos.y -= height;

    if(anchor == RelativePos::TOP || anchor == RelativePos::CENTER || anchor == RelativePos::BOTTOM)
        retPos.x += WinWidth/2;
    else if(anchor == RelativePos::RIGHT || anchor == RelativePos::TOP_RIGHT || anchor == RelativePos::BOTTOM_RIGHT)
        retPos.x += WinWidth;

    if(anchor == RelativePos::LEFT || anchor == RelativePos::CENTER || anchor == RelativePos::RIGHT)
        retPos.y += WinHeight/2;
    else if(anchor == RelativePos::BOTTOM || anchor == RelativePos::BOTTOM_LEFT || anchor == RelativePos::BOTTOM_RIGHT)
        retPos.y += WinHeight;

    return retPos;
}

void HUDelement::Draw(int WinWidth, int WinHeight, const SpriteRenderer& renderer) const{
    renderer.DrawSprite(ResourceManager::GetTexture(texture), CalculateScreenPos(WinWidth, WinHeight), glm::vec2(width, height), 0.0f, colorTint);
}
