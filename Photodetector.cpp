#include "Photodetector.h"
#include "LevelManager.h"
#include <regex>

Photodetector::Photodetector(glm::vec3 powerDest, glm::vec3 position, glm::vec3 rotation, glm::vec3 scale, std::string modelName)
: GameObject(position, rotation, scale, modelName, true, true, false, false), PowerSource(powerDest){
    powerTarget = powerDest;
}

Photodetector::Photodetector(std::string str){
    std::regex pattern{R"((\w{1})\s+(\S+)\s+(\S+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+))"};
    std::smatch matches;
    if(std::regex_match(str, matches, pattern)){
        std::string name = matches[3];
        std::string type = matches[2];

        int x = atoi(((std::string)matches[4]).c_str());
        int y = atoi(((std::string)matches[5]).c_str());
        int z = atoi(((std::string)matches[6]).c_str());
        int r = atoi(((std::string)matches[7]).c_str());
        int dx = atoi(((std::string)matches[8]).c_str());
        int dy = atoi(((std::string)matches[9]).c_str());
        int dz = atoi(((std::string)matches[10]).c_str());

        *this = Photodetector(glm::vec3(2*dx,dy,2*dz), glm::vec3(2*x, y, 2*z), glm::vec3(0,90*r,0), glm::vec3(1.0f), name);
    }
}

std::unique_ptr<GameObject> Photodetector::clone() const{
    std::unique_ptr<Photodetector> n = std::make_unique<Photodetector>(*this);
    n->connected = false;
    return n;
}

std::unique_ptr<GameObject> Photodetector::CopyAt(glm::vec3 pos, glm::vec3 rot, bool safeCopy) {
    std::unique_ptr<Photodetector> obj(dynamic_cast<Photodetector*>(GameObject::CopyAt(pos, rot, safeCopy).release()));
    obj->connected = false;
    return obj;
}

void Photodetector::TurnOn(Direction dir) {
    if(dir == Direction::WEST+transform.rotation.y)
    {
        std::cout << "giving power" << std::endl;
        GivePower();
    }
}

void Photodetector::TurnOff(Direction dir) {
    if(dir == Direction::WEST+transform.rotation.y)
    {
        std::cout << "cutting power" << std::endl;
        CutPower();
    }
}
