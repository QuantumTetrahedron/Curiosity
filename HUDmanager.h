#ifndef HUD_MANAGER_H
#define HUD_MANAGER_H

#include <vector>
#include <map>
#include "HUDelement.h"
#include "Utility.h"

/**
* HUD manager
* Manages and renders HUD elements
*/
class HUDmanager{
public:
    /**
    * Initializes the manager
    * params:
    * * width : width of the window.
    * * height : height of the window.
    */
    void Init(int width, int height);

    /**
    * Renders the stored elements.
    */
    void Render();

    /**
    * Adds a new element.
    * params:
    * * element : new element to add.
    */
    void AddElement(std::shared_ptr<HUDelement> element);

    /**
    * Clears the container.
    */
    void Clear();
private:
    util::sorted_vector<std::shared_ptr<HUDelement>, HUDelement::comparator> elements;
    SpriteRenderer renderer;

    int windowWidth, windowHeight;
};

#endif
