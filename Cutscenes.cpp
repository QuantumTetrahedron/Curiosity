#ifdef __linux__

#include "Cutscenes.h"


VLC::Instance Cutscenes::instance;
VLC::Media Cutscenes::media;
VLC::MediaPlayer Cutscenes::mediaPlayer;
GLFWwindow* Cutscenes::window;
bool Cutscenes::playing;
std::string Cutscenes::loadedFilePath;

void Cutscenes::SetWindow(GLFWwindow* win){
    window = win;
    loadedFilePath = "";
}

void Cutscenes::Load(std::string path){
    if(loadedFilePath == path){
        std::cout << "Already Loaded" << std::endl;
        return;
    }
    if(!window){
        std::cout << "Error - no window set for cutscenes" << std::endl;
        return;
    }
    loadedFilePath = path;
    playing = false;
    instance = VLC::Instance(0, nullptr);
    media = VLC::Media(instance, path.c_str(), VLC::Media::FromPath);
    mediaPlayer = VLC::MediaPlayer(media);
    #ifdef __linux__
    mediaPlayer.setXwindow(glfwGetX11Window(window));
    #else
    /// jeden z tych chyba
    //mediaPlayer.setWin32Window(glfwGetWin32Window(window));
    mediaPlayer.setHwnd(glfwGetWin32Window(window));
    #endif
}

void Cutscenes::Update(){
    if(!mediaPlayer.isPlaying() && playing)
    {
        Stop();
    }
}

void Cutscenes::Play(){
    mediaPlayer.play();
    playing = true;
    while(!mediaPlayer.isPlaying()){}
}
void Cutscenes::Stop(){
    mediaPlayer.stop();
    playing = false;
    //while(mediaPlayer.isPlaying()){}
}

bool Cutscenes::isPlaying(){
    return playing;
}


#endif
