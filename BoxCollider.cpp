#include "BoxCollider.h"

BoxCollider::BoxCollider(glm::vec3 pos, float _height, bool _floor)
{
    height = _height;
    floor = _floor;
    position = pos;
    if(floor)
        position.y -= height;
}

bool BoxCollider::Collided(glm::vec3 point) const {
    return Between(point.x, position.x-1.0f, position.x+1.0f)
        && Between(point.y, position.y, position.y+height)
        && Between(point.z, position.z-1.0f, position.z+1.0f);
}

bool BoxCollider::Between(double value, double leftBound, double rightBound) const {
    if(leftBound<rightBound)
        return value >= leftBound && value <= rightBound;
    else
        return value <= leftBound && value >= rightBound;
}
