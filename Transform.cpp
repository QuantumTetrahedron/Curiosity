#include "Transform.h"

Direction operator+(Direction dir1, Direction dir2){
    int s = (int)dir1+(int)dir2;
    return (Direction)(s>=360 ? s-360 : s);
}
Direction operator+(Direction dir, int r){
    int s = (int)dir+r;
    return (Direction)(s>=360 ? s-360 : s);
}
Direction operator-(Direction dir1, Direction dir2){
    int s = (int)dir1-(int)dir2;
    return (Direction)(s>=0 ? s : s+360);
}
Direction operator-(Direction dir, int r){
    int s = (int)dir-r;
    return (Direction)(s>=0 ? s : s+360);
}

bool Transform::operator==(const Transform& other) const {
    return position == other.position && rotation == other.rotation && scale == other.scale;
}

bool Transform::operator!=(const Transform& other) const {
    return position != other.position || rotation != other.rotation || scale != other.scale;
}

void Transform::AddRotation(glm::vec3 rot){
    rotation += rot;
    while(rotation.x < 0) rotation.x += 360;
    while(rotation.x >=360) rotation.x -= 360;
    while(rotation.y < 0) rotation.y += 360;
    while(rotation.y >=360) rotation.y -= 360;
    while(rotation.z < 0) rotation.z += 360;
    while(rotation.z >=360) rotation.z -= 360;
}

Direction Transform::GetDirection() const {
    return (Direction)(rotation.y);
}
