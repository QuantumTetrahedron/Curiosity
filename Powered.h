#ifndef POWERED_H
#define POWERED_H
#include "LaserRay.h"

/**
* Interfaces for interaction with in game objects.
*/

class ElectricityPowered{
public:
    virtual ~ElectricityPowered(){}

    ElectricityPowered(int required_power = 0)
    :powerLevel(0), powerRequired(required_power){}

    virtual void TurnOn() = 0;
    virtual void TurnOff() = 0;

    virtual void PowerUp(){
        ChangePower(powerLevel+1);
    }
    virtual void PowerDown(){
        ChangePower(powerLevel-1);
    }

    virtual void ChangePower(int new_power){
        if(powerLevel == powerRequired && new_power < powerLevel)
            TurnOff();
        powerLevel = new_power;
        if(powerLevel == powerRequired){
            TurnOn();
        }
    }
protected:
    int powerLevel;
    int powerRequired;
};

class LaserPowered{
public:
    virtual ~LaserPowered(){}
    LaserPowered():laserCount(0) {}

    virtual void TurnOn(Direction dir) = 0;
    virtual void TurnOff(Direction dir) = 0;

    virtual void OnLaserEnter(Direction dir) {
        laserCount++;
        TurnOn(dir);
    }
    virtual void OnLaserLeave(Direction dir) {
        if(laserCount>0){
            laserCount--;
            TurnOff(dir);
        }
    }
    virtual bool isActive() {return laserCount>0;}
protected:
    int laserCount;
};

class Ferromagnetic{
public:
    virtual ~Ferromagnetic(){}
    virtual void Attract(float fromRot) = 0;
    virtual void Repulse(float fromRot) = 0;

    void AddForce(glm::vec3 toAdd){
        force += toAdd;

        if(force.x >= 2)
            force.x = 2;
        else if(force.x <= -2)
            force.x = -2;
        else
            force.x = 0;


        if(force.y >= 2)
            force.y = 2;
        else if(force.y <= -2)
            force.y = -2;
        else
            force.y = 0;


        if(force.z >= 2)
            force.z = 2;
        else if(force.z <= -2)
            force.z = -2;
        else
            force.z = 0;
    }
protected:
    glm::vec3 force;
};

#endif // POWERED_H
