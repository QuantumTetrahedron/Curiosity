
#include "Renderer.h"
#include <algorithm>
#include <functional>

void Renderer::DrawTiles(const std::vector<std::shared_ptr<Tile>>& objects,const Shader& shader){
    shader.Use();

    std::set<std::string> modelNames = GetModelNames();

    for(std::string name : modelNames)
        renderers[name].DrawInstanced(objects, shader);

    std::vector<std::shared_ptr<Tile>> highlighted;
    std::copy_if(std::begin(objects), std::end(objects), std::back_inserter(highlighted), [](const auto& t){return t->Highlighted();});
    std::vector<GameObject> highlighters;
    std::transform(std::begin(highlighted), std::end(highlighted), std::back_inserter(highlighters), [](auto t){return t->highlighter;});

    for(GameObject& obj : highlighters)
    {
        obj.Draw(ResourceManager::GetShader("simple"));
    }
}

std::set<std::string> Renderer::GetModelNames(){
    std::set<std::string> ret;
    std::transform(std::begin(ResourceManager::Models), std::end(ResourceManager::Models), std::inserter(ret, std::begin(ret)), [](const auto& p){return p.first;});

    return ret;
}

void Renderer::Init(){
    std::set<std::string> names = GetModelNames();

    for(std::string name : names)
        AddModelRenderer(name);
}

void Renderer::Clear(){
    renderers.clear();
}

void Renderer::AddModelRenderer(std::string modelName){
    try{
        renderers.at(modelName);
        return;
    }
    catch(...)
    {
        renderers[modelName] = ModelRenderer(modelName);
    }
}

ModelRenderer::ModelRenderer(std::string modelName){
    model = modelName;
    current_size = 0;
    initialized=false;
}

void ModelRenderer::InitRenderData(const std::vector<glm::mat4>& objects){
    glGenBuffers(1, &buffer);
    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glBufferData(GL_ARRAY_BUFFER, objects.size()*sizeof(glm::mat4), &objects[0], GL_STATIC_DRAW);
    Model& m = ResourceManager::GetModel(model);
    for(GLuint i=0; i<m.meshes.size();++i){
        GLuint VAO = m.meshes[i].VAO;
        glBindVertexArray(VAO);

        glEnableVertexAttribArray(3);
        glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (GLvoid*)0);
        glEnableVertexAttribArray(4);
        glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (GLvoid*)(sizeof(glm::vec4)));
        glEnableVertexAttribArray(5);
        glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (GLvoid*)(2 * sizeof(glm::vec4)));
        glEnableVertexAttribArray(6);
        glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (GLvoid*)(3 * sizeof(glm::vec4)));

        glVertexAttribDivisor(3, 1);
        glVertexAttribDivisor(4, 1);
        glVertexAttribDivisor(5, 1);
        glVertexAttribDivisor(6, 1);

        glBindVertexArray(0);
    }

}
