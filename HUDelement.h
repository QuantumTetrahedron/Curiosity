#ifndef HUD_ELEMENT_H
#define HUD_ELEMENT_H

#include <glm/glm.hpp>
#include "Renderer.h"

enum class RelativePos{
    CENTER,
    LEFT,
    RIGHT,
    TOP,
    BOTTOM,
    TOP_LEFT,
    TOP_RIGHT,
    BOTTOM_LEFT,
    BOTTOM_RIGHT
};

/**
* HUD Element
*/
class HUDelement{
public:
    virtual ~HUDelement(){}
    HUDelement() : zIndex(-1){}

    /// Constructor
    HUDelement(std::string texture, float width, float height, int x, int y, int z, RelativePos anchor, RelativePos pivot, glm::vec3 tint = glm::vec3(1.0f));

    /**
    * Draws the element's sprite.
    * params:
    * * WinWidth : width of the window.
    * * WinHeight : height of the window.
    * * renderer : sprite renderer to draw with.
    */
    virtual void Draw(int WinWidth, int WinHeight, const SpriteRenderer& renderer) const;

    int zIndex;
    /**
    * A struct to use in sorting functions.
    */
    struct comparator{
        bool operator()(const std::shared_ptr<HUDelement>& first, const std::shared_ptr<HUDelement>& second) const{
            return first->zIndex < second->zIndex;
        }
    };

    int xPos, yPos;
    std::string texture;
protected:
    float width, height;
    RelativePos anchor;
    RelativePos pivot;
    glm::vec3 colorTint;

    /**
    * Calculates the element's position on the screen
    * based on the anchor, pivot and relative position.
    * params:
    * * WinWidth : width of the window.
    * * WinHeight : height of the window.
    * returns:
    * * A 2D vector of screen space coordinates.
    */
    glm::vec2 CalculateScreenPos(float WinWidth, float WinHeight) const;
};

#endif
