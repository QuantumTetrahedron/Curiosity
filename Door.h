#ifndef DOOR_H
#define DOOR_H
#include <string>
#include <regex>
#include "Powered.h"
#include "Transform.h"
#include "GameObject.h"

/** Door class
* An electrically operated door.
* Open only if powered with enough energy.
*/
class Door : public GameObject, public ElectricityPowered {
public:
    /// Copy constructor
    Door(const Door& other);

    virtual std::unique_ptr<GameObject> clone() const;

    /** Constructor
    * params:
    * * reqPower : power required to open the door.
    * * position : door's position.
    * * rotation : door's rotation.
    * * scale : door's scale.
    * * modelName : name of the model to render.
    * * tint : color tint
    */
    Door(int reqPower,glm::vec3 position, glm::vec3 rotation, glm::vec3 scale, std::string modelName);

    /**
    * Regex constructor
    * params:
    * * str : string to parse
    */
    Door(std::string str);

    void Update(double dt) override;
    void Draw(const Shader& shader) override;

    /**
    * Opens the door when turned on.
    */
    void TurnOn() override;

    /**
    * Closes the door when turned off.
    */
    void TurnOff() override;

    /**
    * Opens the door.
    * The door opens by sliding down.
    */
    void Open();

    /**
    * Closes the door.
    * The door closes by sliding back up.
    */
    void Close();

private:
    bool open;
    float offset;
};

#endif // DOOR_H
