#ifndef RADIAL_H
#define RADIAL_H

#include "HUDelement.h"

/**
* A HUD element filled radially based on its completion parameter.
*/
class Radial : public HUDelement{
public:
    /// Constructors
    Radial(){}
    Radial(std::string texture, float width, float height, int x, int y, int z, RelativePos anchor, RelativePos pivot, glm::vec3 tint = glm::vec3(1.0f));

    /// Draw overloaded.
    void Draw(int windowW, int windowH, const SpriteRenderer& renderer) const override;

    /// Sets the completion parameter.
    void SetCompletion(float c);

private:
    float completion;
};

#endif // RADIAL_H
