#ifndef MAGNET_H
#define MAGNET_H
#include "GameObject.h"
#include "Powered.h"
#include "LevelManager.h"

class Magnet : public GameObject, public ElectricityPowered{
public:
    Magnet(int requiredPower, glm::vec3 position, glm::vec3 rotation, glm::vec3 scale, std::string modelName);

    Magnet(std::string str);

    std::unique_ptr<GameObject> clone() const override;

    enum class Mode{ PULL, PUSH };
    void TurnOn() override;
    void TurnOff() override;

    void Magnetize(Mode mode);

    glm::vec3 Front() const;
};

#endif // MAGNET_H
