#ifndef RESOURCE_MANAGER_H
#define RESOURCE_MANAGER_H

#include "Model.h"
#include "Texture.h"

#include <map>
#include <string>

class ResourceManager{
public:
    static std::map<std::string, Shader> Shaders;
    static std::map<std::string, Model  > Models;
    static std::map<std::string, Texture2D> Textures;

    static Shader& LoadShader(const GLchar* vertFile, const GLchar* fragFile, std::string name);
    static Shader& GetShader(std::string name);

    static Model& LoadModel(const GLchar* file, std::string name, float height);
    static Model& GetModel(std::string name);

    static Texture2D& LoadTexture(const GLchar* file, GLboolean alpha, std::string name);
    static Texture2D& GetTexture(std::string name);

    static void Clear();
private:
    ResourceManager(){}
};

#endif
