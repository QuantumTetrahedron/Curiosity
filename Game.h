#ifndef GAME_H
#define GAME_H

#include <vector>
#include <map>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "TextRenderer.h"
#include "Menu.h"
#include "Level.h"
#include "Input.h"

enum class GameState{
    mainMenu,
    inGame,
    loading,
    cutscene
};

/**
* Main game class
*/
class Game{
public:

    GameState state;
    Menu menu;

    Game(unsigned int w, unsigned int h);

    /**
    * Initializes the game.
    * Loads the necessary shaders and fonts.
    */
    void Init();

    /**
    * Main loop functions.
    * Responsible for processing input from the player,
    * updating necessary game objects and rendering.
    */
    void ProcessInput(Input& in);
    void Update(GLfloat dt);
    void Render();

    unsigned int width, height;

    void PrintFPS(int framesCount){
        level.textRenderer->Render(std::to_string(framesCount), 10.0f, 10.0f, 1.0, glm::vec3(1.0f, 0.5f, 1.0f));
    }
private:
    Level level;
};

#endif
