#include "Laser.h"
#include "LevelManager.h"
#include "LaserRay.h"

#include <regex>

void Laser::CleanUp() {
    TurnOff();
}

std::unique_ptr<GameObject> Laser::clone() const {
    std::unique_ptr<Laser> n = std::make_unique<Laser>(*this);
    n->currentPower = 0;
    n->active = false;
    n->ray = nullptr;
    return n;
}

std::unique_ptr<GameObject> Laser::CopyAt(glm::vec3 pos, glm::vec3 rot, bool safeCopy) {
    std::unique_ptr<Laser> obj(dynamic_cast<Laser*>( GameObject::CopyAt(pos, rot, safeCopy).release() ));
    obj->requiredPower = requiredPower;
    obj->active = false;
    obj->currentPower = 0;
    obj->ray = nullptr;
    if(!safeCopy)
        obj->ChangePower(0);
    return obj;
}

Laser::Laser(int reqPower,glm::vec3 position, glm::vec3 rotation, glm::vec3 scale, std::string modelName)
    : GameObject(position, rotation, scale, modelName, true, true, false), ElectricityPowered(reqPower){
    requiredPower = reqPower;
    currentPower = 0;
    active = false;
    ray = nullptr;
    ChangePower(0);
}

Laser::Laser(std::string str){
    std::regex pattern{R"((\w{1})\s+(\S+)\s+(\S+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+))"};
    std::smatch matches;
    if(std::regex_match(str, matches, pattern)){
        std::string name = matches[3];
        std::string type = matches[2];

        int x = atoi(((std::string)matches[4]).c_str());
        int y = atoi(((std::string)matches[5]).c_str());
        int z = atoi(((std::string)matches[6]).c_str());
        int r = atoi(((std::string)matches[7]).c_str());
        int p = atoi(((std::string)matches[8]).c_str());

        *this = Laser(p, glm::vec3(2*x, y, 2*z), glm::vec3(0,90*r,0), glm::vec3(1.0f), name);
    }
}

void Laser::TurnOn(){
    active = true;
    ray = LevelManager::RegisterLaserRay(transform.position, transform.GetDirection());
}

void Laser::TurnOff(){
    active = false;
    if(ray){
        LevelManager::EraseLaserRay(ray->origin);
        ray = nullptr;
    }
}

void Laser::Update(double dt){
    GameObject::Update(dt);
    if(ray && ray->origin != transform.position){
        ray->origin = transform.position;
    }
}
