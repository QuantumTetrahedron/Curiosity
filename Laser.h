#ifndef LASER_H
#define LASER_H

#include "Powered.h"
#include "LaserRay.h"
#include "GameObject.h"

/**
* An electrical laser emitter.
*/
class Laser : public GameObject, public ElectricityPowered{
public:
    /**
    * Turns off the laser before deleting the object.
    */
    void CleanUp() override;

    virtual std::unique_ptr<GameObject> clone() const;

    /**
    * Generates the laser's copy at a given location.
    * params:
    * * pos : target position.
    * * rot : target rotation.
    * * safeCopy : if set to true, the copied element will not generate anything besides itself.
    * *            Should be set to true if the copy is not placed in a level.
    * returns:
    * * A pointer to the object's copy.
    */
    std::unique_ptr<GameObject> CopyAt(glm::vec3 pos, glm::vec3 rot = glm::vec3(0.0f), bool safeCopy = false) override;

    /**
    * Constructor.
    * params:
    * * reqPower : power required to emit the laser.
    * * position : laser's position.
    * * rotation : laser's rotation.
    * * scale : laser's scale.
    * * modelName : name of the model to render.
    * * tint : color tint.
    */
    Laser(int reqPower,glm::vec3 position, glm::vec3 rotation, glm::vec3 scale, std::string modelName);

    /**
    * Regex constructor
    * params:
    * * str : string to parse
    */
    Laser(std::string str);

    /**
    * Turns the laser on.
    * Creates a laser ray object in level.
    */
    void TurnOn();

    /**
    * Turns the laser off.
    * Removes the associated laser ray object from the level.
    */
    void TurnOff();

    /**
    * Updates the position of the emitted laser ray.
    */
    void Update(double dt);

private:
    int requiredPower;
    int currentPower;
    bool active;
    std::shared_ptr<LaserRay> ray;
};

#endif // LASER_H
