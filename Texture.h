#ifndef TEXTURE_H
#define TEXTURE_H


#include <glad/glad.h>

class Texture2D{
public:
    GLuint ID;
    GLuint width,height;
    GLuint format;

    GLuint wrapS, wrapT, filterMin, filterMax;

    Texture2D();

    /**
    * Constructor.
    * params:
    * * file : path to the image file.
    * * alpha : does the image have an alpha channel.
    */
    Texture2D(const GLchar* file, bool alpha);

    /**
    * Binds the texture.
    */
    void Bind() const;

private:
    /**
    * Generates the texture.
    * params:
    * * width : texture's width.
    * * height : texture's height.
    * * data : data loaded from file.
    */
    void Generate(unsigned int width, unsigned int height, unsigned char* data);
};

#endif
