#include "ConnectionRay.h"

ConnectionRay::ConnectionRay(glm::vec3 _origin, glm::vec3 _dest) {
    origin = _origin;
    dest = _dest;
    modelName = "ray";
    length = glm::distance(dest, origin);
    if (origin.x == dest.x && origin.z == dest.z) {
        //pionowo
        rotation.y = 0.0f;
        rotation.x = 0.0f;
        rotation.z = 90.0f;
    }
    else {
        glm::vec3 dir = dest - origin;
        float angle = glm::degrees(glm::acos(dir.x / length));
        if (dir.z < 0) angle = 360.0f - angle;
        rotation.y = angle;
        rotation.x = 0.0f;
        rotation.z = 0.0f;
    }
}

void ConnectionRay::Draw(const Shader& shader) {
    Model& model = ResourceManager::GetModel(modelName);
    glm::mat4 m = GetModelMatrix();
    shader.Use();
    shader.SetMatrix4("model", m);
    model.Draw(shader);
}

glm::mat4 ConnectionRay::GetModelMatrix() const{
    glm::mat4 m;
    m = glm::translate(m, origin);
    if(glm::length(rotation)>0.0f)
    {
        m = glm::rotate(m, glm::radians(rotation.y), glm::vec3(0.0f,-1.0f,0.0f));
        m = glm::rotate(m, glm::radians(rotation.z), glm::vec3(0.0f,0.0f,1.0f));
        m = glm::rotate(m, glm::radians(rotation.x), glm::vec3(1.0f,0.0f,0.0f));
    }

    m = glm::scale(m, glm::vec3(length / 2.0f, 1.0f, 1.0f));
    return m;

}
