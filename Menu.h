#ifndef MENU_H_INCLUDED
#define MENU_H_INCLUDED

#include "Button.h"
#include <memory>

class Menu {
public:
    Menu() {}

    void Init(){
        renderer = std::make_shared<SpriteRenderer>();
        renderer->initRenderData();
    }
    void Draw() {
        for (const auto& b : buttons) {
            b.Draw();
        }
    }
    void Activate() {
        for (auto& b : buttons) {
            b.active = true;
        }
    }
    void Close() {
        for (auto& b : buttons) {
            b.active = false;
        }
    }

    void setFont(const std::shared_ptr<TextRenderer>& textRenderer) {text = textRenderer;}

    //TODO: init buttons in menu class
    std::vector<Button> buttons;
    std::shared_ptr<SpriteRenderer> renderer;
    std::shared_ptr<TextRenderer> text;
};


#endif // MENU_H_INCLUDED
