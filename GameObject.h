#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H

#include "Model.h"
#include "Transform.h"
#include "BoxCollider.h"
#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include<string>
#include<experimental/optional>
#include<memory>

class GameObject{
public:
    virtual ~GameObject(){}
    virtual void CleanUp(){}

    ///Constructors
    GameObject(){}
    GameObject(glm::vec3 position, glm::vec3 rotation, glm::vec3 scale, std::string modelName, bool addCollider = true, bool scannable = true, bool canEnter = true, bool isFloor = false);

    Transform transform;
    Transform target;
    std::string modelName;

    std::experimental::optional<BoxCollider> collider;

    /**
    * Clones the object.
    * returns:
    * * A copy of the object.
    */
    virtual std::unique_ptr<GameObject> clone() const;

    /**
    * Generates a copy of the object at a given position
    * and a given rotation.
    * params:
    * * pos : target position.
    * * rot : target rotation.
    * * safeCopy : if set to true, the copied element will not generate anything besides itself.
    * *            Should be set to true if the copy is not placed in a level.
    * returns:
    * * A pointer to the object's copy.
    */
    virtual std::unique_ptr<GameObject> CopyAt(glm::vec3 pos, glm::vec3 rot = glm::vec3(0.0f), bool safeCopy = false);

    /**
    * Renders the object.
    * params:
    * * shader : The shader to use.
    */
    virtual void Draw(const Shader& shader);

    /**
    * Updates the object's state.
    * params:
    * * dt : delta time.
    */
    virtual void Update(double dt);

    /** TODO: change to Move(vec3) **/
    /**
    * Forces the object to fall down.
    */
    virtual void Fall();

    /**
    * Gets the model matrix.
    * returns:
    * * the model matrix.
    */
    glm::mat4 GetModelMatrix() const;

    /**
    * Private field getters.
    */
    inline bool IsBlocking() const {return blocking;}
    inline bool CanScan() const {return scannable;}

    void Relocate();
protected:
    bool scannable;
    bool blocking;
    glm::vec3 inLevelPos;
};

#endif
