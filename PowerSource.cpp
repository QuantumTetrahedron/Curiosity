#include "PowerSource.h"
#include "LevelManager.h"


PowerSource::PowerSource():connected(false){}
PowerSource::PowerSource(glm::vec3 target):powerTarget(target),connected(true){}
glm::vec3 PowerSource::GetPowerTarget() {return powerTarget;}
void PowerSource::GivePower() {
    if(connected){
        std::shared_ptr<GameObject> obj = LevelManager::GetObject(powerTarget);
        if(obj)
            std::dynamic_pointer_cast<ElectricityPowered>(obj)->PowerUp();
    }
}
void PowerSource::CutPower() {
    if(connected){
        std::shared_ptr<GameObject> obj = LevelManager::GetObject(powerTarget);
        if(obj){
            std::dynamic_pointer_cast<ElectricityPowered>(obj)->PowerDown();
        }
    }
}
