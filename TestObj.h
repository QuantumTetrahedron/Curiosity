#ifndef TESTOBJ_H
#define TESTOBJ_H
#include <string>
#include <iostream>
#include <regex>
#include<memory>
#include "Tile.h"
#include "Powered.h"

/**
* TODO : Replace with Crate class.
*/

class TestObj : public Tile, public Ferromagnetic{
public:
    std::unique_ptr<GameObject> clone() const;
    std::unique_ptr<GameObject> CopyAt(glm::vec3 pos, glm::vec3 rot = glm::vec3(0.0f), bool safeCopy = false) override;

    TestObj(glm::vec3 position, glm::vec3 rotation, glm::vec3 scale, std::string modelName, glm::vec3 tint = glm::vec3(1.0f),
        bool addCollider = true, bool canEnter = true);

    TestObj(std::string str);

    void Activate();

    void Attract(float fromRot) override;

    void Repulse(float fromRot) override;

    void Update(double dt) override;

    virtual void Fall() override;
private:
    bool CheckCollision();
    bool moving;
};

#endif // TESTOBJ_H
