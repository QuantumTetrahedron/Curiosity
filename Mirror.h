#ifndef MIRROR_H
#define MIRROR_H

#include "GameObject.h"
#include "Powered.h"
#include "LevelManager.h"

#include <regex>

class Mirror : public GameObject, public LaserPowered{
public:
    void TurnOn(Direction dir) override;

    void TurnOff(Direction dir) override;

    std::unique_ptr<GameObject> clone() const override;

    std::unique_ptr<GameObject> CopyAt(glm::vec3 pos, glm::vec3 rot = glm::vec3(0.0f), bool safeCopy = false) override;

    void CleanUp() override;

    Mirror(glm::vec3 position, glm::vec3 rotation, glm::vec3 scale, std::string modelName);

    Mirror(std::string str);

private:
    std::shared_ptr<LaserRay> reflection;
};

#endif // MIRROR_H
