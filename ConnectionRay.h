#ifndef CONNECTION_RAY_H
#define CONNECTION_RAY_H
#include "Model.h"
#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <string>
#include "ResourceManager.h"

class ConnectionRay {
public:
    glm::vec3 origin;
    glm::vec3 dest;
    glm::vec3 rotation;
    float length;
    ConnectionRay(glm::vec3 _origin, glm::vec3 _dest);
    void Draw(const Shader& shader);

private:
    glm::mat4 GetModelMatrix() const;

    std::string modelName;
};

#endif // CONNECTIONRAY_H_INCLUDED
