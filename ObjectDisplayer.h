#ifndef OBJECT_DISPLAYER_H
#define OBJECT_DISPLAYER_H

#include "GameObject.h"

/**
* Displays a miniature object.
*/
class ObjectDisplayer{
public:
    /**
    * Initializes the displayer.
    * params:
    * * wndW : width of the window.
    * * wndH : height of the window.
    */
    void Init(int wndW, int wndH);

    /**
    * Displays the object.
    * params:
    * * obj : the object to display.
    * * xPos : screen position on the x axis.
    * * yPos : screen position on the y axis.
    * * viewportW : width of the miniature.
    * * viewportH : height of the miniature.
    * * shader : shader to use.
    * * tint : color tint for the shader.
    */
    void Display(std::shared_ptr<GameObject> obj, int xPos, int yPos, int viewportW, int viewportH, Shader& shader, glm::vec3 tint = glm::vec3(-1.0));
private:
    int windowW, windowH;

    /**
    * Gets the necessary view matrix based on object's height.
    * params:
    * * height : the object's height.
    * returns:
    * * The view matrix.
    */
    glm::mat4 getView(float height);

    /**
    * Gets the height of the object's model.
    * params:
    * * obj : object to check.
    * returns:
    * * Height of the given object.
    */
    float getHeight(std::shared_ptr<GameObject> obj);
};

#endif // OBJECT_DISPLAYER_H
