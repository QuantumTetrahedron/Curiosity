#include "Input.h"
#include <iostream>

void KeyListener::RegisterKey(int key){
    keyHoldTimes[key] = 0;
    registered.insert(key);
}

void KeyListener::UpdateKey(int key, float deltaTime){
    try{
        keyHoldTimes.at(key) += deltaTime;
    }
    catch(...){
        std::cout << "key not registered!" << std::endl;
    }
}

void KeyListener::ResetKey(int key){
    try{
        keyHoldTimes.at(key) = 0;
    }catch(...){
        std::cout << "key not registered!" << std::endl;
    }
}

void KeyListener::Reset(){
    for(auto& p : keyHoldTimes)
        p.second = 0;
}

float KeyListener::GetHoldTime(int key){
    try{
        return keyHoldTimes.at(key);
    }catch(...){
        std::cout << "key not registered!" << std::endl;
        return 0.0f;
    }
}

const std::set<int>& KeyListener::GetRegisteredKeys(){
    return registered;
}

void Input::ResetOffsets() {
    scrollOffset = 0;
}

void Input::UpdateKeyListener(float deltaTime){
    for(int i : keyListener.GetRegisteredKeys())
        if(keys[i])
            keyListener.UpdateKey(i, deltaTime);
        else
            keyListener.ResetKey(i);
}
