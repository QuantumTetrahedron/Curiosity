#include "Magnet.h"
#include <regex>

Magnet::Magnet(int requiredPower, glm::vec3 position, glm::vec3 rotation, glm::vec3 scale, std::string modelName)
: GameObject(position, rotation, scale, modelName, true, true, false, false), ElectricityPowered(requiredPower){}

Magnet::Magnet(std::string str){
    std::regex pattern{R"((\w{1})\s+(\S+)\s+(\S+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+))"};
    std::smatch matches;
    if(std::regex_match(str, matches, pattern)){
        std::string name = matches[3];
        std::string type = matches[2];

        int x = atoi(((std::string)matches[4]).c_str());
        int y = atoi(((std::string)matches[5]).c_str());
        int z = atoi(((std::string)matches[6]).c_str());
        int r = atoi(((std::string)matches[7]).c_str());
        int p = atoi(((std::string)matches[8]).c_str());

        *this = Magnet(p, glm::vec3(2*x, y, 2*z), glm::vec3(0,90*r,0), glm::vec3(1.0f), name);
    }
}

std::unique_ptr<GameObject> Magnet::clone() const{
    std::unique_ptr<Magnet> m = std::make_unique<Magnet>(*this);
    m->powerLevel = 0;
    return m;
}

void Magnet::TurnOn(){
    Magnetize(Mode::PULL);
}
void Magnet::TurnOff(){
    Magnetize(Mode::PUSH);
}

void Magnet::Magnetize(Mode mode){
    glm::vec3 dp = Front();
    auto pos = transform.position + dp;
    for(int i = 0;i<40;++i){
        if(LevelManager::PlayerAt(pos))
            break;
        if(LevelManager::hasBlockingObject(pos)){
            std::shared_ptr<GameObject> obj = LevelManager::GetObject(pos);
            if(!obj) return; /* Obstackle */
            std::shared_ptr<Ferromagnetic> f = std::dynamic_pointer_cast<Ferromagnetic>(obj);
            if(!f) return;
            if(mode == Mode::PULL)
                f->Attract(transform.rotation.y);
            else
                f->Repulse(transform.rotation.y);
            break;
        }
        pos += dp;
    }
}

glm::vec3 Magnet::Front() const {
    auto ret = glm::vec3(0.0f);
    if(transform.rotation.y == 0.0f)
        ret.x += 2.0f;
    else if(transform.rotation.y == 90.0f)
        ret.z -= 2.0f;
    else if(transform.rotation.y == 180.0f)
        ret.x -= 2.0f;
    else if(transform.rotation.y == 270.0f)
        ret.z += 2.0f;
    return ret;
}
