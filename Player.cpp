#include "Player.h"
#include "LevelManager.h"
#include "Utility.h"

Player::Player(std::string modelName, glm::vec3 position, glm::vec3 rotation, glm::vec3 scale)
    : GameObject(position, rotation, scale, modelName, false, false),
     floatOffset(0.0f), floatHeight(1.5f), scanLimit(4), printLimit(4), selectedScan(0), selectedPrint(0), moveSpeed(5.0f), rotSpeed(5.0f),
     printMode(false), moving(false), falling(false) {
    transform.position.y+=floatHeight;
    target.rotation = transform.rotation;
    at = nullptr;
    deathTimer = 0.0f;
    dead = false;
    tileMoving = false;
}

void Player::Draw(Shader& shader){
    auto old = transform.position.y;
    transform.position.y += floatOffset;
    GameObject::Draw(shader);
    transform.position.y = old;
}

glm::vec3 Player::GetPos() const {
    return transform.position-glm::vec3(0.0f,floatHeight,0.0f);
}

std::shared_ptr<Tile> Player::GetTile() const {
    auto tiles = LevelManager::GetAllTiles();
    std::shared_ptr<Tile> ret;
    glm::vec3 p = transform.position-glm::vec3(0.0f,floatHeight,0.0f);
    for(p.y = std::floor(p.y); p.y>-1.0f; p.y--)
    {
        try{
            ret = tiles.at(p);
        }catch(...){continue;}
        return ret;
    }

    return nullptr;
}

bool Player::ShouldFall(){
    return GetTile()->GetPos().y != GetPos().y;
}

void Player::Fall(){
    falling = true;
    target.position = GetTile()->GetPos() + glm::vec3(0.0f,floatHeight, 0.0f);
}

void Player::Update(GLfloat dt){
    floatOffset = 0.5f * glm::sin(2*glfwGetTime());

    if(LevelManager::state == LevelState::dying){
        DieSlowly(dt);
        return;
    }

    float moveV = dt*moveSpeed;
    float rotV = dt*rotSpeed;

    if(target.rotation != transform.rotation){
        RotateTo(transform.rotation.x, target.rotation.x,rotV*50);
        RotateTo(transform.rotation.y, target.rotation.y,rotV*50);
        RotateTo(transform.rotation.z, target.rotation.z,rotV*50);
    }

    if(!tileMoving && at->transform.position != at->target.position)
        tileMoving = true;

    if(tileMoving){
        transform.position = at->GetPos() + glm::vec3(0.0f,floatHeight,0.0f);
        if(at->transform.position == at->target.position)
            tileMoving = false;
    }

    if(moving){
        int c = 0;

        if(MoveTo(transform.position.x, moveQueue.front()->GetTargetPos().x, moveV)) ++c;
        if(MoveTo(transform.position.y, moveQueue.front()->GetTargetPos().y+floatHeight, moveV)) ++c;
        if(MoveTo(transform.position.z, moveQueue.front()->GetTargetPos().z, moveV)) ++c;

        if(c==3)
        {
            if(at) at->Leave();
            at = moveQueue.front();
            at->Enter();
            moveQueue.pop();
        }
    }
    else if(falling){
        if(MoveTo(transform.position.y, target.position.y, moveV)){
            at = GetTile();
            falling=false;
            LevelManager::CheckLaserRays();
            if(ShouldFall())
                Fall();
        }
    }

    if(moveQueue.size()==0){
        moving = false;
    }
}

void Player::UpdateRotation(glm::vec3 targetPos){
    glm::vec3 d = targetPos - transform.position;
    if(d.z > 0)
    {
        target.rotation.y = glm::degrees(glm::atan(d.x/d.z));

    }
    else if(d.z == 0)
    {
        if(d.x > 0)
            target.rotation.y = 90;
        else if(d.x < 0)
            target.rotation.y = 270;
    }
    else
    {
        target.rotation.y = 180-glm::degrees(glm::atan(-d.x/d.z));
    }

    target.rotation.x = glm::abs(d.x) > 0.01 || glm::abs(d.z) > 0.01 ? glm::degrees(glm::atan(floatOffset+1)/glm::sqrt(d.x*d.x+d.z*d.z)) : 0;

    if(target.rotation.x < 0) target.rotation.x += 360;
    if(target.rotation.x >= 360) target.rotation.x -= 360;
    if(target.rotation.y < 0) target.rotation.y += 360;
    if(target.rotation.y >= 360) target.rotation.y -= 360;
}

void Player::ErasePath(){
    std::queue<std::shared_ptr<Tile>>().swap(moveQueue);
}

void Player::SetPath(const std::vector<std::shared_ptr<Tile>>& path){
    ErasePath();
    for(int i = path.size()-1;i>=0;--i){
        moveQueue.push(path[i]);
    }
}

void Player::Move(){
    moving = true;
}

void Player::CancelMove(){
    auto t = moveQueue.front();
    ErasePath();
    moveQueue.push(t);
}

glm::vec3 Player::Front(){
    glm::mat4 m;
    if(glm::length(transform.rotation)>0.0f)
    {
        m = glm::rotate(m, glm::radians(transform.rotation.y), glm::vec3(0.0f,1.0f,0.0f));
        m = glm::rotate(m, glm::radians(transform.rotation.z), glm::vec3(0.0f,0.0f,1.0f));
        m = glm::rotate(m, glm::radians(transform.rotation.x), glm::vec3(1.0f,0.0f,0.0f));
    }
    return glm::normalize(glm::vec3(m * glm::vec4(0.0f,0.0f,1.0f,0.0f)));
}

bool Player::Scan(std::shared_ptr<GameObject> target){
    glm::vec3 relRot = glm::vec3(0.0f);
    relRot.y = target->transform.rotation.y - this->target.rotation.y;
    if(relRot.y >= 360)
        relRot.y -= 360;
    else if(relRot.y < 0)
        relRot.y +=360;

    if(scans.size() < scanLimit){
        std::shared_ptr<GameObject> scan = target->CopyAt(glm::vec3(0.0), relRot, true);
        scans.push_back(scan);
    }
    else return false;
    return true;
}

void Player::Print(int n, glm::vec3 target){
    if(LevelManager::Printed.size() < printLimit && scans.size() > n)
    {
        glm::vec3 newRot = glm::vec3(0.0f);
        newRot.y = scans[n]->transform.rotation.y + this->target.rotation.y;
        if(newRot.y >= 360)
            newRot.y -= 360;

        LevelManager::Print(target, scans[n]->CopyAt(target, newRot), LevelManager::Printed.size());
        prints.push_back(scans[n]->CopyAt(glm::vec3(0.0f), newRot, true));
    }
}
