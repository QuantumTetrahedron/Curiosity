#ifndef LEVER_H
#define LEVER_H

#include <iostream>
#include "ResourceManager.h"
#include "PowerSource.h"
#include "Powered.h"
#include "GameObject.h"

class Lever : public GameObject, public Ferromagnetic, public PowerSource{
public:
    /// Copy constructor
    Lever(const Lever& other);

    /** Constructor
    * params:
    * * powerDest : Powered location when the lever is pulled.
    * * position : lever's position.
    * * rotation : lever's rotation.
    * * scale : lever's scale.
    * * modelName : name of the lever's model to render.
    * * baseModel : name of the lever's base model to render.
    */
    Lever(glm::vec3 powerDest, glm::vec3 position, glm::vec3 rotation, glm::vec3 scale, std::string modelName, std::string baseModel);

    /**
    * Regex constructor
    * params:
    * * str : string to parse
    */
    Lever(std::string str);

    /// Overloads.
    virtual std::unique_ptr<GameObject> clone() const;
    void Draw(const Shader& shader);

    void Activate();

    void Attract(float fromRot) override;

    void Repulse(float fromRot) override;

private:
    std::string baseModelName;
    bool active;
};

#endif // LEVER_H
