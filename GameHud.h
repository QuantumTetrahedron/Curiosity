#ifndef GAME_HUD_H
#define GAME_HUD_H

#include "HUDmanager.h"
#include "Radial.h"
#include "ObjectDisplayer.h"

enum class HUDmode{
    HUD_MODE_PRINT,
    HUD_MODE_SCAN
};

/**
* A class to manage level HUD
*/
class GameHUD {
public:
    GameHUD(){}
    ~GameHUD();

    /**
    * Initializes the HUD.
    * Uses window's size to position the elements.
    * params:
    * * windowW : width of the window.
    * * windowH : height of the window.
    */
    void Init(int windowW, int windowH);

    /**
    * Renders the HUD and the scanned/printed elements.
    * params:
    * * mode : Determines which parts of the HUD to draw.
    * * elements : The scanned or printed elements to be drawn on the HUD slots.
    */
    void Render(HUDmode mode, std::vector<std::shared_ptr<GameObject>>& elements);

    /**
    * Changes the selector's position to a new one.
    * params:
    * * n : The n-th element will be selected (counting from 0).
    * * mode : Mode in which the change will occur.
    */
    void Select(int n, HUDmode mode);

    /**
    * Changes the appearance of the selector.
    * params:
    * * l : loading state to set.
    * * mode : Mode in which the change will occur.
    */
    void SetSelectorLoadingState(float l, HUDmode mode);

private:
    HUDmanager scanHUD;
    HUDmanager printHUD;
    std::shared_ptr<Radial> scanSelector;
    std::shared_ptr<Radial> printSelector;
    std::shared_ptr<HUDelement> scanBG;
    std::shared_ptr<HUDelement> printBG;

    ObjectDisplayer displayer;
};

#endif // GAME_HUD
