#ifndef LASER_RAY_H
#define LASER_RAY_H

#include "Transform.h"
#include "ResourceManager.h"

/**
* A laser ray.
* Emits a low amount of light.
*/
class LaserRay{
public:
    glm::vec3 origin;
    glm::vec3 dest;
    float length;
    Direction dir;
    glm::vec3 oldOrigin;

    /**
    * Constructor
    * params:
    * * position : position of the ray's origin.
    * * rotation : angle of rotation. Should be divisible by 90.
    */
    LaserRay(glm::vec3 position, Direction direction);

    /**
    * Draws the ray.
    * params:
    * * shader : The shader to use.
    */
    void Draw(const Shader& shader);

    /**
    * Gets the direction of the ray.
    * returns:
    * * A normalized vector pointing from the ray's origin to its destination.
    */
    glm::vec3 Front() const;

    /**
    * Updates the ray.
    * Checks whether the ray should readjust its length.
    */
    void Update();

    /**
    * Makes the tiles beneath trespassable.
    */
    void ClearTiles();
    void ClearTarget();

    /**
    * Updates the data of a given shader.
    * The given shader must be using an array of laser rays named rayLight.
    * The method fills the i-th element of the array with the object's data.
    * params:
    * * shader : the shader used.
    * * i : index of the array's element to fill with data.
    */
    void UpdateShader(Shader& shader, int i);

    Direction GetDirection() const;

    bool markedToDestroy;
private:

    /**
    * Gets the model matrix.
    * returns:
    * * The model matrix.
    */
    glm::mat4 GetModelMatrix() const;

    std::string modelName;
};

#endif // LASER_RAY_H
