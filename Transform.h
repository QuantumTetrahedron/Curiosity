
#ifndef TRANSFORM_H
#define TRANSFORM_H

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

enum class Direction{
    EAST=0, NORTH=90, WEST=180, SOUTH=270
};
Direction operator+(Direction dir1, Direction dir2);
Direction operator+(Direction dir, int r);
Direction operator-(Direction dir1, Direction dir2);
Direction operator-(Direction dir, int r);

struct Transform {
    glm::vec3 position;
    glm::vec3 rotation;
    glm::vec3 scale;

    bool operator==(const Transform& other) const;

    bool operator!=(const Transform& other) const;

    void AddRotation(glm::vec3 rot);

    Direction GetDirection() const;
};

template<typename T>
bool MoveTo(T& point,const T& dest,const T& velocity){
    if(glm::abs(dest - point) > velocity)
        point += ((dest-point)/glm::abs(dest-point))*velocity;
    else{
        point = dest;
        return true;
    }
    return false;
}


template <typename T>
bool isClockwise(const T& rotA,const T& rotB){
    if(rotB > rotA){
        if(rotB - rotA > 180 || rotB - rotA < -180)
            return true;
    }
    else if(rotA > rotB){
        if(rotB - rotA < 180 && rotB - rotA > -180)
            return true;
    }
    return false;
}


template<typename T>
bool RotateTo(T& point,const T& dest,const T& velocity){
    T v = glm::abs(velocity);
    if(isClockwise(point, dest)) v *= -1;

    T dist = dest - point;
    if(dist > 180) dist -=360;
    if(dist <= -180) dist += 360;

    if(glm::abs(dist) > glm::abs(v))
    {
        point += v;
        if(point < 0) point += 360;
        if(point >= 360) point -= 360;
        return true;
    }
    point = dest;
    return true;
}

#endif
