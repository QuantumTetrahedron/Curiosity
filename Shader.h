#ifndef SHADER_H
#define SHADER_H

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class Shader
{
public:
    GLuint ID;

    Shader(){}
    /**
    * Constructor.
    * params:
    * * vertexPath : path to a file containing the vertex shader.
    * * fragmentPath : path to a file containing the fragment shader.
    */
    Shader(const char* vertexPath, const char* fragmentPath);

    /**
    * Enables the shader.
    */
    const Shader& Use() const;

    /// Methods for setting uniform values in the shader.
    void SetFloat    (const char *name, float value) const;
    void SetInteger  (const char *name, int value) const;
    void SetVector2f (const char *name, float x, float y) const;
    void SetVector2f (const char *name, const glm::vec2 &value) const;
    void SetVector3f (const char *name, float x, float y, float z) const;
    void SetVector3f (const char *name, const glm::vec3 &value) const;
    void SetVector4f (const char *name, float x, float y, float z, float w) const;
    void SetVector4f (const char *name, const glm::vec4 &value) const;
    void SetMatrix4  (const char *name, const glm::mat4 &matrix) const;
};

#endif
