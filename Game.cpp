#include "Game.h"
#include "Cutscenes.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <algorithm>
#include<iostream>

Game::Game(GLuint w, GLuint h) : width(w), height(h), state(GameState::mainMenu){
}

void Game::Init(){
    Shader& def = ResourceManager::LoadShader("shaders/instanced.vert", "shaders/instanced.frag", "instanced");
    Shader& simpleShader = ResourceManager::LoadShader("shaders/simple.vert", "shaders/simple.frag", "simple");
    Shader& spriteShader = ResourceManager::LoadShader("shaders/sprite.vert", "shaders/sprite.frag", "sprite");
    Shader& depthInst = ResourceManager::LoadShader("shaders/depthInst.vert", "shaders/depthInst.frag", "depthInst");
    Shader& depth = ResourceManager::LoadShader("shaders/depth.vert", "shaders/depthInst.frag", "depth");
    Shader& debugDepthQuad = ResourceManager::LoadShader("shaders/d.vert", "shaders/d.frag", "debug");
    Shader& HUDmodelShader = ResourceManager::LoadShader("shaders/HUDmodel.vert", "shaders/HUDmodel.frag", "HUDmodel");
    Shader& textShader = ResourceManager::LoadShader("shaders/text.vert", "shaders/text.frag", "text");
    Shader& laserRayShader = ResourceManager::LoadShader("shaders/laser.vert", "shaders/laser.frag", "laser");
    Shader& cRayShader = ResourceManager::LoadShader("shaders/laser.vert", "shaders/conn.frag", "cray");
    Shader& hdrQuadShader = ResourceManager::LoadShader("shaders/hdr.vert", "shaders/hdr.frag", "HDR");
    Shader& blurShader = ResourceManager::LoadShader("shaders/gauss.vert", "shaders/gauss.frag", "blur");

    Shader& PBRShader = ResourceManager::LoadShader("shaders/simple.vert", "shaders/simplePBR.frag", "PBR");
    Shader& PBRInst = ResourceManager::LoadShader("shaders/instanced.vert", "shaders/instancedPBR.frag", "PBRinst");

    ResourceManager::LoadTexture("sprites/hud/circle.png", true, "HUDcircle");
    ResourceManager::LoadTexture("sprites/hud/circleWhite.png", true, "HUDcircleW");
    ResourceManager::LoadTexture("sprites/hud/square.png", false, "HUDsquare");
    ResourceManager::LoadTexture("sprites/hud/button.png", true, "button");
    ResourceManager::LoadTexture("sprites/mainback.png", true, "mainback");

    ResourceManager::LoadModel("models/characters/hero/hero.obj", "player", 1);
    ResourceManager::LoadModel("models/misc/laserRay/ray.obj", "ray", 0.1);
    ResourceManager::LoadModel("models/floorTiles/tile/tile.obj", "tile", 0.2);
    ResourceManager::LoadModel("models/misc/pbrtest/pbrtest.obj", "pbrtest", 1);
    level.Init(width,height);

    glm::mat4 projection = glm::perspective(level.camera.Zoom, (float)width/(float)height, 0.1f, 100.0f);

    PBRInst.Use();
    PBRInst.SetMatrix4("projection", projection);
    PBRInst.SetVector3f("dirLight.direction", -0.4f, -1.0f, -0.3f);
    PBRInst.SetVector3f("dirLight.ambient", 0.005f, 0.005f, 0.005f);
    PBRInst.SetVector3f("dirLight.diffuse", 0.3f, 0.3f, 0.3f);
    PBRInst.SetVector3f("dirLight.specular", 0.0f, 0.0f, 0.0f);

    PBRInst.SetVector3f("spotLight.ambient", 0.0f, 0.0f, 0.0f);
    PBRInst.SetVector3f("spotLight.diffuse", 8.0f, 8.0f, 8.0f);
    PBRInst.SetVector3f("spotLight.specular", 1.0f, 1.0f, 1.0f);
    PBRInst.SetFloat("spotLight.constant", 1.0f);
    PBRInst.SetFloat("spotLight.linear", 0.045);
    PBRInst.SetFloat("spotLight.quadratic", 0.0075);
    PBRInst.SetFloat("spotLight.cutOff", glm::cos(glm::radians(28.0f)));
    PBRInst.SetFloat("spotLight.outerCutOff", glm::cos(glm::radians(40.0f)));
    PBRInst.SetInteger("shadowMap", 0);

    simpleShader.Use();
    simpleShader.SetMatrix4("projection", projection);
    simpleShader.SetVector3f("dirLight.direction", -0.4f, -1.0f, -0.3f);
    simpleShader.SetVector3f("dirLight.ambient", 0.005f, 0.005f, 0.005f);
    simpleShader.SetVector3f("dirLight.diffuse", 0.3f, 0.3f, 0.3f);
    simpleShader.SetVector3f("dirLight.specular", 0.0f, 0.0f, 0.0f);

    simpleShader.SetVector3f("spotLight.ambient", 0.0f, 0.0f, 0.0f);
    simpleShader.SetVector3f("spotLight.diffuse", 14.0f, 14.0f, 14.0f);
    simpleShader.SetVector3f("spotLight.specular", 1.0f, 1.0f, 1.0f);
    simpleShader.SetFloat("spotLight.constant", 1.0f);
    simpleShader.SetFloat("spotLight.linear", 0.045);
    simpleShader.SetFloat("spotLight.quadratic", 0.0075);
    simpleShader.SetFloat("spotLight.cutOff", glm::cos(glm::radians(28.0f)));
    simpleShader.SetFloat("spotLight.outerCutOff", glm::cos(glm::radians(40.0f)));
    simpleShader.SetInteger("shadowMap", 0);

    HUDmodelShader.Use();
    glm::mat4 projection2 = glm::perspective(level.camera.Zoom, 1.0f, 0.1f, 100.0f);
    HUDmodelShader.SetMatrix4("projection", projection2);
    glm::mat4 view = glm::lookAt(glm::vec3(0.0,1.0,4.0), glm::vec3(0.0,1.0,0.0), glm::vec3(0.0,1.0,0.0));
    HUDmodelShader.SetMatrix4("view", view);

    glm::mat4 orth = glm::ortho(0.0f, static_cast<GLfloat>(width), static_cast<GLfloat>(height), 0.0f, -1.0f, 1.0f);
    spriteShader.Use();
    spriteShader.SetMatrix4("projection", orth);
    spriteShader.SetInteger("image", 0);

    textShader.Use();
    textShader.SetMatrix4("projection", orth);
    textShader.SetInteger("fontImage", 0);

    laserRayShader.Use();
    laserRayShader.SetMatrix4("projection", projection);

    cRayShader.Use();
    cRayShader.SetMatrix4("projection", projection);

    hdrQuadShader.Use();
    hdrQuadShader.SetInteger("hdrBuffer", 0);
    hdrQuadShader.SetInteger("bloomBuffer", 1);
    hdrQuadShader.SetFloat("exposure", 1.0f);

    def.Use();
    def.SetMatrix4("projection", projection);
    def.SetVector3f("dirLight.direction", -0.4f, -1.0f, -0.3f);
    def.SetVector3f("dirLight.ambient", 0.005f, 0.005f, 0.005f);
    def.SetVector3f("dirLight.diffuse", 0.3f, 0.3f, 0.3f);
    def.SetVector3f("dirLight.specular", 0.0f, 0.0f, 0.0f);

    def.SetVector3f("spotLight.ambient", 0.0f, 0.0f, 0.0f);
    def.SetVector3f("spotLight.diffuse", 14.0f, 14.0f, 14.0f);
    def.SetVector3f("spotLight.specular", 1.0f, 1.0f, 1.0f);
    def.SetFloat("spotLight.constant", 1.0f);
    def.SetFloat("spotLight.linear", 0.045);
    def.SetFloat("spotLight.quadratic", 0.0075);
    def.SetFloat("spotLight.cutOff", glm::cos(glm::radians(28.0f)));
    def.SetFloat("spotLight.outerCutOff", glm::cos(glm::radians(40.0f)));
    def.SetInteger("shadowMap", 0);

    PBRShader.Use();
    PBRShader.SetVector3f("albedo", 0.9, 0.1, 0.1);
    PBRShader.SetFloat("metallic", 0.9);
    PBRShader.SetFloat("roughness", 0.7);
    PBRShader.SetFloat("ao", 0.0);
    PBRShader.SetMatrix4("projection", projection);
    PBRShader.SetVector3f("dirLight.direction", -0.4f, -1.0f, -0.3f);
    PBRShader.SetVector3f("dirLight.ambient", 0.005f, 0.005f, 0.005f);
    PBRShader.SetVector3f("dirLight.diffuse", 0.3f, 0.3f, 0.3f);
    PBRShader.SetVector3f("dirLight.specular", 0.0f, 0.0f, 0.0f);

    PBRShader.SetVector3f("spotLight.ambient", 0.0f, 0.0f, 0.0f);
    PBRShader.SetVector3f("spotLight.diffuse", 8.0f, 8.0f, 8.0f);
    PBRShader.SetVector3f("spotLight.specular", 1.0f, 1.0f, 1.0f);
    PBRShader.SetFloat("spotLight.constant", 1.0f);
    PBRShader.SetFloat("spotLight.linear", 0.045);
    PBRShader.SetFloat("spotLight.quadratic", 0.0075);
    PBRShader.SetFloat("spotLight.cutOff", glm::cos(glm::radians(28.0f)));
    PBRShader.SetFloat("spotLight.outerCutOff", glm::cos(glm::radians(40.0f)));
    PBRShader.SetInteger("shadowMap", 0);

    std::shared_ptr<TextRenderer> textRenderer = std::make_shared<TextRenderer>(textShader);
    #ifdef __linux__
        textRenderer->Load("/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf", 24);
    #else
        textRenderer->Load("C://Windows/Fonts/DejaVuSans.ttf", 24);
    #endif
    menu.Init();
    menu.setFont(textRenderer);
    menu.buttons.emplace_back(menu.renderer, menu.text, "Start Game", glm::vec2(200.0f, 400.0f),
                              glm::vec2(ResourceManager::GetTexture("button").width/3,
                              ResourceManager::GetTexture("button").height/3), ResourceManager::GetTexture("button"));

    //level.Load("levels/test.lvl");
    //level.Load("levels/first.lvl");
    //level.Load("levels/level1.lvl");
    //level.Load("levels/test2.lvl");
    //level.Load("levels/magnet_tests.lvl");
    level.Load("levels/demo1.lvl");
}

void Game::ProcessInput(Input& in){
    Shader& s1 = ResourceManager::GetShader("PBR");
    Shader& s2 = ResourceManager::GetShader("PBRinst");

    Shader& hdrQuadShader = ResourceManager::GetShader("HDR");
    hdrQuadShader.Use();
    hdrQuadShader.SetInteger("hdr", true);
    s1.Use().SetInteger("pbrTest", in.keys[GLFW_KEY_SPACE]);
    s1.SetInteger("laserTest", in.keys[GLFW_KEY_L]);
    s2.Use().SetInteger("pbrTest", in.keys[GLFW_KEY_SPACE]);
    s2.SetInteger("laserTest", in.keys[GLFW_KEY_L]);
    if(state == GameState::inGame){
        level.ProcessInput(in);
    }
    else if(state == GameState::mainMenu){
        if (in.mouseButtons[GLFW_MOUSE_BUTTON_LEFT]) {
            /*in.clicksToProcess[GLFW_MOUSE_BUTTON_LEFT] = false;
            in.mouseButtons[GLFW_MOUSE_BUTTON_LEFT] = false;
            for (const auto& b : menu.buttons) {
                if (b.IsHovered(in)) {
                    in.clicksToProcess[GLFW_MOUSE_BUTTON_LEFT] = true;
                    in.mouseButtons[GLFW_MOUSE_BUTTON_LEFT] = true;
                    break;
                }
            }
        }
        else if (in.clicksToProcess[GLFW_MOUSE_BUTTON_LEFT]) { */
            if (menu.buttons[0].IsHovered(in)) {
                state = GameState::inGame;
            }
        }
    }
    else if(state == GameState::loading){

    }
    in.mouseButtons[GLFW_MOUSE_BUTTON_LEFT] = false;
    in.mouseButtons[GLFW_MOUSE_BUTTON_RIGHT] = false;
    in.mouseButtons[GLFW_MOUSE_BUTTON_MIDDLE] = false;
}

void Game::Update(GLfloat dt){

    if(state == GameState::inGame){
        level.Update(dt);
    }
    else if(state == GameState::mainMenu){

    }
    else if(state == GameState::loading){

    }

    #ifdef __linux__
    if(state == GameState::cutscene){
        Cutscenes::Update();
        if(!Cutscenes::isPlaying()){
        std::cout << "leave cutscene" << std::endl;
            state = GameState::inGame;}
    }
    else if(Cutscenes::isPlaying()){
        std::cout << "enter cutscene" << std::endl;
        state = GameState::cutscene;
    }
    #endif
}

void Game::Render(){
    if(state == GameState::inGame){
        level.Render();
    }
    else if(state == GameState::mainMenu){
        menu.renderer->DrawSprite(ResourceManager::GetTexture("mainback"), glm::vec2(0.0f,0.0f), glm::vec2(width, height));
        menu.Draw();
    }
    else if(state == GameState::loading){

    }
}
