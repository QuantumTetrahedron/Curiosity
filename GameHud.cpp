#include "GameHud.h"

GameHUD::~GameHUD(){
    scanHUD.Clear();
    printHUD.Clear();
}

void GameHUD::Init(int windowW, int windowH){
    scanHUD.Init(windowW, windowH);
    printHUD.Init(windowW, windowH);
    displayer.Init(windowW, windowH);

    scanHUD.AddElement(std::shared_ptr<HUDelement>(new HUDelement("HUDsquare", 600, 170, 0, 0, 0, RelativePos::BOTTOM_LEFT, RelativePos::BOTTOM_LEFT, glm::vec3(0.2f))));

    for(int i=0;i<4;++i)
    {
        scanHUD.AddElement(std::shared_ptr<HUDelement>(new HUDelement("HUDsquare", 149, 20, 1+i*150, 150, 1, RelativePos::BOTTOM_LEFT, RelativePos::BOTTOM_LEFT, glm::vec3(1.0f,0.3f,0.3f))));
        scanHUD.AddElement(std::shared_ptr<HUDelement>(new HUDelement("HUDcircle", 150, 150, i*150, 0, 4, RelativePos::BOTTOM_LEFT, RelativePos::BOTTOM_LEFT)));
    }

    printHUD.AddElement(std::shared_ptr<HUDelement>(new HUDelement("HUDsquare", 600, 170, 0, 0, 0, RelativePos::BOTTOM_LEFT, RelativePos::BOTTOM_LEFT, glm::vec3(0.4f))));

    for(int i=0;i<4;++i){
        printHUD.AddElement(std::shared_ptr<HUDelement>(new HUDelement("HUDsquare", 149, 170, 1+i*150, 0, 1, RelativePos::BOTTOM_LEFT, RelativePos::BOTTOM_LEFT, glm::vec3(1.0f,0.3f,0.3f))));
        printHUD.AddElement(std::shared_ptr<HUDelement>(new HUDelement("HUDcircle", 150, 150, i*150, 0, 4, RelativePos::BOTTOM_LEFT, RelativePos::BOTTOM_LEFT)));
    }

    scanBG = std::shared_ptr<HUDelement>( new HUDelement("HUDcircleW", 160, 160, -5, -5, 2, RelativePos::BOTTOM_LEFT, RelativePos::BOTTOM_LEFT, glm::vec3(1.0f,1.0f,0.0f)));
    printBG = std::shared_ptr<HUDelement>( new HUDelement("HUDcircleW", 160, 160, -5, -5, 2, RelativePos::BOTTOM_LEFT, RelativePos::BOTTOM_LEFT, glm::vec3(1.0f,1.0f,0.0f)));
    scanSelector = std::shared_ptr<Radial>( new Radial("HUDcircleW", 160, 160, -5, -5, 3, RelativePos::BOTTOM_LEFT, RelativePos::BOTTOM_LEFT, glm::vec3(1.0f,0.0f,0.0f)));
    printSelector = std::shared_ptr<Radial>( new Radial("HUDcircleW", 160, 160, -5, -5, 3, RelativePos::BOTTOM_LEFT, RelativePos::BOTTOM_LEFT, glm::vec3(1.0f,0.0f,0.0f)));

    scanHUD.AddElement(scanSelector);
    printHUD.AddElement(printSelector);
    scanHUD.AddElement(scanBG);
    printHUD.AddElement(printBG);
}

void GameHUD::Render(HUDmode mode, std::vector<std::shared_ptr<GameObject>>& elements){
    Shader& shader = ResourceManager::GetShader("HUDmodel");
    glm::vec3 color = glm::vec3(0.0f);
    if(mode == HUDmode::HUD_MODE_SCAN){
        scanHUD.Render();
        color.g += 1.0f;
    }
    else{
        printHUD.Render();
        color.r += 1.0f;
    }

    for(int i=0;i<elements.size();++i)
        displayer.Display(elements[i], 150*i, 0, 150, 150, shader, color);
}

void GameHUD::Select(int n, HUDmode mode){
    int newPos = n*150-5;
    if(mode == HUDmode::HUD_MODE_SCAN){
        scanSelector->xPos = newPos;
        scanBG->xPos = newPos;
    }
    else if(mode == HUDmode::HUD_MODE_PRINT){
        printSelector->xPos = newPos;
        printBG->xPos = newPos;
    }
}

void GameHUD::SetSelectorLoadingState(float l, HUDmode mode){
    if(mode == HUDmode::HUD_MODE_SCAN)
        scanSelector->SetCompletion(l);
    else if(mode == HUDmode::HUD_MODE_PRINT)
        printSelector->SetCompletion(l);
}
