#include "Mirror.h"

void Mirror::TurnOn(Direction dir){
    Direction newDir = Direction::EAST;
    if(dir == Direction::NORTH){
        if(transform.rotation.y == 0.0f) newDir = Direction::WEST;
        else if(transform.rotation.y == 270.0f) newDir = Direction::EAST;
        else return;
    }
    else if(dir == Direction::SOUTH){
        if(transform.rotation.y == 90.0f) newDir = Direction::WEST;
        else if(transform.rotation.y == 180.0f) newDir = Direction::EAST;
        else return;
    }
    else if(dir == Direction::WEST){
        if(transform.rotation.y == 0.0f) newDir = Direction::NORTH;
        else if(transform.rotation.y == 90.0f) newDir = Direction::SOUTH;
        else return;
    }
    else if(dir == Direction::EAST){
        if(transform.rotation.y == 180.0f) newDir = Direction::SOUTH;
        else if(transform.rotation.y == 270.0f) newDir = Direction::NORTH;
        else return;
    }

    reflection = LevelManager::RegisterLaserRay(target.position, newDir);
}

void Mirror::TurnOff(Direction dir){
    if(reflection){
        LevelManager::EraseLaserRay(transform.position);
        reflection = nullptr;
    }
}

std::unique_ptr<GameObject> Mirror::clone() const{
    return std::make_unique<Mirror>(*this);
}

std::unique_ptr<GameObject> Mirror::CopyAt(glm::vec3 pos, glm::vec3 rot, bool safeCopy){
    std::unique_ptr<Mirror> obj(dynamic_cast<Mirror*>(GameObject::CopyAt(pos, rot, safeCopy).release()));
    obj->reflection = nullptr;
    obj->laserCount = 0;
    return obj;
}

void Mirror::CleanUp(){
    if(reflection)
        LevelManager::EraseLaserRay(transform.position);
}

Mirror::Mirror(glm::vec3 position, glm::vec3 rotation, glm::vec3 scale, std::string modelName)
: GameObject(position, rotation, scale, modelName, true, true, false, false){
}

Mirror::Mirror(std::string str){
    std::regex pattern{R"((\w{1})\s+(\S+)\s+(\S+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+))"};
    std::smatch matches;
    if(std::regex_match(str, matches, pattern)){
        std::string name = matches[3];
        std::string type = matches[2];

        int x = atoi(((std::string)matches[4]).c_str());
        int y = atoi(((std::string)matches[5]).c_str());
        int z = atoi(((std::string)matches[6]).c_str());
        int r = atoi(((std::string)matches[7]).c_str());

        *this = Mirror(glm::vec3(2*x, y, 2*z), glm::vec3(0,90*r,0), glm::vec3(1.0f), name);
    }
}
