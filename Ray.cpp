#include "Ray.h"
#include "Tile.h"

Ray::Ray(const Camera& cam, double scrX, double scrY){
    position = cam.Position;

    double x = 2*scrX/cam.Width-1;
    double y = 1-2*scrY/cam.Height;
    double z = 1.0f;
    glm::vec3 ray_nds = glm::vec3(x,y,z);
    glm::vec4 ray_clip = glm::vec4(ray_nds.x, ray_nds.y, -1.0f, 1.0f);
    glm::vec4 ray_eye = glm::inverse(glm::perspective(cam.Zoom, cam.Width/(float)cam.Height, 0.1f, 100.0f)) * ray_clip;
    ray_eye = glm::vec4(ray_eye.x, ray_eye.y, -1.0, 0.0);
    glm::vec4 ray_wor = glm::inverse(cam.GetViewMatrix())*ray_eye;
    glm::vec3 ray = glm::vec3(ray_wor.x, ray_wor.y, ray_wor.z);
    ray = glm::normalize(ray);
    ray /= -ray.y;

    direction = ray;
}

std::experimental::optional<glm::vec3> Ray::Collide(const std::vector<std::shared_ptr<const GameObject>>& objects){
    glm::vec3 s;
    for(float h = position.y; h>=-5.0f; h-=0.1){
        s = position+direction*(position.y-h);
        for(std::shared_ptr<const GameObject> obj : objects){
            if(!obj->collider) continue;
            if(obj->collider.value().Collided(s))
            {
                if(std::shared_ptr<const Tile> t = std::dynamic_pointer_cast<const Tile>(obj))
                    return t->GetPos();
                else
                    return obj->transform.position;
            }
        }
    }
}
