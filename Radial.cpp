#include "Radial.h"

Radial::Radial(std::string texture, float width, float height, int x, int y, int z, RelativePos anchor, RelativePos pivot, glm::vec3 tint)
: HUDelement(texture, width, height, x, y, z, anchor, pivot, tint){
    completion = 0;
}

void Radial::Draw(int windowW, int windowH, const SpriteRenderer& renderer) const{
    renderer.DrawRadial(ResourceManager::GetTexture(texture), completion, CalculateScreenPos(windowW, windowH), glm::vec2(width, height), 0.0f, colorTint);
}

void Radial::SetCompletion(float c){
    completion = c*100.f;
}
