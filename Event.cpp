#include "Event.h"
#include "Level.h"
#include "LevelManager.h"
#include "Cutscenes.h"

Level* Event::level;

Event::Event(){
    isFile = false;
    command = "";
}

Event::Event(std::string str, bool file)
{
    isFile  = file;
    command = str;
}

void Event::Trigger(){
    if(isFile)
        ParseScriptFile();
    else
        ParseSingleCommand(command);
}

void Event::RegisterLevel(Level* l){
    level = l;
}

void Event::ParseSingleCommand(std::string str){
    std::regex pattern{R"(([^\(\)]+)\(([^\(\)]*)\))"};
    std::smatch matches;
    if(std::regex_match(str, matches, pattern)){
        std::string c = (std::string)matches[1];
        std::string arg = (std::string)matches[2];

        if(c == "debug")
            std::cout << arg << std::endl;
        else if(c == "stop" && level)
            level->player->CancelMove();
        else if(c == "load" && level)
            level->SetLoad(arg.c_str());
        #ifdef __linux__
        else if(c == "play")
        {
            Cutscenes::Load(arg);
            Cutscenes::Play();
        }
        #endif
    }
}


void Event::ParseScriptFile(){
    std::ifstream f(command);
    std::string com;
    while(std::getline(f, com)){
        if(com.length()==0 || com[0] == '#')
            continue;
        else
            ParseSingleCommand(com);
    }
    f.close();
}
