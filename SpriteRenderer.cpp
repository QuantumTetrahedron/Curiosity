#include "SpriteRenderer.h"
#include <iostream>

void SpriteRenderer::initRenderData(){
    shader = ResourceManager::GetShader("sprite");
    unsigned int VBO;
    float vertices[] = {
        0.0f, 1.0f, 0.0f, 1.0f,
        1.0f, 0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 0.0f, 0.0f,

        0.0f, 1.0f, 0.0f, 1.0f,
        1.0f, 1.0f, 1.0f, 1.0f,
        1.0f, 0.0f, 1.0f, 0.0f
    };

    glGenVertexArrays(1, &quadVAO);
    glGenBuffers(1, &VBO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glBindVertexArray(quadVAO);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (GLvoid*)0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    ///radial
    glGenVertexArrays(1, &radialVAO);
    glGenBuffers(1, &radialVBO);
    glBindVertexArray(radialVAO);
    glBindBuffer(GL_ARRAY_BUFFER, radialVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 5 * 3 * 4, NULL, GL_DYNAMIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

void SpriteRenderer::DrawSprite(const Texture2D &texture, const glm::vec2& position,
  const glm::vec2& size, float rotate, const glm::vec3& color) const
{
    shader.Use();
    glm::mat4 model;
    model = glm::translate(model, glm::vec3(position, 0.0f));

    model = glm::translate(model, glm::vec3(0.5f * size.x, 0.5f * size.y, 0.0f));
    model = glm::rotate(model, rotate, glm::vec3(0.0f, 0.0f, 1.0f));
    model = glm::translate(model, glm::vec3(-0.5f * size.x, -0.5f * size.y, 0.0f));

    model = glm::scale(model, glm::vec3(size, 1.0f));

    shader.SetMatrix4("model", model);
    shader.SetVector3f("spriteColor", color);

    glActiveTexture(GL_TEXTURE0);
    texture.Bind();

    glDisable(GL_DEPTH_TEST);
    glBindVertexArray(quadVAO);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);
    glEnable(GL_DEPTH_TEST);
}

void SpriteRenderer::DrawRadial(const Texture2D& texture, float percent, const glm::vec2& position, const glm::vec2& size, float rotate, const glm::vec3& color) const {
    float vertices[] {
        0.5f, 0.5f, 0.5f, 0.5f,
        1.0f, 0.0f, 1.0f, 0.0f,
        0.5f, 0.0f, 0.5f, 0.0f,

        0.5f, 0.5f, 0.5f, 0.5f,
        1.0f, 1.0f, 1.0f, 1.0f,
        1.0f, 0.0f, 1.0f, 0.0f,

        0.5f, 0.5f, 0.5f, 0.5f,
        0.0f, 1.0f, 0.0f, 1.0f,
        1.0f, 1.0f, 1.0f, 1.0f,

        0.5f, 0.5f, 0.5f, 0.5f,
        0.0f, 0.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f, 1.0f,

        0.5f, 0.5f, 0.5f, 0.5f,
        0.5f, 0.0f, 0.5f, 0.0f,
        0.0f, 0.0f, 0.0f, 0.0f
    };

    float angle = percent * 3.6f;
    glm::vec2 limit = glm::vec2(cos(glm::radians(90.0f - angle)),sin(glm::radians(90.0f - angle)));

    float maxdim = std::max(std::abs(limit.x), std::abs(limit.y));
    limit.x /= maxdim;
    limit.y /= maxdim;

    glm::vec2 limitPoint = glm::vec2((limit.x + 1)/2, 1 - (limit.y+1)/2);

    if (percent < 100.0f)
    {
        vertices[4 * 12 + 4] = limitPoint.x;
        vertices[4 * 12 + 5] = limitPoint.y;
        vertices[4 * 12 + 6] = limitPoint.x;
        vertices[4 * 12 + 7] = limitPoint.y;
    }
    if (percent < 87.5f)
    {
        vertices[3 * 12 + 4] = limitPoint.x;
        vertices[3 * 12 + 5] = limitPoint.y;
        vertices[3 * 12 + 6] = limitPoint.x;
        vertices[3 * 12 + 7] = limitPoint.y;

        vertices[4 * 12 + 8] = limitPoint.x;
        vertices[4 * 12 + 9] = limitPoint.y;
        vertices[4 * 12 + 10] = limitPoint.x;
        vertices[4 * 12 + 11] = limitPoint.y;
    }
    if (percent < 62.5f)
    {
        vertices[2 * 12 + 4] = limitPoint.x;
        vertices[2 * 12 + 5] = limitPoint.y;
        vertices[2 * 12 + 6] = limitPoint.x;
        vertices[2 * 12 + 7] = limitPoint.y;

        vertices[3 * 12 + 8] = limitPoint.x;
        vertices[3 * 12 + 9] = limitPoint.y;
        vertices[3 * 12 + 10] = limitPoint.x;
        vertices[3 * 12 + 11] = limitPoint.y;
    }
    if (percent < 37.5f)
    {
        vertices[12 + 4] = limitPoint.x;
        vertices[12 + 5] = limitPoint.y;
        vertices[12 + 6] = limitPoint.x;
        vertices[12 + 7] = limitPoint.y;

        vertices[2 * 12 + 8] = limitPoint.x;
        vertices[2 * 12 + 9] = limitPoint.y;
        vertices[2 * 12 + 10] = limitPoint.x;
        vertices[2 * 12 + 11] = limitPoint.y;
    }
    if (percent < 12.5f)
    {
        vertices[4] = limitPoint.x;
        vertices[5] = limitPoint.y;
        vertices[6] = limitPoint.x;
        vertices[7] = limitPoint.y;
        vertices[12 + 8] = limitPoint.x;
        vertices[12 + 9] = limitPoint.y;
        vertices[12 + 10] = limitPoint.x;
        vertices[12 + 11] = limitPoint.y;
    }

    shader.Use();
    glm::mat4 model;
    model = glm::translate(model, glm::vec3(position, 0.0f));

    model = glm::translate(model, glm::vec3(0.5f * size.x, 0.5f * size.y, 0.0f));
    model = glm::rotate(model, rotate, glm::vec3(0.0f, 0.0f, 1.0f));
    model = glm::translate(model, glm::vec3(-0.5f * size.x, -0.5f * size.y, 0.0f));

    model = glm::scale(model, glm::vec3(size, 1.0f));

    shader.SetMatrix4("model", model);
    shader.SetVector3f("spriteColor", color);

    glActiveTexture(GL_TEXTURE0);
    texture.Bind();

    glBindBuffer(GL_ARRAY_BUFFER, radialVBO);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(radialVAO);
    glDrawArrays(GL_TRIANGLES, 0, 15);
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
}
