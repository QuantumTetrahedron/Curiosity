#ifndef INPUT_H
#define INPUT_H

#include <map>
#include <set>

/**
* A struct to store the hold time of registered keys.
*/
struct KeyListener{
    /**
    * Registers a new key to listen to.
    * params:
    * key : the new key.
    */
    void RegisterKey(int key);

    /**
    * Updates a key's hold time based on time passed.
    * params:
    * * key : the key to update.
    * * deltaTime : time passed.
    */
    void UpdateKey(int key, float deltaTime);

    /**
    * Restores the hold time of a key to 0.
    * params:
    * * key : the key to reset.
    */
    void ResetKey(int key);

    /**
    * Resets every key.
    */
    void Reset();

    /**
    * Returns the hold time of a specific key.
    * params:
    * * key : The key to check.
    * returns:
    * * The key's hold time or 0 if key is not registered.
    */
    float GetHoldTime(int key);

    /**
    * Gets the set of registered keys.
    * returns:
    * * A reference to the set of registered keys.
    */
    const std::set<int>& GetRegisteredKeys();

private:

    std::set<int> registered;
    std::map<int, float> keyHoldTimes;
};

/**
* A class to store input data.
*/
struct Input{
    bool keys[1024];

    float mouseX;
    float mouseY;
    bool mouseButtons[3];
    bool clicksToProcess[3];

    float scrollOffset;

    /**
    * Resets offsets.
    */
    void ResetOffsets();

    KeyListener keyListener;
    /**
    * Updates the key listener.
    * params:
    * * deltaTime : Time passed.
    */
    void UpdateKeyListener(float deltaTime);
};

#endif
