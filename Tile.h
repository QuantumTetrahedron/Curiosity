#ifndef TILE_H
#define TILE_H

#include <map>
#include <vector>
#include <memory>
#include "GameObject.h"
#include "Utility.h"
#include<string>
class Tile : public GameObject{
public:
    /**
    * Constructor
    * params:
    * * position : world space position.
    * * rotation : tile's rotation.
    * * scale : tile's scale
    * * modelName : name of the model to use.
    * * moveCost : the cost to enter the tile.
    * * TilePos : The tile's position.
    * * isFloor : determines the collider behaviour.
    */
    Tile(glm::vec3 position = glm::vec3(0.0f), glm::vec3 rotation = glm::vec3(0.0f), glm::vec3 scale = glm::vec3(1.0f), std::string modelName= "",
         int moveCost = 1, glm::vec3 TilePosOffset = glm::vec3(0.0f), bool isFloor = true);

    /// clone overloads
    virtual std::unique_ptr<GameObject> clone()const{return std::make_unique<Tile>(*this);}
    virtual std::unique_ptr<GameObject> CopyAt(glm::vec3 pos, glm::vec3 rot = glm::vec3(0.0f), bool safeCopy = false) override;

    virtual void Update(double dt) override;

    virtual void Enter();
    virtual void Leave();
    bool hasPlayer;

    /**
    * Draw overloaded to also draw tile's highlighter.
    */
    virtual void Draw(const Shader& shader);

    virtual void Fall() override;

    /**
    * Checks if the tile is highlighted.
    * returns:
    * * true if the tile is highlighted.
    */
    bool Highlighted() const;

    /// Methods to set the appropriate state of the highlighter.
    void Hover();
    void Unhover();
    void SetAsPath();
    void ClearPath();
    void Select();
    void Deselect();
    void UpdateHighlighter();

    GameObject highlighter;

    int movementCost;

    /**
    * Gets the distance to a given tile.
    * params:
    * * other : The tile to check.
    * returns:
    * * The distance to a given tile.
    */
    int GetDistance(const Tile& other);

    /**
    * Gets the tile's neighbours from a map.
    * params:
    * * tiles : a map of tiles to search for neighbours.
    * returns:
    * * a vector of neighbouring tiles.
    */
    std::vector<std::shared_ptr<Tile>> GetNeighbours(const std::map<glm::vec3, std::shared_ptr<Tile>, util::vec3Comparator>& tiles) const;

    bool occupied;
    bool trespassable;

    glm::vec3 GetPos() const {return transform.position + TilePositionOffset;}
    glm::vec3 GetTargetPos() const {return target.position + TilePositionOffset;}
protected:
    bool hovered;
    bool isPath;
    bool selected;
    glm::vec3 TilePositionOffset;

    static const std::vector<glm::vec3> directions;
};

#endif
