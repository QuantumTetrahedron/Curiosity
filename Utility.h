#ifndef UTILITY_H
#define UTILITY_H

#include <queue>
#include <memory>

#include <glm/glm.hpp>

namespace util{

struct vec3Comparator{
    bool operator()(const glm::vec3& a, const glm::vec3& b) const {
        return a.x < b.x || (a.x==b.x && a.y < b.y) || (a.x==b.x && a.y==b.y && a.z < b.z);
    }
};

template<class T>
struct gt{
    bool operator()(T first, T second) const{
        return first > second;
    }
};
template<class T>
struct gt<T*>{
    bool operator()(T first, T second) const{
        return first > second;
    }
};

template<class T, class P = gt<T>>
struct sorted_vector{
    struct node{
        node(){
            value = T();
            next = nullptr;
        }

        node(T v){
            value = v;
            next = nullptr;
        }

        node* next;
        T value;
    };

    struct iterator{
        node* _node;
        iterator(node* n){
            _node = n;
        }
        T operator*() const {
            return _node->value;
        }
        void operator++(){
            _node = _node->next;
        }
        bool operator==(const iterator& other) const{
            return _node == other._node;
        }
        bool operator!=(const iterator& other) const{
            return _node != other._node;
        }
    };


    ~sorted_vector(){
        if(head != &sentinel){
            node* n = head;
            node* t = n->next;
            for(;t != &sentinel;n = t)
            {
                 t = n->next;
                 delete n;
            }
        }
    }

    sorted_vector(){
        _size = 0;
        sentinel = node();
        head = &sentinel;
    }

    void push(T value){
        if(empty())
        {
            node* n = new node(value);
            n->next = head;
            head = n;
        }
        else{
            for(node* it = head; it != &sentinel; it = it->next){
                P pred;
                if(pred(value, it->value)){
                    node* n = new node(it->value);
                    it->value = value;
                    n->next = it->next;
                    it->next = n;
                    break;
                }

                if(it->next == &sentinel){
                    node* n = new node(value);
                    n->next = &sentinel;
                    it->next = n;
                    break;
                }
            }
        }
        _size++;
    }

    bool empty(){
        return _size == 0;
    }

    bool size(){
        return _size;
    }

    void clear(){
        if(head != &sentinel){
            node* n = head;
            node* t = n->next;
            for(;t != &sentinel;n = t)
            {
                 t = n->next;
                 delete n;
            }
        }
        head = &sentinel;
        _size = 0;
    }

    iterator begin(){
        return iterator(head);
    }

    iterator end(){
        return iterator(&sentinel);
    }

private:
    size_t _size;
    node* head;
    node sentinel;
};

template<class T, class P>
struct sorted_vector<T*, P>{
    struct node{
        node(){
            value = nullptr;
            next = nullptr;
        }

        node(T* v){
            value = v;
            next = nullptr;
        }

        node* next;
        T* value;
    };

    struct iterator{
        node* _node;
        iterator(node* n){
            _node = n;
        }
        T* operator*() const {
            return _node->value;
        }
        void operator++(){
            _node = _node->next;
        }
        bool operator==(const iterator& other) const{
            return _node == other._node;
        }
        bool operator!=(const iterator& other) const{
            return _node != other._node;
        }
    };


    ~sorted_vector(){
        if(head != &sentinel){
            node* n = head;
            node* t = n->next;
            for(;t != &sentinel;n = t)
            {
                 t = n->next;
                 delete n;
            }
        }
    }

    sorted_vector(){
        _size = 0;
        sentinel = node();
        head = &sentinel;
    }

    void push(T* value){
        if(empty())
        {
            node* n = new node(value);
            n->next = head;
            head = n;
        }
        else{
            for(node* it = head; it != &sentinel; it = it->next){
                P pred;
                if(pred(*value, *(it->value))){
                    node* n = new node(it->value);
                    it->value = value;
                    n->next = it->next;
                    it->next = n;
                    break;
                }

                if(it->next == &sentinel){
                    node* n = new node(value);
                    n->next = &sentinel;
                    it->next = n;
                    break;
                }
            }
        }
        _size++;
    }

    bool empty(){
        return _size == 0;
    }

    bool size(){
        return _size;
    }

    void clear(){
        if(head != &sentinel){
            node* n = head;
            node* t = n->next;
            for(;t != &sentinel;n = t)
            {
                 t = n->next;
                 delete n;
            }
        }
        head = &sentinel;
        _size = 0;
    }

    iterator begin(){
        return iterator(head);
    }

    iterator end(){
        return iterator(&sentinel);
    }

private:
    size_t _size;
    node* head;
    node sentinel;
};



}

#endif // UTILITY_H
