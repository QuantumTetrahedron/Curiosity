
#pragma once

#include <string>
#include <map>
#include <vector>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "Mesh.h"
#include "Texture.h"

class Model
{
public:
    Model(){}
    Model(std::string const &path);

    void Draw(const Shader& shader) const;
    void DrawInstanced(const Shader& shader, int count) const;

    std::vector<Texture> textures_loaded;
    std::vector<Mesh> meshes;
    std::string directory;

    float height;

private:

    void loadModel(std::string const &path);

    void processNode(aiNode* node, const aiScene* scene);

    Mesh processMesh(aiMesh* mesh, const aiScene* scene);

    std::vector<Texture> loadMaterialTextures(aiMaterial *mat, aiTextureType type, std::string typeName, bool gammaCorrection);
};
