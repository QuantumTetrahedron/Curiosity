# Curiosity

Curiosity is a 3D logic game where you have to scan the surrounding elements and print their copies to find a way to the next level.

# Status

This project is currently suspended as the code grew to be too complex to maintain. It will be resumed as soon as the Tetrahedron Engine will be operational enough.
The project will be completely refactored.

## Download link

Latest version precompiled for Linux.

```
https://drive.google.com/file/d/1lNeSozW2HWt37YMXwTHVnq2ssUtFx2G9/view?usp=sharing
```

### Instructions

1) Download
2) Extract
3) ./Curiosity.run
(You might need chmod 744 Curiosity.run)
This will also install the required packages
4) cd Release
5) ./Curiosity
6) Enjoy

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software:

```
GLFW3
GLM
Assimp
Freetype 2
```

### Compile flags

-std=c++14

### Linker options for linux

```
-lglfw3
-lGL
-lX11
-lXi
-lXrandr
-lXxf86vm
-lXinerama
-lrt
-lm
-ldl
-lpthread
-lXcursor
-lassimp
-lfreetype
```

## Playing the game

The game takes place on a grid.
Click an unoccupied tile to create a path. Click on it again to confirm movement.
Right clicking while moving will cancel the move if moving or erase the path if not confirmed.

On the bottom left of the screen there is a HUD showing 4 memory slots.
Right click on an in game object while standing on a tile next to it to scan that object.
With the scan in your memory tou can right-click on a free tile to place a copy of that item in the level.
The copy's rotation will be the same as it was when scanned (relative to the character).
When multiple objects are in your memory you will need to switch between them. You can use the 1-4 keys for that.
To delete a previously placed item you need to switch the mode of the character. To do so press and hold the left shift key.
The interface will now change and you will see the placed objects in your memory.
Select one of them by using the 1-4 keys and then press that same key again and hold to erase the item.
Similarly if you wish to forget an object scanned to make room for other object's data, hold the appropiate 1-4 key while the shift key is not pressed.

When your vision gets obstructed or an object makes it impossible to select the desired destination of your actions you can try to move the camera up or down by using the mouse scroll.
If that did not help press the q or e keys to rotate the camera by 90 degrees around the character.

### In game objects

* **Cage** - An object that doesn't do much. It is possible to use them to block lasers or walk over them.
* **Door** - Doors can open when they are powered with electricity.
* **Laser** - A laser emitter. Lasers block your way, if however a laser does hit you, you do not die as that is not yet implemented. It does still block your movement so you will need to get rid of it or reset the game to continue.
* **Lever** - Should power an object with electricity but this is currently not possible as the character does not have tiny robotic arms to pull it. It will be possible to pull it in the future by using magnets.
* **Magnet** - When an electict current appears, a magnetic impulse is created attracting the first object on its way. When the electricity disappears an impulse is created that repulses the first object on its way. For now only cages can be pulled or pushed with a magnet but this will change.
* **Mirror** - Deflects the incoming laser rays. Does not really look like a mirror though.
* **Photodetector** - Powers an object with electricity if a ray of laser hits it from the appropiate side. For now it is impossible to predict what the level designer set as it's target but an interface will be implemented someday that will allow to see every in level connection with ease. For now though just place them near its target to avoid confusing players.

### Making your own levels

Levels are easy to build using just a text editor.
The syntax is explained in comments of the test.lvl file.
To test your level you must manually set it's path in the Game.cpp file at line 144 or just save it as 'test.lvl'.

### Testing shaders

To enable experimental shading options:
* hold down the **L** key to change the light calculations from **lasers**. - The experimental version still experiences artifacts and for a better effect each point of the ray should be taken into account.
* hold down the **spacebar** to enable experimental **PBR**. - In the experimental version, the normal distribution function has the normalization factor (1/pi*alpha^2) removed.

## Authors

* **Bartosz Borecki**
* **Magdalena Holewa**

## Acknowledgments

* Joey de Vries and his site learnopengl.com
