#ifndef BOX_COLLIDER_H
#define BOX_COLLIDER_H

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

/** Box collider */
class BoxCollider{
public:
    glm::vec3 position;
    float height;
    bool floor;

    /**
    * Box collider constructor.
    * params:
    * * pos : world space position of boxed object.
    * * _height : height of the boxed object.
    * * _floor : determines whether the boxed object's position is relative to its top.
    */
    BoxCollider(glm::vec3 pos = glm::vec3(0.0f), float _height = 1.0f, bool _floor = false);

    /**
    * Determines whether a given point is inside the collider.
    * params:
    * * point : the tested point.
    * returns:
    * * true if the point is inside the box, false otherwise.
    */
    bool Collided(glm::vec3 point) const;
private:
    /**
    * Helper function to determine whether a given double lays between given boundaries on the real axis.
    * params:
    * * value : a given value to check.
    * * leftBound : the left boundary.
    * * rightBound : the right boundary.
    * returns:
    * * true if the value is between the bounds, false otherwise.
    */
    bool Between(double value, double leftBound, double rightBound) const;
};

#endif // BOX_COLLIDER_H
