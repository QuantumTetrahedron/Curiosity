#include "Camera.h"

Camera::Arm::Arm()
: angle(0.0), targetAngle(0.0), radius(RADIUS), rotating(false){}

glm::vec2 Camera::Arm::angleToDirection(){
    return glm::vec2(glm::sin(glm::radians(angle)), glm::cos(glm::radians(angle)));
}

glm::vec3 Camera::Arm::Offset(){
    glm::vec2 dir = angleToDirection();
    return glm::vec3(dir.x*(8), radius, dir.y*(8));
}

void Camera::Arm::Update(double dt){
    if(rotating){
        double speed = 90.0;
        double v = speed*dt;
        if(targetAngle < angle) v *= -1;
        if(glm::abs(targetAngle-angle)>v){
            angle += v;
        }
        else{
            angle = targetAngle;
            rotating = false;
            if(angle < 0)
                angle += 360;
            if(angle >= 360)
                angle -= 360;
        }
    }
}

Camera::Camera()
: Camera(1600,900,nullptr,glm::vec3(0.0f, 1.0f, 0.0f)){}

Camera::Camera(int width, int height, std::shared_ptr<GameObject> parent, glm::vec3 up)
: Yaw(YAW), Pitch(PITCH), MovementSpeed(SPEED), MouseSensitivity(SENSITIVITY), Zoom(ZOOM)
{
    Width = width;
    Height = height;
    WorldUp = up;
    if(parent)
        SetParent(parent);
    else
        SetTarget(glm::vec3(0.0f));
}

glm::mat4 Camera::GetViewMatrix() const
{
    return glm::lookAt(Position, Target, Up);
}

void Camera::SetTarget(glm::vec3 pos){
    Anchor = pos;
    glm::vec2 dir = arm.angleToDirection();
    Target = Anchor + glm::vec3(dir.x*(-2),0,dir.y*(-2));
    Position = Anchor + arm.Offset();
    updateCameraVectors();
}

void Camera::SetParent(std::shared_ptr<GameObject> newParent){
    parent = newParent;
    if(!newParent)return;
    SetTarget(newParent->transform.position);
}

void Camera::RotateRight(){
    arm.targetAngle = arm.angle+90.0;
    arm.rotating = true;
}

void Camera::RotateLeft(){
    arm.targetAngle = arm.angle-90.0;
    arm.rotating = true;
}

bool Camera::Rotating(){
    return arm.rotating;
}

void Camera::Update(double dt){
    if(parent)
        Anchor = parent->transform.position;
    arm.Update(dt);
    Position = Anchor + arm.Offset();
    updateCameraVectors();
}

void Camera::ProcessMouseScroll(double yoffset){
    if(arm.radius >= 1.0 && arm.radius<=24.0)
        arm.radius -= yoffset;
    if(arm.radius < 8.0f)
        arm.radius = 8.0f;
    if(arm.radius > 24.0f)
        arm.radius = 24.0f;
    Position = Anchor + arm.Offset();
    updateCameraVectors();
}

void Camera::updateCameraVectors(){
    glm::vec2 dir = arm.angleToDirection();
    Target = Anchor + glm::vec3(dir.x*(-2),0,dir.y*(-2));
    Front = glm::normalize(Target-Position);
    Right = glm::normalize(glm::cross(Front, WorldUp));
    Up    = glm::normalize(glm::cross(Right, Front));
    Forward = glm::normalize(glm::cross(WorldUp, Right));
}
