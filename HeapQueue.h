#ifndef HEAP_QUEUE_H
#define HEAP_QUEUE_H

#include<vector>

template<typename T>
class QueueNode{
public:
    T item;
    int priority;

    QueueNode(T it, int p)
    :item(it), priority(p){}
};

/**
* A priority queue
*/
template<typename T>
class HeapQueue{
public:
    HeapQueue(){}
    size_t size(){
        return queue.size();
    }
    void Enqueue(T item, int priority){
        queue.push_back(QueueNode<T>(item, priority));
        int ci = queue.size()-1;
        while (ci>0)
        {
            int pi = (ci-1)/2;
            if(queue[ci].priority >= queue[pi].priority)
                break;
            auto tmp = queue[ci];
            queue[ci] = queue[pi];
            queue[pi] = tmp;
            ci = pi;
        }
    }
    T Dequeue(){
        int li = queue.size()-1;
        auto frontItem = queue[0];
        queue[0] = queue[li];
        queue.pop_back();
        --li;
        int pi = 0;
        while(true){
            int ci = pi*2+1;
            if(ci>li) break;
            int rc = ci+1;
            if(rc<=li&&queue[rc].priority < queue[ci].priority)
                ci = rc;
            if(queue[pi].priority <= queue[ci].priority) break;
            auto tmp = queue[pi];
            queue[pi] = queue[ci];
            queue[ci] = tmp;
            pi = ci;
        }
        return frontItem.item;
    }
private:
    std::vector<QueueNode<T>> queue;
};

#endif
