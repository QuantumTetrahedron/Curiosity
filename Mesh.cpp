#include "Mesh.h"
#include <iostream>
#include <sstream>

Mesh::Mesh(std::vector<Vertex> vertices, std::vector<GLuint> indices, std::vector<Texture> textures){
    this->vertices = vertices;
    this->indices = indices;
    this->textures = textures;

    this->setupMesh();
}

Mesh::~Mesh(){
    //glDeleteVertexArrays(1, &VAO);
    //glDeleteBuffers(1, &VBO);
    //glDeleteBuffers(1, &EBO);
}

Mesh::Mesh(Mesh&& other)
    : indices(other.indices), vertices(other.vertices), textures(other.textures){
    VAO = other.VAO;
    VBO = other.VBO;
    EBO = other.EBO;
    other.VAO = 0;
    other.VBO = 0;
    other.EBO = 0;
}

Mesh& Mesh::operator=(Mesh&& other){
    indices = other.indices;
    vertices = other.vertices;
    textures = other.textures;
    VAO = other.VAO;
    VBO = other.VBO;
    EBO = other.EBO;
    other.VAO = 0;
    other.VBO = 0;
    other.EBO = 0;
}

void Mesh::SetupDraw(const Shader& shader) const{
    shader.Use();

    unsigned int albedoNr = 1;
    unsigned int metallicNr = 1;
    unsigned int roughnessNr = 1;
    unsigned int normalNr = 1;
    unsigned int occlusionNr = 1;
    for(unsigned int i=1; i < textures.size()+1; ++i){
        glActiveTexture(GL_TEXTURE0 + i);

        std::stringstream ss;
        std::string number;
        std::string name = textures[i-1].type;
        if(name == "texture_albedo")
            ss << albedoNr++;
        else if(name == "texture_metallic")
            ss << metallicNr++;
        else if(name == "texture_roughness")
            ss << roughnessNr++;
        else if(name == "texture_normal")
            ss << normalNr++;
        else if(name == "texture_ao")
            ss << occlusionNr++;
        number = ss.str();
        shader.SetInteger((name+number).c_str(), i);
        glBindTexture(GL_TEXTURE_2D, textures[i-1].id);
    }
}

void Mesh::Draw(const Shader& shader) const{
    SetupDraw(shader);

    glBindVertexArray(VAO);
    glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);

    glActiveTexture(GL_TEXTURE0);
}

void Mesh::DrawInstanced(const Shader& shader, int count) const{
    SetupDraw(shader);

    glBindVertexArray(VAO);
    glDrawElementsInstanced(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0, count);
    glBindVertexArray(0);

    glActiveTexture(GL_TEXTURE0);
}

void Mesh::setupMesh(){
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);

    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);

    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), &indices[0], GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)0);

    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, Normal));

    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)offsetof(Vertex, TexCoords));

    glBindVertexArray(0);
}
