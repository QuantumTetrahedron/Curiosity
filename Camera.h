#ifndef CAMERA_H
#define CAMERA_H

#include "GameObject.h"
#include <vector>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

///Camera constants
const double YAW        = -90.0f;
const double PITCH      =  0.0f;
const double SPEED      =  3.0f;
const double SENSITIVITY =  0.25f;
const double ZOOM       =  45.0f;
const double RADIUS     =  16.0f;

/** Camera class
* Used to process input and calculate the view matrix accordingly.
*/
class Camera
{
public:
    /**
    * Arm structure used to calculate camera's offset
    */
    struct Arm{
        /**
        * Arm constructor.
        */
        Arm();

        double angle;
        double targetAngle;
        bool rotating;
        double radius;

        /**
        * Calculates the direction vector used for offset calculations
        * based on the rotation angle.
        * returns:
        * * the direction vector.
        */
        glm::vec2 angleToDirection();

        /**
        * Calculates the camera's offset.
        * returns:
        * * the camera's offset.
        */
        glm::vec3 Offset();

        /**
        * Updates the angle over time.
        * params:
        * * dt : delta time.
        */
        void Update(double dt);
    };

    glm::vec3 Position;
    glm::vec3 Target;
    glm::vec3 Anchor;
    glm::vec3 Front;
    glm::vec3 Up;
    glm::vec3 Right;
    glm::vec3 WorldUp;
    glm::vec3 Forward;

    Arm arm;

    double Yaw;
    double Pitch;

    double MovementSpeed;
    double MouseSensitivity;
    float Zoom;

    int Width;
    int Height;

    std::shared_ptr<GameObject> parent;

    /// Default constructor
    Camera();

    /**
    * Camera constructor
    * params:
    * * width : viewport width
    * * height : viewport height
    * * parent : A target for the camera to follow
    * * up : direction vector pointing in the world's up direction
    * *      needed to calculate other camera vectors.
    */
    Camera(int width, int height, std::shared_ptr<GameObject> parent, glm::vec3 up);

    /**
    * Returns the matrix representing the linear transformation
    * from world space to view space.
    * returns:
    * * the view matrix.
    */
    glm::mat4 GetViewMatrix() const;

    /** TODO: Check if works **/
    /** Sets the camera target to another position.
    * params:
    * * pos : position of the new camera target.
    */
    void SetTarget(glm::vec3 pos);

    /** Sets a parent for the camera to follow.
    * params:
    * * newParent: the parent to follow
    */
    void SetParent(std::shared_ptr<GameObject> newParent);

    /**
    * Rotates the camera 90 degrees right around the anchor.
    */
    void RotateRight();

    /**
    * Rotates the camera 90 degrees left around the anchor.
    */
    void RotateLeft();

    /**
    * Checks whether the camera is rotating.
    * returns:
    * * true if the camera is rotating, false otherwise.
    */
    bool Rotating();

    /**
    * Updates the camera position and/or rotation
    * based on the parent's position and previous
    * rotation methods called.
    * params:
    * * dt : delta time
    */
    void Update(double dt);

    /**
    * Updates the camera offset based on mouse scroll input.
    * params:
    * * yoffset : mouse scroll offset.
    */
    void ProcessMouseScroll(double yoffset);

private:

    /**
    * Calculates the camera vectors needed
    * to calculate the view matrix.
    */
    void updateCameraVectors();
};

#endif // CAMERA_H
