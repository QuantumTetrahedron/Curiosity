#ifndef POWERSOURCE_H_INCLUDED
#define POWERSOURCE_H_INCLUDED

#include "Powered.h"

class PowerSource {
public:
    PowerSource();
    PowerSource(glm::vec3 target);
    glm::vec3 GetPowerTarget();
    virtual void GivePower();
    virtual void CutPower();
protected:
    bool connected;
    glm::vec3 powerTarget;
};

#endif // POWERSOURCE_H_INCLUDED
