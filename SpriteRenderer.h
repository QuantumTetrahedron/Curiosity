#ifndef SPRITE_RENDERER_H
#define SPRITE_RENDERER_H
#include "ResourceManager.h"

class SpriteRenderer
{
public:
    /**
    * Draws a sprite.
    * params:
    * * texture : texture to draw.
    * * position : screen coordinates.
    * * size : width and height of the sprite.
    * * rotate : angle of rotation.
    * * color : color tint.
    */
    void DrawSprite(const Texture2D& texture, const glm::vec2& position, const glm::vec2& size, float rotate = 0.0f, const glm::vec3& color = glm::vec3(1.0f)) const;

    /**
    * Draws a radially filled sprite.
    * params:
    * * texture : texture to draw.
    * * percent : percent of the radial to draw.
    * * position : screen coordinates.
    * * size : width and height of the sprite.
    * * rotate : angle of rotation.
    * * color : color tint.
    */
    void DrawRadial(const Texture2D &texture, float percent, const glm::vec2& position, const glm::vec2& size, float rotate = 0.0f, const glm::vec3& color = glm::vec3(1.0f)) const;

    /**
    * Initializes rendering data.
    */
    void initRenderData();
private:
    unsigned int quadVAO, radialVAO, radialVBO;
    Shader shader;
};

#endif
