#ifndef A_STAR_H
#define A_STAR_H

#include <map>
#include <algorithm>
#include <memory>
#include "HeapQueue.h"
#include "Tile.h"
#include "Utility.h"

/** A* Pathfinding
* Implements the A* algorithm
* using a heap queue to find
* the shortest path between
* two tiles.
*/
class AStarPathfinding{
public:
    /**
    * Finds the shortest path between two given tiles in a directed graph
    * using the A* algorithm.
    *
    * params:
    * * edges : A map representing the directed graph of tiles with a cost assigned to every edge.
    * *
    * * originNode : A pointer to the tile of origin.
    * *
    * * destinationNode : A pointer to the tile of destination
    */
    std::vector<std::shared_ptr<Tile>> FindPath(std::map<std::shared_ptr<Tile>, std::map<std::shared_ptr<Tile>,int>>& edges, std::shared_ptr<Tile> originNode, std::shared_ptr<Tile> destinationNode);

    /**
    * Uses the relative location of tiles passed in a map to find the edges of the graph.
    * params:
    * * tiles : a map of tiles mapped to their location.
    *
    * returns:
    * * A map representing a directed graph, every node in the graph is mapped
    * * to a container of reachable neighbours and the cost of entering that neighbour.
    */
    std::map<std::shared_ptr<Tile>, std::map<std::shared_ptr<Tile>, int>> GetGraphEdges(const std::map<glm::vec3, std::shared_ptr<Tile>, util::vec3Comparator>& tiles);
private:

    /**
    * Finds the tiles connected to the passed node
    * with an edge from the passed map
    * params:
    * * node : a node for which the neighbours
    * * edges : each tile is mapped to a container of tiles, neighbours, which are mapped to an integer representing the cost of entry.
    * *         note that the cost is not needed in the implementation of the method, however to optimize performance we reuse a container
    * *         that already exists for other reasons, instead of creating a new one for every call.
    *
    * returns:
    * * a vector of tiles connected with the node.
    */
    std::vector<std::shared_ptr<Tile>> GetNeighbours(std::shared_ptr<Tile> node, const std::map<std::shared_ptr<Tile>,std::map<std::shared_ptr<Tile>,int>>& edges);

    /**
    * The heuristic used by the algorithm.
    * params : tiles to check
    * returns : distance between tiles
    */
    int Heuristic(std::shared_ptr<Tile> a, std::shared_ptr<Tile> b);
};

#endif // A_STAR_H
