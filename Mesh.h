
#pragma once

#include <string>
#include <vector>
#include "Shader.h"

#include <assimp/Importer.hpp>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

struct Vertex {
    glm::vec3 Position;
    glm::vec3 Normal;
    glm::vec2 TexCoords;
    //glm::vec3 Tangent;
    //glm::vec3 Bitangent;
};

struct Texture {
    unsigned int id;
    std::string type;
    aiString path;
};

class Mesh {
public:
    std::vector<Vertex> vertices;
    std::vector<unsigned int> indices;
    std::vector<Texture> textures;

    Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, std::vector<Texture> textures);
    ~Mesh();
    Mesh(const Mesh& other) = delete;
    Mesh(Mesh&& other);
    Mesh& operator=(const Mesh& other) = delete;
    Mesh& operator=(Mesh&& other);
    void Draw(const Shader& shader) const;
    void DrawInstanced(const Shader& shader, int count) const;

    unsigned int VAO, VBO, EBO;
private:
    void SetupDraw(const Shader& shader) const;
    void setupMesh();
};


