#ifndef TEXT_RENDERER_H
#define TEXT_RENDERER_H

#include <iostream>
#include <map>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <ft2build.h>
#include FT_FREETYPE_H

#include "Shader.h"

struct Character{
    unsigned int textureID;
    glm::ivec2 size;
    glm::ivec2 bearing;
    unsigned int advance;
};

class TextRenderer {
public:
    TextRenderer(const Shader& shader);
    void Load(std::string font, unsigned int fontSize);
    void Render(std::string text, float posX, float posY, double scale = 1.0, glm::vec3 color = glm::vec3(1.0f));
    float GetStringWidth(const std::string &text);
    float GetStringHeight(const std::string &text);

    std::map<char, Character> characters;
    const Shader& textShader;
private:
    unsigned int VAO, VBO;
};

#endif // TEXT_RENDERER_H
