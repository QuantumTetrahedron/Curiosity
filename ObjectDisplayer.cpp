#include "ObjectDisplayer.h"
#include "ResourceManager.h"

void ObjectDisplayer::Init(int wndW, int wndH){
    windowW = wndW;
    windowH = wndH;
}

void ObjectDisplayer::Display(std::shared_ptr<GameObject> obj, int xPos, int yPos, int viewportW, int viewportH, Shader& shader, glm::vec3 tint){
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glViewport(xPos,yPos,viewportW,viewportH);

        shader.Use();
        shader.SetMatrix4("view", getView(getHeight(obj)));

        glm::vec3 oldPos = obj->transform.position;
        obj->transform.position = glm::vec3(0.0f);
        if(tint.x >= 0.0f && tint.y >= 0.0f && tint.z >= 0.0f){
            shader.SetVector3f("colorTint", tint);
        }
        obj->Draw(shader);
        obj->transform.position = oldPos;
        glViewport(0,0,windowW,windowH);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
}

glm::mat4 ObjectDisplayer::getView(float height){
    return glm::lookAt(glm::vec3(1.0,height/2.0f,-height/2.0f-3.0f), glm::vec3(0.0,height/2.0f,0.0), glm::vec3(0.0,1.0,0.0));
}

float ObjectDisplayer::getHeight(std::shared_ptr<GameObject> obj){
    return ResourceManager::GetModel(obj->modelName).height;
}
