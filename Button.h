#ifndef BUTTON_H_INCLUDED
#define BUTTON_H_INCLUDED

#include "TextRenderer.h"
#include "SpriteRenderer.h"
#include "Input.h"
#include <memory>

class Button {
public:
    Button();
    Button(std::shared_ptr<SpriteRenderer> imageRenderer, std::shared_ptr<TextRenderer> fontRenderer, std::string text,
           glm::vec2 pos, glm::vec2 size, Texture2D sprite, glm::vec3 color = glm::vec3(1.0f));
    bool IsClicked(const Input& mouse) const;
    bool IsHovered(const Input& mouse) const;
    void Update(const Input& mouse);
    void Draw() const;

    bool active;

    glm::vec2 imagePosition;
    glm::vec2 imageSize;
    glm::vec2 textPosition;
    std::string label;
    glm::vec3 color;
    Texture2D sprite;

private:
    std::shared_ptr<SpriteRenderer> renderer;
    std::shared_ptr<TextRenderer> textRenderer;
};

#endif // BUTTON_H_INCLUDED
