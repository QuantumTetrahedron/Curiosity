#include <string>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "Game.h"
#include "Cutscenes.h"
#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

//#include "Utility.h"

GLuint screenWidth = 1600, screenHeight = 900;
//GLuint screenWidth = 1920, screenHeight = 1080;

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);

Input input;
Game game(screenWidth, screenHeight);

GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;

int nbFrames = 0;
GLfloat lastTime = 0.0f;
int lastFPS = 0;
int showFPS()
{
     // Measure speed
     double currentTime = glfwGetTime();
     nbFrames++;
     if ( currentTime - lastTime >= 1.0 ){ // If last cout was more than 1 sec ago
         lastFPS = nbFrames;
         nbFrames = 0;
         lastTime += 1.0;
     }
     return lastFPS;
}

int main()
{
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
    glfwWindowHint(GLFW_SAMPLES, 4);

    /** FULLSCREEN **/
    ///GLFWwindow* window = glfwCreateWindow(screenWidth, screenHeight, "Curiosity", glfwGetPrimaryMonitor(), nullptr);
    /** WINDOWED **/
    GLFWwindow* window = glfwCreateWindow(screenWidth, screenHeight, "Curiosity", nullptr, nullptr);

    glfwMakeContextCurrent(window);

    glfwSetKeyCallback(window, key_callback);
    glfwSetCursorPosCallback(window, mouse_callback);
    glfwSetScrollCallback(window, scroll_callback);
    glfwSetMouseButtonCallback(window, mouse_button_callback);
    #ifdef __linux__
    Cutscenes::SetWindow(window);
    #endif
    gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
    glGetError();

    glViewport(0, 0, screenWidth, screenHeight);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    //glEnable(GL_MULTISAMPLE);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    input.keyListener.RegisterKey(GLFW_KEY_1);
    input.keyListener.RegisterKey(GLFW_KEY_2);
    input.keyListener.RegisterKey(GLFW_KEY_3);
    input.keyListener.RegisterKey(GLFW_KEY_4);

    game.Init();

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    while(!glfwWindowShouldClose(window))
    {
        //showFPS();
        GLfloat currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        glfwPollEvents();
        game.Update(deltaTime);

        input.UpdateKeyListener(deltaTime);

        game.ProcessInput(input);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        game.Render();

        game.PrintFPS(showFPS());

        glfwSwapBuffers(window);
    }

    glfwTerminate();
    return 0;
}


void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
    if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);

    if(action == GLFW_PRESS)
        input.keys[key] = true;
    else if(action == GLFW_RELEASE)
        input.keys[key] = false;
}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods){

    if(action == GLFW_PRESS)
    {
        input.mouseButtons[button] = true;
    }
    //else if(action == GLFW_RELEASE)
    //    input.mouseButtons[button] = false;
}
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
    double x, y;
    glfwGetCursorPos(window, &x, &y);
    input.mouseX = x;
    input.mouseY = y;
    //game.input.mousePressed = true;
}

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
    input.scrollOffset = yoffset;
}

