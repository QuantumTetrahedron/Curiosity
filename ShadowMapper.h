#ifndef SHADOW_H
#define SHADOW_H
#include "Shader.h"
#include "Renderer.h"

/**
* Used to create a shadow map.
*/
class ShadowMapper{
public:
    /**
    * Initializes the shadow mapper.
    * params:
    * * shadowWidth : width of the created shadow map.
    * * shadowHeight : height of the created shadow map.
    * * windowWidth : width of the window.
    * * windowHeight : height of the window.
    */
    void Init(int shadowWidth, int shadowHeight, int windowWidth, int windowHeight);

    /**
    * Starts the shadow mapping.
    */
    void Enable();

    /**
    * Ends the shadow mapping.
    */
    void Disable();

    /**
    * Binds the created shadow map.
    */
    void BindTexture();

    Shader& GetShader(bool instanced){
        if(instanced)
            return instShader();
        else
            return shader();
    }

private:
    /// Retrieves the appropriate shader.
    Shader& shader();
    Shader& instShader();

    /// Initializes rendering data.
    void InitRenderData();
    unsigned int depthMapFBO;
    unsigned int depthMap;
    int width, height;
    int wndW, wndH;
};

#endif
