#include "TestObj.h"
#include "Utility.h"
#include "LevelManager.h"

std::unique_ptr<GameObject> TestObj::clone() const{
    return std::make_unique<TestObj>(*this);
}

std::unique_ptr<GameObject> TestObj::CopyAt(glm::vec3 pos, glm::vec3 rot, bool safeCopy){
    std::unique_ptr<TestObj> obj(dynamic_cast<TestObj*>(Tile::CopyAt(pos, rot).release()));
    return obj;
}

TestObj::TestObj(glm::vec3 position, glm::vec3 rotation, glm::vec3 scale, std::string modelName, glm::vec3 tint, bool addCollider, bool canEnter)
    : Tile(position, rotation, scale, modelName, 1, glm::vec3(0.0f,2.0f,0.0f), false){
    trespassable = canEnter;
    blocking = true;
    scannable = true;
    moving = false;
}

TestObj::TestObj(std::string str){
    std::regex pattern{R"((\w{1})\s+(\S+)\s+(\S+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\S{1}))"};
    std::smatch matches;
    if(std::regex_match(str, matches, pattern)){
        std::string name = matches[3];
        std::string type = matches[2];

        int x = atoi(((std::string)matches[4]).c_str());
        int y = atoi(((std::string)matches[5]).c_str());
        int z = atoi(((std::string)matches[6]).c_str());
        int r = atoi(((std::string)matches[7]).c_str());

        *this = TestObj(glm::vec3(2*x, y, 2*z), glm::vec3(0,90*r,0), glm::vec3(1.0f), name);
    }
}


void TestObj::Activate() {
    std::cout << "activated" << std::endl;
}

bool TestObj::CheckCollision(){
    auto tiles = LevelManager::GetAllTiles();
    auto xPos = transform.position + glm::vec3(force.x,0.0f,0.0f);
    auto yPos = transform.position + glm::vec3(0.0f,force.y,0.0f);
    auto zPos = transform.position + glm::vec3(0.0f,0.0f,force.z);
    auto newPos = transform.position + force;
    return  (glm::abs(force.x) > 0.01f && (LevelManager::hasBlockingObject(xPos) || tiles.find(xPos)==std::end(tiles))) ||
            (glm::abs(force.y) > 0.01f && (LevelManager::hasBlockingObject(yPos) || tiles.find(yPos)==std::end(tiles))) ||
            (glm::abs(force.z) > 0.01f && (LevelManager::hasBlockingObject(zPos) || tiles.find(zPos)==std::end(tiles))) ||
            (LevelManager::hasBlockingObject(newPos) || tiles.find(newPos)==std::end(tiles));
}

void TestObj::Update(double dt){
    if(glm::length(force) != 0){
        if(target.position == transform.position)
        {
            if(CheckCollision())
                force = glm::vec3(0.0f);
            else{
                target.position = transform.position + force;
                auto objOnTop = LevelManager::GetObject(transform.position+TilePositionOffset);
                if(objOnTop)
                    objOnTop->target.position = target.position + TilePositionOffset;
                moving = true;
            }
        }

        if(moving){
            float speed = 15.0f;
            float v = dt*speed;

            int c =0;
            if(MoveTo(transform.position.x, target.position.x, v))++c;
            if(MoveTo(transform.position.y, target.position.y, v*0.9f))++c;
            if(MoveTo(transform.position.z, target.position.z, v))++c;
            if(collider){
                collider->position = transform.position;
            }
            if(c == 3){
                moving = false;
                Relocate();
            }

            highlighter.transform.position = transform.position + glm::vec3(0.0f, 2.25f, 0.0f);
        }
    }
    else{
        Tile::Update(dt);
    }
}

void TestObj::Fall(){
    AddForce(glm::vec3(0.0f, -2.0f, 0.0f));
    moving = true;
}

void TestObj::Attract(float fromRot){
    glm::vec3 dp = glm::vec3(0.0f);
    if(fromRot == 0) dp = glm::vec3(-2,0,0);
    else if(fromRot == 90) dp = glm::vec3(0,0,2);
    else if(fromRot == 180) dp = glm::vec3(2,0,0);
    else if(fromRot == 270) dp = glm::vec3(0,0,-2);
    AddForce(dp);
    moving = true;
}

void TestObj::Repulse(float fromRot){
    glm::vec3 dp = glm::vec3(0.0f);
    if(fromRot == 0) dp = glm::vec3(-2,0,0);
    else if(fromRot == 90) dp = glm::vec3(0,0,2);
    else if(fromRot == 180) dp = glm::vec3(2,0,0);
    else if(fromRot == 270) dp = glm::vec3(0,0,-2);
    AddForce(-dp);
    moving = true;
}
