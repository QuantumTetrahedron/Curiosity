#include "HUDmanager.h"
#include <string>
#include <vector>
#include "Utility.h"

void HUDmanager::Init(int width, int height){
    windowHeight = height;
    windowWidth = width;
    renderer.initRenderData();
}

void HUDmanager::AddElement(std::shared_ptr<HUDelement> element){
    elements.push(element);
}

void HUDmanager::Render(){
    for(const auto& e : elements)
        e->Draw(windowWidth, windowHeight, renderer);
}

void HUDmanager::Clear(){
    elements.clear();
}
