#include "LevelManager.h"
#include "GameObject.h"
#include "ResourceManager.h"
#include "Utility.h"

GameObject::GameObject(glm::vec3 position, glm::vec3 rotation, glm::vec3 scale, std::string modelName, bool addCollider, bool scannable, bool canEnter, bool isFloor)
: modelName(modelName), blocking(!canEnter), scannable(scannable){
    this->transform.position = position;
    this->transform.rotation = rotation;
    this->transform.scale = scale;
    inLevelPos = position;

    target = transform;

    if(addCollider)
        this->collider = std::experimental::make_optional(BoxCollider(position, ResourceManager::GetModel(modelName).height, isFloor));
    else
        this->collider = {};
}

std::unique_ptr<GameObject> GameObject::clone() const {
    return std::make_unique<GameObject>(*this);
}

std::unique_ptr<GameObject> GameObject::CopyAt(glm::vec3 pos, glm::vec3 rot, bool safeCopy){
    std::unique_ptr<GameObject> obj = clone();
    obj->transform.position = pos;
    obj->target.position = pos;
    if(obj->collider)
        obj->collider.value().position = pos;
    obj->transform.rotation = rot;
    obj->target.rotation = rot;
    if(!safeCopy)
        obj->inLevelPos = pos;
    return obj;
}

void GameObject::Draw(const Shader& shader) {
    Model& model = ResourceManager::GetModel(modelName);
    glm::mat4 m = GetModelMatrix();
    shader.Use();
    shader.SetMatrix4("model", m);
    model.Draw(shader);
}

void GameObject::Fall(){
    auto tiles = LevelManager::GetAllTiles();
    try{tiles.at(target.position-glm::vec3(0.0f,2.0f,0.0f));}
    catch(...){return;}

    target.position.y -= 2.0f;
    if(collider)
        collider.value().position.y -= 2.0f;
}

glm::mat4 GameObject::GetModelMatrix() const{
    glm::mat4 m;
    m = glm::translate(m, transform.position);
    if(glm::length(transform.rotation)>0.0f)
    {
        m = glm::rotate(m, glm::radians(transform.rotation.y), glm::vec3(0.0f,1.0f,0.0f));
        m = glm::rotate(m, glm::radians(transform.rotation.z), glm::vec3(0.0f,0.0f,1.0f));
        m = glm::rotate(m, glm::radians(transform.rotation.x), glm::vec3(1.0f,0.0f,0.0f));
    }
    m = glm::scale(m, transform.scale);
    return m;
}

void GameObject::Update(double dt){
    if(transform != target)
    {
        float speed = 15.0f;
        float v = dt*speed;

        if(transform.position != target.position)
        {
            int c = 0;
            if(MoveTo(transform.position.x, target.position.x, v))++c;
            if(MoveTo(transform.position.y, target.position.y, v))++c;
            if(MoveTo(transform.position.z, target.position.z, v))++c;
            if(collider){
                collider->position = transform.position;
            }
            if(c == 3)
                Relocate();
        }

        if(transform.rotation != target.rotation)
        {
            RotateTo(transform.rotation.x, target.rotation.x, v*50);
            RotateTo(transform.rotation.y, target.rotation.y, v*50);
            RotateTo(transform.rotation.z, target.rotation.z, v*50);
        }
    }
}

void GameObject::Relocate(){
    LevelManager::SetToRelocate(inLevelPos, transform.position);
    inLevelPos = transform.position;
}
