#include "LevelManager.h"
#include "Player.h"

std::map<glm::vec3, std::shared_ptr<Tile>, util::vec3Comparator> LevelManager::Tiles;
std::map<glm::vec3, std::shared_ptr<GameObject>, util::vec3Comparator> LevelManager::Obstackles;
std::map<glm::vec3, std::shared_ptr<GameObject>, util::vec3Comparator> LevelManager::Objects;
std::map<glm::vec3, std::pair<int, std::shared_ptr<GameObject>>, util::vec3Comparator> LevelManager::Printed;
std::list<std::shared_ptr<LaserRay>> LevelManager::LaserRays;
bool LevelManager::checkLaserRays = true;
std::vector<std::pair<glm::vec3,glm::vec3>> LevelManager::Relocator;
glm::vec3* LevelManager::playerPos = nullptr;

std::map<glm::vec3, Event, util::vec3Comparator> LevelManager::Events;
Connector LevelManager::connector;
LevelState LevelManager::state;

std::array<int, 5> LevelManager::deathMessageSubset;
std::vector<std::string> LevelManager::deathMessages {
    "warning: comparison between signed and unsigned integer expressions",
    "Exception in thread \"main\" java.lang.NullPointerException",
    "error: no match for 'operator<' in 'ld.vsip::lud<T, by_reference>::solve",
    "I cannot be cast to java.lang.Integer at Solution.main(Solution.java:23)",
    "warning: incompatible implicit declaration of built-in function ‘exit’",
    "undefined reference to `pthread_create`",
    "5 packets transmitted, 0 received, 100% packet loss, time 4032ms",
    "Error 502: Bad gateway",
    "Segmentation fault",
    "...",
    "..."
};

void LevelManager::Print(glm::vec3 pos, std::shared_ptr<GameObject> obj, int slot){
    Printed[pos] = std::make_pair(slot, obj);
    auto tiles = GetAllTiles();
    try{
        tiles.at(pos)->occupied = true;
        tiles.at(pos)->trespassable = !obj->IsBlocking();
    }
    catch(...){}

    checkLaserRays = true;
}

void LevelManager::Delete(int n){
    auto tiles = GetAllTiles();
    auto it = Printed.begin();
    for(;it!=Printed.end();++it)
    {
        if(it->second.first == n)
            break;
    }

    if(it != Printed.end()){
        it->second.second->CleanUp();
        it->second.second = nullptr;
        int slotErased = it->second.first;
        glm::vec3 pos = it->first;
        try{
            std::shared_ptr<Tile> t = tiles.at(pos);
            t->occupied = false;
            t->trespassable = true;
        }catch(...){}
        Printed.erase(it);

        for(auto& p : Printed){
            if(p.second.first > slotErased)
                --p.second.first;
        }
        pos.y += 2.0;
        ApplyGravityAt(pos);
    }

    checkLaserRays = true;
}

bool LevelManager::hasObject(glm::vec3 pos){
    return Objects.find(pos) != std::end(Objects) || Printed.find(pos) != std::end(Printed);
}

bool LevelManager::hasBlockingObject(glm::vec3 pos){
    return Obstackles.find(pos) != std::end(Obstackles) || (hasObject(pos) && GetObject(pos)->IsBlocking()) || Tiles.find(pos+glm::vec3(0.0f,2.0f,0.0f)) != std::end(Tiles);
}

std::shared_ptr<GameObject> LevelManager::GetObject(glm::vec3 pos){
    if(Objects.find(pos) != std::end(Objects))
        return Objects.at(pos);
    else if(Printed.find(pos) != std::end(Printed))
        return Printed.at(pos).second;
    else return nullptr;
}

std::shared_ptr<LaserRay> LevelManager::RegisterLaserRay(glm::vec3 pos, Direction dir){
    std::shared_ptr<LaserRay> ray = std::make_shared<LaserRay>(pos, dir);
    ray->Update();
    LaserRays.push_back(ray);
    return LaserRays.back();
}

void LevelManager::EraseLaserRay(glm::vec3 pos){
    std::for_each(std::begin(LaserRays),std::end(LaserRays),[pos](auto& r){if(r->origin == pos){r->ClearTiles();r->ClearTarget();r->markedToDestroy = true;}});
    LaserRays.remove_if([pos](auto& r){return glm::abs(r->origin.x-pos.x) < 0.01 && glm::abs(r->origin.y-pos.y) < 0.01 && glm::abs(r->origin.z-pos.z) < 0.01;});
}

void LevelManager::AddTile(std::shared_ptr<Tile> t){
    glm::vec3 pos = t->transform.position;
    Tiles[pos] = t;
}

void LevelManager::AddObstackle(std::shared_ptr<GameObject> o){
    glm::vec3 pos = o->transform.position;
    Obstackles[pos] = o;

    try{
        Tiles.at(pos)->occupied = true;
        Tiles.at(pos)->trespassable = false;
    }
    catch(...){}
}

void LevelManager::AddObject(std::shared_ptr<GameObject> obj){
    glm::vec3 pos = obj->transform.position;

    Objects[pos] = obj;

    try{
        Tiles.at(pos)->occupied = true;
        Tiles.at(pos)->trespassable = !obj->IsBlocking();
    }
    catch(...){}

    if (std::shared_ptr<PowerSource> src = std::dynamic_pointer_cast<PowerSource>(obj)) {
        AddConnection(obj->transform.position, src->GetPowerTarget());
    }
}

void LevelManager::AddConnection(glm::vec3 origin, glm::vec3 dest) {
    connector.AddConnection(origin, dest);
}

void LevelManager::AddEvent(glm::vec3 pos, Event ev){
    Events[pos] = ev;
}

void LevelManager::Clear(){
    for(auto& p : Objects){
        p.second->CleanUp();
        p.second = nullptr;
    }
    Objects.clear();
    for(auto& p : Printed){
        p.second.second->CleanUp();
        p.second.second = nullptr;
    }
    Printed.clear();
    Events.clear();
    Obstackles.clear();
    Tiles.clear();
    connector.Clean();
}

void LevelManager::ApplyGravityAt(glm::vec3 pos){
    /*try{
        GetAllTiles().at(pos);
    }catch(...){
        try{
            for(;;){
                std::shared_ptr<GameObject> obj = GetObject(pos);
                if(!obj) break;
                obj->Fall();
                if(std::dynamic_pointer_cast<Tile>(obj))
                    pos.y += 2.0;
                else
                    break;
            }
        }catch(...){}
    }*/
    std::shared_ptr<GameObject> obj = GetObject(pos);
    if(obj)
        obj->Fall();
}

void LevelManager::RelocateObject(glm::vec3 oldPos, glm::vec3 newPos){
    if(oldPos == newPos) return;

    std::shared_ptr<GameObject> toMove;
    if(Objects.find(oldPos) != std::end(Objects)){
        toMove = Objects.at(oldPos);
        Objects[newPos] = toMove;
        Objects.erase(oldPos);
    }
    else if(Printed.find(oldPos) != std::end(Printed)){
        toMove = Printed.at(oldPos).second;
        Printed[newPos] = Printed.at(oldPos);
        Printed.erase(oldPos);
    }
    else return;

    if(toMove->IsBlocking()){
        auto tiles = GetAllTiles();
        try{
        tiles.at(newPos)->occupied = true;
        tiles.at(newPos)->trespassable = false;
        tiles.at(oldPos)->occupied = false;
        tiles.at(oldPos)->trespassable = true;
        }catch(...){}
    }
}

bool LevelManager::PlayerAt(glm::vec3 pos){
    auto tiles = GetAllTiles();
    try{
        return tiles.at(pos)->hasPlayer;
    }catch(...){}
    return false;
}

void LevelManager::RegisterPlayer(std::shared_ptr<Player> player){
    playerPos = &player->transform.position;
}
