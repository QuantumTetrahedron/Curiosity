#include "Door.h"
#include "LevelManager.h"

std::unique_ptr<GameObject> Door::clone() const{
    return std::make_unique<Door>(*this);
}

Door::Door(const Door& other)
: ElectricityPowered(other.powerRequired){
    *this = other;
    powerLevel = 0;
}

Door::Door(int reqPower,glm::vec3 position, glm::vec3 rotation, glm::vec3 scale, std::string modelName)
: GameObject(position, rotation, scale, modelName, true, true, false, false), ElectricityPowered(reqPower){
    open = false;
    offset = 0.0f;
}

void Door::Update(double dt){
    float v = 15.0f * dt;
    GameObject::Update(dt);
    MoveTo(offset, open?-5.5f:0.0f, v);
    if(collider)
        collider->position = transform.position + glm::vec3(0.0f, offset, 0.0f);
}

void Door::Draw(const Shader& shader){
    float old = transform.position.y;
    transform.position.y += offset;
    GameObject::Draw(shader);
    transform.position.y = old;
}

Door::Door(std::string str){
    std::regex pattern{R"((\w{1})\s+(\S+)\s+(\S+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+))"};
    std::smatch matches;
    if(std::regex_match(str, matches, pattern)){
        std::string name = matches[3];
        std::string type = matches[2];

        int x = atoi(((std::string)matches[4]).c_str());
        int y = atoi(((std::string)matches[5]).c_str());
        int z = atoi(((std::string)matches[6]).c_str());
        int r = atoi(((std::string)matches[7]).c_str());
        int p = atoi(((std::string)matches[8]).c_str());

        *this = Door(p, glm::vec3(2*x, y, 2*z), glm::vec3(0,90*r,0), glm::vec3(1.0f), name);
    }
}

void Door::TurnOn(){
    Open();
}

void Door::TurnOff(){
    Close();
}

void Door::Open(){
    if(open) return;
    //glm::vec3 offset = glm::vec3(0, -5, 0);

    open = true;
    scannable = false;
    blocking = false;

    //target.position = transform.position + offset;
    try{
        LevelManager::GetTiles()->at(transform.position)->trespassable = true;
        LevelManager::GetTiles()->at(transform.position)->occupied = true;
    }
    catch(...){}
}

void Door::Close(){
    if(!open) return;
    //glm::vec3 offset = glm::vec3(0, -5, 0);

    open = false;
    scannable = true;
    blocking = true;

    //target.position = transform.position - offset;
    try{
        LevelManager::GetTiles()->at(target.position)->trespassable = false;
        LevelManager::GetTiles()->at(target.position)->occupied = true;
    }
    catch(...){}
}
