#include "Level.h"
#include <fstream>
#include <string>
#include <iostream>
#include <regex>
#include "Ray.h"
#include "LevelManager.h"
#include "TestObj.h"
#include "Door.h"
#include "Lever.h"
#include "Laser.h"
#include "Mirror.h"
#include "Photodetector.h"
#include "Magnet.h"
#include "Event.h"

void Level::Load(std::string file){
    shouldReload = false;
    lastClicked = nullptr;
    lastHovered = nullptr;
    currentLevelFile = file;
    LevelManager::state = LevelState::awaitingInput;
    hud.Select(0, HUDmode::HUD_MODE_PRINT);
    hud.Select(0, HUDmode::HUD_MODE_SCAN);
    std::ifstream f;
    f.open(file);
    if(!f.good())return;
    LevelManager::Clear();
    renderer.Clear();
    std::string str;
    while(std::getline(f, str)){
        if(str.length()==0 || str[0] == '#')
            continue;
        if(str[0] == 'm')
            LoadModel(str);
        else if(str[0] == 't')
            LoadTile(str);
        else if(str[0] == 'w')
            LoadObstackle(str);
        else if(str[0] == 'o')
            LoadObject(str);
        else if(str[0] == 's')
            LoadPlayer(str);
        else if(str[0] == 'e')
            LoadEvent(str);
    }
    f.close();
    player->dead = false;
    player->ResetMemory();
    player->at = player->GetTile();

    camera.SetParent(player);

    renderer.Init();
    LevelManager::GenerateSubset();
}

void Level::LoadEvent(std::string str){
    std::regex pattern{R"((\w{1})\s+(\w{1})\s+'([^']+)'\s+(\d+)\s+(\d+)\s+(\d+))"};
    std::smatch matches;
    if(std::regex_match(str, matches, pattern)){
        std::string filename = matches[3];
        std::string t = (std::string)matches[2];
        bool f = (t[0] == 'f');

        int x = atoi(((std::string)matches[4]).c_str());
        int y = atoi(((std::string)matches[5]).c_str());
        int z = atoi(((std::string)matches[6]).c_str());

        LevelManager::AddEvent(glm::vec3(2*x,y,2*z), Event(filename, f));
    }
}

void Level::LoadPlayer(std::string str){
    std::regex pattern{R"((\w{1})\s+(\d+)\s+(\d+)\s+(\d+))"};
    std::smatch matches;
    if(std::regex_match(str, matches, pattern)){

        int x = atoi(((std::string)matches[2]).c_str());
        int y = atoi(((std::string)matches[3]).c_str());
        int z = atoi(((std::string)matches[4]).c_str());

        player = std::shared_ptr<Player>(new Player("player", glm::vec3(2*x,y,2*z),glm::vec3(0.0)));

        LevelManager::RegisterPlayer(player);
    }
}

void Level::Init(int windowWidth, int windowHeight){
    shouldReload = false;
    windowW = windowWidth;
    windowH = windowHeight;
    lastClicked = nullptr;
    connMode = false;
    camera = Camera(windowWidth, windowHeight, player, glm::vec3(0.0f,1.0f,0.0f));
    LevelManager::state = LevelState::awaitingInput;
    shadowMapper.Init(2048,2048,camera.Width, camera.Height);
    initHDR();
    hud.Init(windowWidth, windowHeight);
    Event::RegisterLevel(this);

    Shader& textShader = ResourceManager::GetShader("text");
    textRenderer = std::make_shared<TextRenderer>(textShader);
    #ifdef __linux__
        textRenderer->Load("/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf", 24);
    #else
        textRenderer->Load("C://Windows/Fonts/DejaVuSans.ttf", 24);
    #endif
}

void Level::AddTile(std::shared_ptr<Tile> t){ LevelManager::AddTile(t); }
void Level::AddObstackle(std::shared_ptr<GameObject> o){ LevelManager::AddObstackle(o); }
void Level::AddObject(std::shared_ptr<GameObject> obj){ LevelManager::AddObject(obj); }

Level::~Level(){
    LevelManager::Clear();
}

bool Level::hasObject(glm::vec3 pos){
    return LevelManager::hasObject(pos);
}

void Level::ClearPath(){
    for(auto& p : path){
        p->ClearPath();
    }
    path.clear();
}

void Level::ErasePath(){
    player->ErasePath();
    ClearPath();
    if(lastClicked)
        lastClicked->Deselect();
    lastClicked = nullptr;
}

std::vector<std::shared_ptr<const GameObject>> Level::GetCollidable(){
    std::vector<std::shared_ptr<const GameObject>> collidable;
    for(const auto& t : LevelManager::Tiles)
        if(t.second->collider) collidable.push_back(t.second);
    for(const auto& t : LevelManager::Obstackles)
        if(t.second->collider) collidable.push_back(t.second);
    for(const auto& t : LevelManager::Objects)
        if(t.second->collider) collidable.push_back(t.second);

    for(const auto& t : LevelManager::Printed)
        if(t.second.second->collider) collidable.push_back(t.second.second);

    return collidable;
}

bool Level::SetPath(std::shared_ptr<Player> player, std::shared_ptr<Tile> dest){
    if(!player->GetTile()) return false;

    std::map<glm::vec3, std::shared_ptr<Tile>, util::vec3Comparator> tiles = LevelManager::GetAllTiles();

    auto edges = pathfinder.GetGraphEdges(tiles);
    path = pathfinder.FindPath(edges, player->GetTile(), dest);

    if(path.size() == 0) return false;

    for(std::shared_ptr<Tile> t : path){
        t->SetAsPath();
    }
    dest->ClearPath();
    dest->Select();

    player->SetPath(path);

    return true;
}

unsigned int quadVAO = 0;
unsigned int quadVBO;
void renderQuad()
{
    if (quadVAO == 0)
    {
        float quadVertices[] = {
            // positions        // texture Coords
            -1.0f,  1.0f, 0.0f, 0.0f, 1.0f,
            -1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
             1.0f,  1.0f, 0.0f, 1.0f, 1.0f,
             1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
        };
        glGenVertexArrays(1, &quadVAO);
        glGenBuffers(1, &quadVBO);
        glBindVertexArray(quadVAO);
        glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
    }
    glBindVertexArray(quadVAO);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glBindVertexArray(0);
}

void Level::Render(){
    std::vector<std::shared_ptr<Tile>> tiles;
    std::vector<std::shared_ptr<GameObject>> obst;
    std::vector<std::shared_ptr<GameObject>> objects;
    std::vector<std::shared_ptr<GameObject>> printed;
    std::vector<std::shared_ptr<LaserRay>> rays;
    std::list<ConnectionStrip> crays;

    GetTiles(&tiles);
    GetObstackles(&obst);
    GetObjects(&objects);
    GetPrinted(&printed);
    GetRays(&rays);
    crays = LevelManager::connector.connections;

    UpdateShaders();

    Shader& s = ResourceManager::GetShader("instanced");
    Shader& laser = ResourceManager::GetShader("laser");
    Shader& simple = ResourceManager::GetShader("simple");
    Shader& blur = ResourceManager::GetShader("blur");
    Shader& PBR = ResourceManager::GetShader("PBR");
    Shader& PBRinst = ResourceManager::GetShader("PBRinst");
    Shader& cray = ResourceManager::GetShader("cray");

    /** TODO: consider solution without sorting each frame **/
    glm::vec3 pp = player->transform.position;
    std::sort(std::begin(rays), std::end(rays), [&pp](std::shared_ptr<LaserRay> r1, std::shared_ptr<LaserRay> r2){return glm::length((r1->origin+r1->dest)/glm::vec3(2.0f)-pp) < glm::length((r2->origin+r2->dest)/glm::vec3(2.0f)-pp);});

    for(int i=0; i<8 && i<rays.size();++i){
        rays[i]->UpdateShader(s, i);
        rays[i]->UpdateShader(PBRinst, i);
    }
    s.Use();
    s.SetInteger("rayLightsToRender", glm::min(8,(int)rays.size()));
    PBRinst.Use();
    PBRinst.SetInteger("rayLightsToRender", glm::min(8,(int)rays.size()));

    /// Draw shadow map ---------------------------------------------------------------------------------
    shadowMapper.Enable();
    renderer.DrawInstanced(tiles, shadowMapper.GetShader(true));
    renderer.DrawInstanced(obst, shadowMapper.GetShader(true));
    for(std::shared_ptr<GameObject> o : objects){
        o->Draw(shadowMapper.GetShader(false));
    }
    for(std::shared_ptr<GameObject> o : printed){
        o->Draw(shadowMapper.GetShader(false));
    }
    shadowMapper.Disable();
    shadowMapper.BindTexture();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    /// Draw scene to framebuffer -------------------------------------------------------------------------
    glBindFramebuffer(GL_FRAMEBUFFER, hdrFBO);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        renderer.DrawTiles(tiles, PBRinst);
        renderer.DrawInstanced(obst, PBRinst);

        for(std::shared_ptr<LaserRay> r : rays){
            r->Draw(laser);
        }
        for(std::shared_ptr<GameObject> o : objects){
            o->Draw(PBR);
        }
        for(std::shared_ptr<GameObject> o : printed){
            o->Draw(PBR);
        }
        if (connMode) {
            for(ConnectionStrip c : crays) {
                c.first.Draw(cray);
                c.diagonal.Draw(cray);
                c.last.Draw(cray);
            }
        }

        player->Draw(simple);

    glBindFramebuffer(GL_READ_FRAMEBUFFER, hdrFBO);
    glReadBuffer(GL_COLOR_ATTACHMENT0);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, finalFBO);
    glBlitFramebuffer(0,0,windowW, windowH, 0,0,windowW, windowH, GL_COLOR_BUFFER_BIT, GL_NEAREST);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    ///apply blur
    bool horizontal = true, firstPass = true;
    int passes = 10;
    blur.Use();
    for (unsigned int i = 0; i<passes; ++i) {
        glBindFramebuffer(GL_FRAMEBUFFER, blurFBO[horizontal]);
        blur.SetInteger("horizontal", horizontal);
        glBindTexture(GL_TEXTURE_2D, firstPass ? colorBuffers[1] : blurBuffers[!horizontal]);
        renderQuad();
        horizontal = !horizontal;
        if (firstPass)
            firstPass = false;
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    /// Draw frame ------------------------------------------------------------------------------------------
    Shader& hdrQuad = ResourceManager::GetShader("HDR");
    hdrQuad.Use();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, finalColorBuffer);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, blurBuffers[1]);
    renderQuad();
    glClear(GL_DEPTH_BUFFER_BIT);

    /// Draw HUD ------------------------------------------------------------------------------------------------
    if(player->printMode)
        hud.Render(HUDmode::HUD_MODE_PRINT, *player->GetPrints());
    else
        hud.Render(HUDmode::HUD_MODE_SCAN, *player->GetScans());

    /// Draw death -------------------------------------------------------------------------------------------
    if(LevelManager::state == LevelState::dying){
        int msgCount = glm::ceil(player->deathTimer);
        if(player->deathTimer > 0.0f){
            for(int i=0;i<msgCount;i++)
                textRenderer->Render(LevelManager::deathMessages[LevelManager::deathMessageSubset[i]] , 10+(i+1)*30.0f, (i+1)*75.0f, 1.5, glm::vec3(1.0f));
        }
    }
}


void Level::GetTiles(std::vector<std::shared_ptr<Tile>>* tiles){
    std::transform(std::begin(LevelManager::Tiles), std::end(LevelManager::Tiles), std::back_inserter(*tiles), [](auto& p){return p.second;});
}

void Level::GetObstackles(std::vector<std::shared_ptr<GameObject>>* obst){
    std::transform(std::begin(LevelManager::Obstackles), std::end(LevelManager::Obstackles), std::back_inserter(*obst), [](auto& p){return p.second;});
}

void Level::GetObjects(std::vector<std::shared_ptr<GameObject>>* objects){
    std::transform(std::begin(LevelManager::Objects), std::end(LevelManager::Objects), std::back_inserter(*objects), [](auto& p){return p.second;});
}

void Level::GetPrinted(std::vector<std::shared_ptr<GameObject>>* printed){
    std::transform(std::begin(LevelManager::Printed), std::end(LevelManager::Printed), std::back_inserter(*printed), [](auto& p){return p.second.second;});
}

void Level::GetRays(std::vector<std::shared_ptr<LaserRay>>* rays){
    std::transform(std::begin(LevelManager::LaserRays), std::end(LevelManager::LaserRays), std::back_inserter(*rays), [](auto& r){return r;});
}

void Level::UpdateShaders(){
    /** TODO: Uniform Buffer Objects **/

    Shader& s = ResourceManager::GetShader("instanced");
    s.Use();
    glm::mat4 view = camera.GetViewMatrix();
    s.SetMatrix4("view", view);
    s.SetVector3f("viewPos", camera.Position);

    s.SetVector3f("spotLight.position", player->transform.position);
    s.SetVector3f("spotLight.direction", player->Front());

    Shader& depthInst = ResourceManager::GetShader("depthInst");
    glm::mat4 lightProjection, lightView;
    glm::mat4 lightSpaceMatrix;

    lightProjection = glm::perspective(glm::radians(90.0f), 1.0f, 0.1f, 100.0f);
    lightView = glm::lookAt(player->transform.position, player->transform.position+player->Front(), glm::vec3(0.0f, 1.0f, 0.0f));
    lightSpaceMatrix = lightProjection * lightView;
    depthInst.Use();
    depthInst.SetMatrix4("lightSpaceMatrix", lightSpaceMatrix);
    s.Use();
    s.SetMatrix4("lightSpaceMatrix", lightSpaceMatrix);

    Shader& simple = ResourceManager::GetShader("simple");
    simple.Use();
    simple.SetMatrix4("lightSpaceMatrix", lightSpaceMatrix);
    simple.SetMatrix4("view", view);
    simple.SetVector3f("viewPos", camera.Position);

    simple.SetVector3f("spotLight.position", player->transform.position);
    simple.SetVector3f("spotLight.direction", player->Front());
    Shader& depth = ResourceManager::GetShader("depth");
    depth.Use();
    depth.SetMatrix4("lightSpaceMatrix", lightSpaceMatrix);

    Shader& laser = ResourceManager::GetShader("laser");
    laser.Use();
    laser.SetMatrix4("view", view);

    Shader& cray = ResourceManager::GetShader("cray");
    cray.Use();
    cray.SetMatrix4("view", view);

    Shader& PBR = ResourceManager::GetShader("PBR");
    PBR.Use();
    PBR.SetMatrix4("view", view);
    PBR.SetVector3f("viewPos", camera.Position);
    PBR.SetMatrix4("lightSpaceMatrix", lightSpaceMatrix);
    PBR.SetVector3f("spotLight.position", player->transform.position);
    PBR.SetVector3f("spotLight.direction", player->Front());

    Shader& PBRinst = ResourceManager::GetShader("PBRinst");
    PBRinst.Use();
    PBRinst.SetMatrix4("view", view);
    PBRinst.SetVector3f("viewPos", camera.Position);

    PBRinst.SetVector3f("spotLight.position", player->transform.position);
    PBRinst.SetVector3f("spotLight.direction", player->Front());
    PBRinst.SetMatrix4("lightSpaceMatrix", lightSpaceMatrix);
}

void Level::LoadModel(std::string str){
    std::regex pattern{R"((\w{1})\s+(\S+)\s+(\S+)\s+(\S+))"};
    std::smatch matches;
    if(std::regex_match(str, matches, pattern)){
        std::string path = matches[3];
        std::string name = matches[2];
        float height = atof(((std::string)matches[4]).c_str());
        ResourceManager::LoadModel(path.c_str(), name, height);
    }
}

void Level::Update(double dt){
    for(auto& o : LevelManager::Objects)
        o.second->Update(dt);
    for(auto& o : LevelManager::Printed){
        o.second.second->Update(dt);
    }

    std::for_each(std::begin(LevelManager::Relocator), std::end(LevelManager::Relocator), [](auto& p){LevelManager::RelocateObject(p.first, p.second);});
    if (LevelManager::Relocator.size() > 0) {
        LevelManager::checkLaserRays = true;
    }
    LevelManager::Relocator.clear();

    if (LevelManager::checkLaserRays) {
        LevelManager::checkLaserRays = false;
        for(auto& r : LevelManager::LaserRays)
            r->Update();
    }

    player->Update(dt);
    if(player->dead) Reload();

    if(LevelManager::state == LevelState::moving && !player->moving)
        LevelManager::state = LevelState::awaitingInput;
    if(LevelManager::state == LevelState::falling && !player->falling)
        LevelManager::state = LevelState::awaitingInput;
    camera.Update(dt);

    if(shouldReload)
    {
        Load(currentLevelFile);
    }
}

void Level::LoadTile(std::string str){
    std::regex pattern{R"((\w{1})\s+(\S+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\S{1}))"};
    std::smatch matches;
    if(std::regex_match(str, matches, pattern)){
        std::string name = matches[2];
        int x1 = atoi(((std::string)matches[3]).c_str());
        int y1 = atoi(((std::string)matches[4]).c_str());
        int z1 = atoi(((std::string)matches[5]).c_str());
        int x2 = atoi(((std::string)matches[6]).c_str());
        int y2 = atoi(((std::string)matches[7]).c_str());
        int z2 = atoi(((std::string)matches[8]).c_str());
        int r = atoi(((std::string)matches[9]).c_str());

        for(int i=x1; i<=x2; ++i)
        {
            for(int j=y1; j<=y2; ++j)
            {
                for(int k=z1; k<=z2; ++k)
                {
                    AddTile(std::shared_ptr<Tile>(new Tile(glm::vec3(2*i, j, 2*k), glm::vec3(0,90*r,0),glm::vec3(1.0f),name,1)));
                }
            }
        }
    }
}

void Level::LoadObstackle(std::string str){
    std::regex pattern{R"((\w{1})\s+(\S+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\S{1}))"};
    std::smatch matches;
    if(std::regex_match(str, matches, pattern)){
        std::string name = matches[2];
        int x1 = atoi(((std::string)matches[3]).c_str());
        int y1 = atoi(((std::string)matches[4]).c_str());
        int z1 = atoi(((std::string)matches[5]).c_str());
        int x2 = atoi(((std::string)matches[6]).c_str());
        int y2 = atoi(((std::string)matches[7]).c_str());
        int z2 = atoi(((std::string)matches[8]).c_str());
        int r = atoi(((std::string)matches[9]).c_str());

        for(int i=x1; i<=x2; ++i)
        {
            for(int j=y1; j<=y2; ++j)
            {
                for(int k=z1; k<=z2; ++k)
                {
                    AddObstackle(std::shared_ptr<GameObject>(new GameObject(glm::vec3(2*i, j, 2*k), glm::vec3(0,90*r,0),glm::vec3(1.0f),name)));
                }
            }
        }
    }
}

void Level::LoadObject(std::string str){
    std::regex pattern{R"((\w{1})\s+(\S+)\s+(\S+).*)"};
    std::smatch matches;
    if(std::regex_match(str, matches, pattern)){
        std::string type = matches[2];

        if(type == "test")
            AddObject(std::shared_ptr<TestObj>(new TestObj(str)));
        else if(type == "door")
            AddObject(std::shared_ptr<Door>(new Door(str)));
        else if(type == "lever")
            AddObject(std::shared_ptr<Lever>(new Lever(str)));
        else if(type == "laser")
            AddObject(std::shared_ptr<Laser>(new Laser(str)));
        else if(type=="mirror")
            AddObject(std::shared_ptr<Mirror>(new Mirror(str)));
        else if(type=="photodetector")
            AddObject(std::shared_ptr<Photodetector>(new Photodetector(str)));
        else if(type=="magnet")
            AddObject(std::shared_ptr<Magnet>(new Magnet(str)));
    }
}

std::shared_ptr<Tile> Level::ProcessHovering(Input& in, std::map<glm::vec3, std::shared_ptr<Tile>, util::vec3Comparator>& tiles){
    Ray ray(camera, in.mouseX, in.mouseY);

    std::shared_ptr<Tile> hovered = nullptr;

    std::vector<std::shared_ptr<const GameObject>> collidable = GetCollidable();
    auto p = ray.Collide(collidable);
    if(p){
        auto pp = p.value_or(glm::vec3(0.0f));
        try{
            while(tiles.at(pp))
            {
                hovered = tiles.at(pp);
                pp+=glm::vec3(0.0f,2.0f,0.0f);
            }
        }
        catch(...){}

        if(hovered)
            player->UpdateRotation(hovered->transform.position);
    }
    if(lastHovered){
        lastHovered->Unhover();
        lastHovered = nullptr;
    }
    if(hovered){
        hovered->Hover();
        lastHovered = hovered;
    }
    return hovered;
}

void Level::ProcInputStateAwaiting(Input& in){
    auto tiles = LevelManager::GetAllTiles();
    std::shared_ptr<Tile> hovered = ProcessHovering(in, tiles);

    if(in.mouseButtons[GLFW_MOUSE_BUTTON_LEFT]){
        if(hovered){
            if(lastClicked)
                lastClicked->Deselect();

            if(hovered->GetDistance(*player->GetTile()) == 2 && hasObject(hovered->transform.position) && !hovered->trespassable)
            {
            }
            else{
                if(SetPath(player, hovered)){
                    lastClicked = hovered;
                    LevelManager::state = LevelState::pathSelected;
                }
                else
                    lastClicked = nullptr;
            }
        }
    }

    if(in.mouseButtons[GLFW_MOUSE_BUTTON_RIGHT]){
        if(hovered && hovered->GetDistance(*player->GetTile())>2)
        {
            try{
                hovered = tiles.at(glm::vec3(hovered->transform.position.x, player->GetTile()->transform.position.y, hovered->transform.position.z));
            }
            catch(...){}
        }

        if(hovered && hovered->GetDistance(*player->GetTile())==2)
        {
            if(hasObject(hovered->GetPos()) && LevelManager::GetObject(hovered->GetPos())->CanScan())
            {
                try{
                    player->Scan(LevelManager::GetObjects()->at(hovered->GetPos()));
                }catch(...){}
            }
            else if(!hovered->occupied){
                player->Print(player->selectedScan, hovered->GetPos());
            }
        }
    }

    ProcessCamera(in);
    ProcessPrintScan(in);
}

void Level::ProcInputStatePathSelected(Input& in){
    auto tiles = LevelManager::GetAllTiles();
    std::shared_ptr<Tile> hovered = ProcessHovering(in, tiles);

    if(in.mouseButtons[GLFW_MOUSE_BUTTON_LEFT]){

        if(hovered && lastClicked == hovered){
            ClearPath();

            if(lastClicked)
                lastClicked->Deselect();

            LevelManager::state = LevelState::moving;
            player->Move();
        }
        else if(hovered && lastClicked != hovered){
            ClearPath();

            if(lastClicked)
                lastClicked->Deselect();

            if(hovered->GetDistance(*player->GetTile()) == 2 && hasObject(hovered->transform.position) && !hovered->trespassable)
            {
            }
            else{
                if(SetPath(player, hovered))
                    lastClicked = hovered;
                else
                    lastClicked = nullptr;
            }
        }
    }
    if(in.mouseButtons[GLFW_MOUSE_BUTTON_RIGHT]){
        ErasePath();
        LevelManager::state = LevelState::awaitingInput;
    }

    ProcessCamera(in);
    ProcessPrintScan(in);
}

void Level::ProcInputStateMoving(Input& in){
    auto tiles = LevelManager::GetAllTiles();
    ProcessHovering(in, tiles);

    if(in.mouseButtons[GLFW_MOUSE_BUTTON_RIGHT]){
        player->CancelMove();
    }

    ProcessCamera(in);
}

void Level::ProcInputStateFalling(Input& in){
    auto tiles = LevelManager::GetAllTiles();
    ProcessHovering(in, tiles);
    ProcessCamera(in);
}

void Level::ProcessCamera(Input& in){
    if(in.scrollOffset)
        camera.ProcessMouseScroll(in.scrollOffset);
    in.ResetOffsets();

    if(in.keys[GLFW_KEY_Q] && !camera.Rotating())
        camera.RotateLeft();
    if(in.keys[GLFW_KEY_E] && !camera.Rotating())
        camera.RotateRight();
}

void Level::ProcessPrintScan(Input& in){
    if(in.keys[GLFW_KEY_LEFT_SHIFT])
        player->printMode = true;
    else
        player->printMode = false;

    ProcessSelectorKey(in, GLFW_KEY_1, 0);
    ProcessSelectorKey(in, GLFW_KEY_2, 1);
    ProcessSelectorKey(in, GLFW_KEY_3, 2);
    ProcessSelectorKey(in, GLFW_KEY_4, 3);
}

void Level::ProcessInput(Input& in){
    if(LevelManager::state == LevelState::awaitingInput){
        ProcInputStateAwaiting(in);
    }
    else if(LevelManager::state == LevelState::pathSelected){
        ProcInputStatePathSelected(in);
    }
    else if(LevelManager::state == LevelState::moving){
        ProcInputStateMoving(in);
    }
    else if(LevelManager::state == LevelState::falling){
        ProcInputStateFalling(in);
    }

    connMode = in.keys[GLFW_KEY_LEFT_CONTROL];
    debug = in.keys[GLFW_KEY_P];
}

void Level::ProcessSelectorKey(Input& in, int key, int n){

    if(in.keys[key]){
        if(player->printMode){
            if(player->selectedPrint != n){
                player->selectedPrint = n;
                hud.Select(n, HUDmode::HUD_MODE_PRINT);
                in.keys[key] = false;
            }
            else{
                hud.SetSelectorLoadingState(in.keyListener.GetHoldTime(key)/1.0f, HUDmode::HUD_MODE_PRINT);
                if(in.keyListener.GetHoldTime(key) >= 1.0f){
                    hud.SetSelectorLoadingState(0.f, HUDmode::HUD_MODE_PRINT);
                    LevelManager::Delete(n);
                    ErasePath();
                    if(player->ShouldFall()){
                        player->Fall();
                        LevelManager::state = LevelState::falling;
                    }
                    player->ErasePrint(n);
                    in.keys[key] = false;
                }
            }
        }
        else{
            if(player->selectedScan != n){
                player->selectedScan = n;
                hud.Select(n, HUDmode::HUD_MODE_SCAN);
                in.keys[key] = false;
            }
            else{
                hud.SetSelectorLoadingState(in.keyListener.GetHoldTime(key)/1.0f, HUDmode::HUD_MODE_SCAN);
                if(in.keyListener.GetHoldTime(key) >= 1.0f){
                    hud.SetSelectorLoadingState(0.f, HUDmode::HUD_MODE_SCAN);
                    player->EraseScan(n);
                    in.keys[key] = false;
                }
            }
        }
    }else{

        if(!player->printMode && player->selectedScan == n)
            hud.SetSelectorLoadingState(0.f, HUDmode::HUD_MODE_SCAN);
        else if(player->printMode && player->selectedPrint == n)
            hud.SetSelectorLoadingState(0.f, HUDmode::HUD_MODE_PRINT);

    }
}

void Level::initHDR(){
    glGenFramebuffers(1, &hdrFBO);
    glBindFramebuffer(GL_FRAMEBUFFER, hdrFBO);

    glGenTextures(2, colorBuffers);

    /* MULTISAMPLING - nie rusza�, bo nie dzia�a */
    //multisample texture
    /*
     glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, colorBuffers[0]);
    glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, 4, GL_RGBA16F, windowW, windowH, GL_TRUE);
   // glTexParameteri(GL_TEXTURE_2D_MULTISAMPLE)
    glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, 0);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D_MULTISAMPLE, colorBuffers[0], 0);

    //bloom texture to framebuffer
    glBindTexture(GL_TEXTURE_2D, colorBuffers[1]);
    glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, 4, GL_RGBA16F, windowW, windowH, GL_TRUE);
   // glTexParameteri(GL_TEXTURE_2D_MULTISAMPLE)
    glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, 0);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D_MULTISAMPLE, colorBuffers[1], 0);*/
    /*
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, windowW, windowH, 0, GL_RGB, GL_FLOAT, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, colorBuffers[1], 0);
    */
    for (unsigned int i = 0; i < 2; i++)
    {
        glBindTexture(GL_TEXTURE_2D, colorBuffers[i]);
        glTexImage2D(
            GL_TEXTURE_2D, 0, GL_RGB16F, windowW, windowH, 0, GL_RGB, GL_FLOAT, NULL
        );
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        // attach texture to framebuffer
        glFramebufferTexture2D(
            GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, colorBuffers[i], 0
        );
    }

    unsigned int attachments[2] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
    glDrawBuffers(2, attachments);

    unsigned int rboDepth;
    glGenRenderbuffers(1, &rboDepth);
    glBindRenderbuffer(GL_RENDERBUFFER, rboDepth);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, windowW, windowH);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rboDepth);

    /* glGenRenderbuffers(1, &hdrDepthRBO);
    glBindRenderbuffer(GL_RENDERBUFFER, hdrDepthRBO);
    glRenderbufferStorageMultisample(GL_RENDERBUFFER, 4, GL_DEPTH24_STENCIL8, windowW, windowH);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, hdrDepthRBO); */

    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        std::cout << "ERROR::FRAMEBUFFER:: Framebuffer is not complete!!!!" << std::endl;
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glGenFramebuffers(1, &finalFBO);
    glBindFramebuffer(GL_FRAMEBUFFER, finalFBO);
    glGenTextures(1, &finalColorBuffer);
    glBindTexture(GL_TEXTURE_2D, finalColorBuffer);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, windowW, windowH, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glBindTexture(GL_TEXTURE_2D, 0);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, finalColorBuffer, 0);
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        std::cout << "ERROR::FRAMEBUFFER:: Intermediate framebuffer is not complete!" << std::endl;
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    //ping pong fbos
    glGenFramebuffers(2, blurFBO);
    glGenTextures(2, blurBuffers);
    for (unsigned int i = 0; i<2; ++i) {
        glBindFramebuffer(GL_FRAMEBUFFER, blurFBO[i]);
        glBindTexture(GL_TEXTURE_2D, blurBuffers[i]);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, windowW, windowH, 0, GL_RGB, GL_FLOAT, nullptr);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, blurBuffers[i], 0);
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}
