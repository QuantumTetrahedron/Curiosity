#ifndef LEVEL_H
#define LEVEL_H

#include <map>
#include <memory>

#include "Renderer.h"
#include "AStar.h"
#include "Player.h"
#include "Input.h"
#include "Camera.h"
#include "GameHud.h"
#include "LaserRay.h"
#include "ShadowMapper.h"
#include "TextRenderer.h"

/**
* The level itself.
*/
class Level{
public:
    Level(){}
    /**
    * Initializes the data, creates the necessary objects.
    * params:
    * * windowWidth : width of the window.
    * * windowHeight : height of the window.
    */
    void Init(int windowWidth, int windowHeight);

    /**
    * Loads the level from a given file.
    * params:
    * * path to a file describing the level.
    */
    void Load(std::string file);
    std::string currentLevelFile;
    bool shouldReload;
    void Reload(){
        shouldReload = true;
    }
    void SetLoad(std::string newFile){
        currentLevelFile = newFile;
        shouldReload = true;
    }

    /**
    * Clears the level manager.
    */
    ~Level();

    /// Main Loop Functions
    void Update(double dt);
    void Render();
    void ProcessInput(Input& in);

    /**
    * Checks whether an object (other than a tile or an obstacle) is placed at a given position.
    * params:
    * * pos : The position to check.
    * returns:
    * * true if an object is present at the given position, false otherwise.
    */
    bool hasObject(glm::vec3 pos);

    /**
    * Creates a path between the player and a destination.
    * params:
    * * player : A pointer to the player.
    * * dest : A pointer to the destination tile.
    * returns:
    * * true if the path was created.
    * * false if no path was found.
    */
    bool SetPath(std::shared_ptr<Player> player, std::shared_ptr<Tile> dest);

    /**
    * Clears the path selection.
    */
    void ClearPath();

    /**
    * Erases the path and clears it.
    */
    void ErasePath();

    /**
    * Gets all the objects with a collider.
    */
    std::vector<std::shared_ptr<const GameObject>> GetCollidable();

    std::shared_ptr<Player> player;
    Camera camera;

    std::shared_ptr<TextRenderer> textRenderer;
private:
    /// Methods loading from a file line.
    void LoadModel(std::string str);
    void LoadTile(std::string str);
    void LoadObstackle(std::string str);
    void LoadObject(std::string str);
    void LoadEvent(std::string str);

    ///Sets player's start position from a file line.
    void LoadPlayer(std::string str);

    /// Adds an object to the level manager.
    void AddTile(std::shared_ptr<Tile> t);
    void AddObstackle(std::shared_ptr<GameObject> o);
    void AddObject(std::shared_ptr<GameObject> obj);

    /**
    * Processes mouse hovering.
    * params:
    * * in : input data.
    * * tiles : A map of tiles.
    * returns:
    * * A pointer to the hovered tile.
    */
    std::shared_ptr<Tile> ProcessHovering(Input& in, std::map<glm::vec3, std::shared_ptr<Tile>, util::vec3Comparator>& tiles);

    /// Input processing.
    void ProcInputStateAwaiting(Input& in);
    void ProcInputStatePathSelected(Input& in);
    void ProcInputStateMoving(Input& in);
    void ProcInputStateFalling(Input& in);
    void ProcessCamera(Input& in);
    void ProcessPrintScan(Input& in);

    AStarPathfinding pathfinder;
    std::vector<std::shared_ptr<Tile>> path;

    std::shared_ptr<Tile> lastClicked;
    std::shared_ptr<Tile> lastHovered;

    Renderer renderer;

    int windowW, windowH;

    ShadowMapper shadowMapper;
    unsigned int hdrFBO, hdrDepthRBO;
    unsigned int colorBuffers[2];
    unsigned int blurFBO[2];
    unsigned int blurBuffers[2];
    unsigned int finalFBO, finalColorBuffer;
    void initHDR();
    bool debug;
    bool connMode;

    GameHUD hud;

    /**
    * Checks the state of a given key and updates a scan/print selector accordingly.
    * params:
    * * in : input data.
    * * key : key to check.
    * * n : index of scan/print slot corresponding to the key.
    */
    void ProcessSelectorKey(Input& in, int key, int n);

    /// Render helper methods
    void GetTiles(std::vector<std::shared_ptr<Tile>>* tiles);
    void GetObstackles(std::vector<std::shared_ptr<GameObject>>* obst);
    void GetObjects(std::vector<std::shared_ptr<GameObject>>* objects);
    void GetPrinted(std::vector<std::shared_ptr<GameObject>>* printed);
    void GetRays(std::vector<std::shared_ptr<LaserRay>>* rays);
    void UpdateShaders();
};

#endif // LEVEL_H
