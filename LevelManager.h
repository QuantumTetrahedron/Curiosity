#ifndef LEVEL_MANAGER_H
#define LEVEL_MANAGER_H

#include <list>

#include "Utility.h"
#include "Tile.h"
#include "LaserRay.h"
#include "Event.h"
#include "PowerSource.h"
#include "Connector.h"

class Level;
class Player;

/**
* Defines the possible states of the level.
*/
enum class LevelState{
    ///implemented
    awaitingInput,
    pathSelected,
    moving,
    falling,
    dying,
    ///to implement
    updating,
    contextMenu,
    scanning,
    printing,
    paused
};

/**
* Stores and manages objects from the active level.
*/
class LevelManager{
public:
    ///Getters.
    static std::map<glm::vec3, std::shared_ptr<Tile>, util::vec3Comparator>* GetTiles() {return &Tiles;}
    static std::map<glm::vec3, std::shared_ptr<GameObject>, util::vec3Comparator>* GetObstackles() {return &Obstackles;}
    static std::map<glm::vec3, std::shared_ptr<GameObject>, util::vec3Comparator>* GetObjects() { return &Objects; }

    static std::map<glm::vec3, std::pair<int, std::shared_ptr<GameObject>>, util::vec3Comparator> Printed;
    static Connector connector;

    /**
    * Returns a container of every type of tiles from the level.
    * * Static tiles
    * * Objects inheriting from Tile class.
    * * Printed objects inheriting from Tile class.
    */
    static std::map<glm::vec3, std::shared_ptr<Tile>, util::vec3Comparator> GetAllTiles(){
        std::map<glm::vec3, std::shared_ptr<Tile>, util::vec3Comparator> ret;
        std::copy(std::begin(Tiles), std::end(Tiles), std::inserter(ret, std::end(ret)));
        std::map<glm::vec3, std::shared_ptr<GameObject>, util::vec3Comparator> tmp;
        std::copy_if(std::begin(LevelManager::Objects), std::end(LevelManager::Objects), std::inserter(tmp, std::end(tmp)), [](auto& p){return std::dynamic_pointer_cast<Tile>(p.second);});
        std::transform(std::begin(tmp), std::end(tmp), std::inserter(ret, std::end(ret)), [](auto& p){return std::make_pair(std::dynamic_pointer_cast<Tile>(p.second)->GetPos(), std::dynamic_pointer_cast<Tile>(p.second));});
        std::map<glm::vec3, std::pair<int, std::shared_ptr<GameObject>>, util::vec3Comparator> tmp2;
        std::copy_if(std::begin(LevelManager::Printed), std::end(LevelManager::Printed), std::inserter(tmp2, std::end(tmp2)), [](auto& p){return std::dynamic_pointer_cast<Tile>(p.second.second);});
        std::transform(std::begin(tmp2), std::end(tmp2), std::inserter(ret, std::end(ret)), [](auto& p){return std::make_pair(std::dynamic_pointer_cast<Tile>(p.second.second)->GetPos(), std::dynamic_pointer_cast<Tile>(p.second.second));});
        return ret;
    }

    /**
    * Places an object into the Printed container.
    * params:
    * * pos : position of the object serving as key of the map.
    * * obj : object to store.
    * * slot : associated print slot index.
    */
    static void Print(glm::vec3 pos, std::shared_ptr<GameObject> obj, int slot);

    /**
    * Deletes a printed object associated with a given print slot index.
    * params:
    * * n : print slot index associated with the object to delete.
    */
    static void Delete(int n);

    /**
    * Checks whether an object (other than a tile or an obstacle) is placed at a given position.
    * params:
    * * pos : The position to check.
    * returns:
    * * true if an object is present at the given position, false otherwise.
    */
    static bool hasObject(glm::vec3 pos);

    /**
    * Checks whether an GameObject that should block movement is present at a given position.
    * params:
    * * pos : The position to check.
    * returns:
    * * true if an object with a collider is present at the given position, false otherwise.
    */
    static bool hasBlockingObject(glm::vec3 pos);

    /**
    * Gets an object from a given position. Searches through static and printed objects.
    * params:
    * * pos : The position to check.
    * returns:
    * * A pointer to the object at the position given or nullptr if no object is found.
    */
    static std::shared_ptr<GameObject> GetObject(glm::vec3 pos);

    /**
    * Places a laser ray in the level.
    * params:
    * * pos : position of the ray's origin.
    * * rotation : rotation of the ray.
    * returns:
    * * A pointer to the ray created.
    */
    static std::shared_ptr<LaserRay> RegisterLaserRay(glm::vec3 pos, Direction dir);

    /**
    * Removes a laser ray from the level.
    * params:
    * * pos : position of the origin of the ray erased.
    */
    static void EraseLaserRay(glm::vec3 pos);

    /**
    * Forces the objects above a given position to fall if no tile is found beneath them.
    * params:
    * * pos : position to check for lack of tile.
    */
    static void ApplyGravityAt(glm::vec3 pos);

    static void SetToRelocate(glm::vec3 oldPos, glm::vec3 newPos){
        Relocator.push_back(std::make_pair(oldPos, newPos));
    }

    static bool PlayerAt(glm::vec3 pos);

    static std::map<glm::vec3, Event, util::vec3Comparator> Events;

    friend class Level;

    static LevelState state;

    //std::shared_ptr<Player> player;
    static glm::vec3* playerPos;
    static void RegisterPlayer(std::shared_ptr<Player> player);

    static void CheckLaserRays() { checkLaserRays = true; }
private:

    LevelManager(){}

    static std::map<glm::vec3, std::shared_ptr<Tile>, util::vec3Comparator> Tiles;
    static std::map<glm::vec3, std::shared_ptr<GameObject>, util::vec3Comparator> Obstackles;
    static std::map<glm::vec3, std::shared_ptr<GameObject>, util::vec3Comparator> Objects;
    static std::list<std::shared_ptr<LaserRay>> LaserRays;
    static bool checkLaserRays;

    static std::vector<std::pair<glm::vec3,glm::vec3>> Relocator;
    static void RelocateObject(glm::vec3 oldPos, glm::vec3 newPos);

    /// Adders
    static void AddTile(std::shared_ptr<Tile> t);
    static void AddObstackle(std::shared_ptr<GameObject> o);
    static void AddObject(std::shared_ptr<GameObject> obj);
    static void AddEvent(glm::vec3 pos, Event ev);
    static void AddConnection(glm::vec3 origin, glm::vec3 dist);

    /**
    * Clears everything.
    */
    static void Clear();

    static std::array<int, 5> deathMessageSubset;
    static std::vector<std::string> deathMessages;
    static void GenerateSubset(){
        deathMessageSubset = {0,1,2,7,4};
    }
};

#endif // LEVEL_MANAGER_H
