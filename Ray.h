#ifndef RAY_H
#define RAY_H

#include "Camera.h"
#include "Transform.h"

#include "BoxCollider.h"
#include<experimental/optional>

#include <map>
#include <memory>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

/**
* A class used for raycasting.
*/
class Ray{
public:
    /**
    * Constructor
    * params:
    * * cam : The camera object used.
    * * scrX : cursor's x coordinate.
    * * scrY : cursor's y coordinate.
    */
    Ray(const Camera& cam, double scrX, double scrY);

    /**
    * Returns the position of the first object the ray collides with.
    * params:
    * * objects : A list of objects to check for collision.
    * returns:
    * * A position of the first object hit if such an object was found.
    */
    std::experimental::optional<glm::vec3> Collide(const std::vector<std::shared_ptr<const GameObject>>& objects);

private:
    glm::vec3 position;
    glm::vec3 direction;
};

#endif
