#ifndef CUTSCENES_H
#define CUTSCENES_H

#ifdef __linux__

#include <iostream>
#include <memory>

#include <GLFW/glfw3.h>
#ifdef __linux__
    #define GLFW_EXPOSE_NATIVE_X11
#else
    #define GLFW_EXPOSE_NATIVE_WIN32
    #define GLFW_EXPOSE_NATIVE_WGL
#endif
#include <GLFW/glfw3native.h>

#include "vlcpp/vlc.hpp"

class Cutscenes{
public:
    static void SetWindow(GLFWwindow* win);

    static void Load(std::string path);

    static void Update();

    static void Play();
    static void Stop();

    static bool isPlaying();
private:
    Cutscenes(){}

    static bool playing;
    static std::string loadedFilePath;

    static VLC::Instance instance;
    static VLC::Media media;
    static VLC::MediaPlayer mediaPlayer;
    static GLFWwindow* window;
};
#endif // __linux__

#endif // CUTSCENES_H


