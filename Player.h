#ifndef PLAYER_H
#define PLAYER_H

#include "Tile.h"
#include "TextRenderer.h"
#include "ResourceManager.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <string>
#include <iostream>
#include <queue>
#include <vector>
#include <GLFW/glfw3.h>
#include <random>
#include <memory>

class Player : public GameObject{
public:
    /// Constructor
    Player(std::string modelName = "", glm::vec3 position = glm::vec3(0.0f), glm::vec3 rotation = glm::vec3(0.0f), glm::vec3 scale=glm::vec3(1.0f));

    /// Draw overload.
    void Draw(Shader& shader);

    /**
    * Gets the tile beneath the player.
    * returns:
    * * The first tile found beneath the player.
    */
    std::shared_ptr<Tile> GetTile() const;

    /**
    * Gets the in-level position ignoring the float height.
    * returns:
    * * The player's in-level position.
    */
    glm::vec3 GetPos() const;

    /**
    * Checks for the presence of a tile directly beneath the player.
    * returns:
    * true if no tile was found, false otherwise.
    */
    bool ShouldFall();

    /**
    * Forces the player to fall to the first tile found beneath the player.
    */
    virtual void Fall();

    /**
    * Update override.
    * Moves and rotates the player if necessary.
    * params:
    * * dt : delta time.
    */
    void Update(GLfloat dt);

    bool dead;
    float deathTimer;
    void DieSlowly(float dt){
        if(!dead){
            deathTimer += dt;
            if(deathTimer > 5.0f)
                Die();
        }
    }
    void Die(){
        deathTimer = 0.0f;
        dead = true;
    }

    /**
    * Updates the player's target rotation so that it is facing a given position.
    * params:
    * * targetPos : position to look at.
    */
    void UpdateRotation(glm::vec3 targetPos);

    /**
    * Sets a path to follow as soon as the move method is called.
    * params:
    * * path : the path to set.
    */
    void SetPath(const std::vector<std::shared_ptr<Tile>>& path);

    /**
    * Clears the path set.
    */
    void ErasePath();

    /**
    * Initializes movement along a previously set path.
    */
    void Move();

    /**
    * Stops the player and clears its path.
    */
    void CancelMove();

    /**
    * Gets the normalized front vector.
    * returns:
    * * The normalized front vector.
    */
    glm::vec3 Front();

    /**
    * Saves a clone of the target and an associated rotation relative to the player.
    * params:
    * * target : Scanned target.
    * returns:
    * * True if the object was saved successfully, false if there was no free space left to save it.
    */
    bool Scan(std::shared_ptr<GameObject> target);

    /**
    * Places an object in the level at a given position with the saved rotation relative to the player.
    * params:
    * * n : index of the print slot from which to clone the object.
    * * target : target position.
    */
    void Print(int n, glm::vec3 target);

    bool printMode;

    /**
    * Gets the next free scan slot.
    */
    inline int GetScanSlotN() const { return scans.size()-1; }

    std::queue<std::shared_ptr<Tile>> moveQueue;
    bool moving;
    bool falling;

    int selectedScan;
    int selectedPrint;

    /// Getters.
    std::vector<std::shared_ptr<GameObject>>* GetScans() {return &scans;}
    std::vector<std::shared_ptr<GameObject>>* GetPrints() {return &prints;}

    /**
    * Free a given print/scan slot.
    * params:
    * * n : index of the print/scan slot to free.
    */
    void ErasePrint(int n){if(prints.size()>n)prints.erase(prints.begin()+n);}
    void EraseScan(int n){if(scans.size()>n)scans.erase(scans.begin()+n);}
    inline void ResetMemory(){ scans.clear(); prints.clear(); }

    std::shared_ptr<Tile> at;
private:
    float floatOffset;
    float floatHeight;

    std::vector<std::shared_ptr<GameObject>> scans;
    std::vector<std::shared_ptr<GameObject>> prints;
    std::vector<int> relativeRotations;
    int scanLimit;
    int printLimit;

    float moveSpeed;
    float rotSpeed;

    bool tileMoving;
};

#endif
