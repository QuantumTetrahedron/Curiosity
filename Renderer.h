
#ifndef RENDERER_H
#define RENDERER_H

#include "Tile.h"
#include "SpriteRenderer.h"

#include <set>
#include <string>
#include <iostream>
#include <memory>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

/**
* Stores the data of a specific model and renders it.
*/
class ModelRenderer{
public:
    ///Constructors.
    ModelRenderer(){initialized=false;}
    ModelRenderer(std::string modelName);

    /// Initializes the renderer.
    void InitRenderData(const std::vector<glm::mat4>& objects);

    /**
    * Draws a vector of objects using instancing.
    * params:
    * * objects : a vector of objects to draw.
    * * shader : the shader to use.
    */
    template <typename T>
    void DrawInstanced(const std::vector<std::shared_ptr<T>>& objects, const Shader& shader){
        std::vector<std::shared_ptr<GameObject>> toDraw = FilterByModel(objects, model);
        std::vector<glm::mat4> transforms;
        std::transform(std::begin(toDraw), std::end(toDraw), std::back_inserter(transforms), [](auto& obj){return obj->GetModelMatrix();});

        if(!initialized)
        {
            InitRenderData(transforms);
            initialized = true;
        }

        if(current_size != transforms.size())
        {
            glBindBuffer(GL_ARRAY_BUFFER, buffer);
            glBufferData(GL_ARRAY_BUFFER, transforms.size()*sizeof(glm::mat4), &transforms[0], GL_STATIC_DRAW);
        }

        shader.Use();
        Model& m = ResourceManager::GetModel(model);
        if(toDraw.size()>0){
            m.DrawInstanced(shader, transforms.size());
        }
    }
private:
    /**
    * Returns a vector of objects filtered by their model name.
    * params:
    * * objects : the vector of objects to filter.
    * * modelName : the wanted model name.
    * returns:
    * * The filtered vector of objects.
    */
    template <typename T>
    std::vector<std::shared_ptr<GameObject>> FilterByModel(const std::vector<std::shared_ptr<T>>& objects, std::string modelName){
        std::vector<std::shared_ptr<GameObject>> ret;
        std::copy_if(std::begin(objects), std::end(objects), std::back_inserter(ret), [modelName](auto& obj) { return obj->modelName.compare(modelName) == 0; });
        return ret;
    }

    std::string model;
    size_t current_size;
    bool initialized;
    GLuint buffer;
};

/**
* Used to render models.
*/
class Renderer{
public:
    /**
    * Draws tiles using instancing and the needed highlighters separately.
    * params:
    * * objects : a vector of tiles to render.
    * * shader : the shader to use.
    */
    void DrawTiles(const std::vector<std::shared_ptr<Tile>>& objects, const Shader& shader);

    /**
    * Draw objects using instancing.
    * params:
    * * objects : a vector of objects to draw.
    * * shader : the shader to use.
    */
    template<typename T>
    void DrawInstanced(const std::vector<std::shared_ptr<T>>& objects, const Shader& shader){
        shader.Use();

        std::set<std::string> modelNames = GetModelNames();

        for(std::string name : modelNames)
            renderers[name].DrawInstanced(objects, shader);
    }

    Renderer(){}

    /**
    * Initializes the renderer.
    */
    void Init();

    /**
    * Clears the renderer's saved model data.
    */
    void Clear();
private:
    std::map<std::string, ModelRenderer> renderers;

    /**
    * Retrieves the set of model names used.
    */
    std::set<std::string> GetModelNames();

    /**
    * Adds a new model renderer.
    */
    void AddModelRenderer(std::string modelName);
};

#endif
