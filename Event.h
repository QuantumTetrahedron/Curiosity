#ifndef EVENT_H
#define EVENT_H
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <regex>

class Level;

class Event{
public:
    Event();

    Event(std::string str, bool file);

    void Trigger();

    static void RegisterLevel(Level* l);

private:

    void ParseSingleCommand(std::string str);

    void ParseScriptFile();

    std::string command;
    bool isFile;

    static Level* level;
};

#endif // EVENT_H
