#ifndef CONNECTOR_H
#define CONNECTOR_H
#include <list>
#include "ConnectionRay.h"

class ConnectionStrip {
public:
    ConnectionStrip(glm::vec3 origin, glm::vec3 dest): first(origin, glm::vec3(origin.x, 4.0f, origin.z)),
     diagonal(glm::vec3(origin.x, 4.0f, origin.z), glm::vec3(dest.x, 4.0f, dest.z)), last(dest, glm::vec3(dest.x, 4.0f, dest.z)){ }
    ConnectionRay first;
    ConnectionRay diagonal;
    ConnectionRay last;
};

class Connector {
public:
    void AddConnection(glm::vec3 origin, glm::vec3 dest) {
        connections.emplace_back(origin, dest);
    }
    std::list<ConnectionStrip> connections;

    void Clean(){
        connections.clear();
    }
};

#endif // CONNECTOR_H_INCLUDED
