#include "ResourceManager.h"
#include "stb_image.h"

std::map<std::string, Shader> ResourceManager::Shaders;
std::map<std::string, Model> ResourceManager::Models;
std::map<std::string, Texture2D> ResourceManager::Textures;

Shader& ResourceManager::LoadShader(const GLchar* vertFile, const GLchar* fragFile, std::string name){
    Shaders[name] = Shader(vertFile, fragFile);
    return Shaders[name];
}

Shader& ResourceManager::GetShader(std::string name){
    return Shaders[name];
}

Model& ResourceManager::LoadModel(const GLchar* file, std::string name, float height){
    Models[name] = Model(file);
    Models[name].height = height;
    return Models[name];
}

Model& ResourceManager::GetModel(std::string name){
    try{
    return Models.at(name);
    }catch(...)
    {
        return Models[name];
    }
}

Texture2D& ResourceManager::LoadTexture(const GLchar* file, GLboolean alpha, std::string name){
    Textures[name] = Texture2D(file, alpha);
    return Textures[name];
}

Texture2D& ResourceManager::GetTexture(std::string name){
    return Textures[name];
}

void ResourceManager::Clear(){
    for(auto& s : Shaders)
        glDeleteProgram(s.second.ID);
    Shaders.clear();
    Models.clear();
    for(auto& t : Textures)
        glDeleteTextures(1, &t.second.ID);
    Textures.clear();
}
